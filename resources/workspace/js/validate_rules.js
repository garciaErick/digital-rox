var createUserValidation = function () {
    
    //$.validator.setDefaults({
    //	submitHandler: function() { alert("submitted!"); }
    //});
    
    $().ready(function() {
        
        // validate signup form on keyup and submit
        $("#createUserForm").validate({
            rules: {
                firstName: "required",
                lastName:  "required",
                password: {
                    required:  true,
                    minlength: 2
                },
                confirmPassword: {
                    required:  true,
                    minlength: 5,
                    equalTo:   "#password"
                },
                email: {
                    required: true,
                    email:    true
                },
                picture: {
                    extension: "jpg"
                }
            },
            messages: {
                firstName: "Please enter your firstname",
                lastName:  "Please enter your lastname",
                password: {
                    required:  "Please provide a password",
                    minlength: "Your password must be at least 5 characters long"
                },
                confirmPassword: {
                    required:  "Please provide a password",
                    minlength: "Your password must be at least 5 characters long",
                    equalTo:   "Please enter the same password as above"
                },
                email: "Please enter a valid email address",
                picture: "Please select an image with a jpg extension"
            }
        });
    });
}();

var updateInformationValidation = function () {
    
    //$.validator.setDefaults({
    //	submitHandler: function() { alert("submitted!"); }
    //});
    
    $().ready(function() {
        
        // validate signup form on keyup and submit
        $("#updateInformationForm").validate({
            rules: {
                firstName: "required",
                lastName:  "required",
                email: {
                    required: true,
                    email:    true
                },
                picture: {
                    extension: "jpg"
                }
            },
            messages: {
                firstName: "Please enter your firstname",
                lastName:  "Please enter your lastname",
                email:     "Please enter a valid email address",
                picture: "Please select an image with a jpg extension"
            }
        });
    });
}();

var updatePasswordValidation = function () {
    
    //$.validator.setDefaults({
    //	submitHandler: function() { alert("submitted!"); }
    //});
    
    $().ready(function() {
        
        // validate signup form on keyup and submit
        $("#updatePasswordForm").validate({
            rules: {
                oldPassword: {
                    required:  true,
                    minlength: 5
                },
                password: {
                    required:  true,
                    minlength: 5
                },
                confirmPassword: {
                    required:  true,
                    minlength: 5,
                    equalTo:   "#password"
                }		   
            },
            messages: {
                oldPassword: {
                    required:  "Please enter your previous password",
                    minlength: "Must be at least 5 characters long"
                },
                password: {
                    required:  "Please provide a password",
                    minlength: "Your password must be at least 5 characters long"
                },
                confirmPassword: {
                    required:  "Please provide a password",
                    minlength: "Your password must be at least 5 characters long",
                    equalTo:   "Please enter the same password as above"
                }
            }
        });
    });
}();

var addPlaceValidation = function () {
    $().ready(function() {
        $("#addPlaceForm").validate({
            rules: {
                pname:    "required",
                pcity:    "required",
                padress:  "required",
                pcountry: "required"
            },
            messages: {
                pname:    "Please enter the place name",
                pcity:    "Please enter the place city",
                padress:  "Please enter the place adress",
                pcountry: "Please enter the place country"
            }
        });
    });
}();

var addMeetingValidation = function () {
    $().ready(function() {
        $("#addMeetingForm").validate({
            rules: {
                metitle:     "required",
                medate_time: "required",
                medescription: {
                    required:  true,
                    minlength: 20 
                }
            },
            messages: {
                metitle:     "Please enter the meeting title",
                medate_time: "Please enter the meeting date and time",
                medescription: {
                    required:  "Please enter a meeting description",
                    minlength: "Description must be at least 20 characters long"
                }
            }
        });
    });
}();

var addNewsValidation= function () {
    $().ready(function() {
        $("#NewsForm").validate({
            rules: {
                ntitle: "required",
                ncontent: "required"
            },
            messages: {
                ntitle: "Please enter the news title",
                ncontent: "Please enter news content"
            }
        });
    });
}();

var addPublicationValidation = function () {
    $().ready(function() {
        $("#addPublicationForm").validate({
            rules: {
                ptitle:     "required",
                pcontent_link: "required",
                planguage: "required",
                psubject: "required",
                psource: "required",
                pdate: "required",
                pformat: "required",
                pdescription: {
                    required:  true,
                    minlength: 20 
                }
            },
            messages: {
                ptitle:     "Please enter the publication title",
                pcontent_link: "Please enter the publication link",
                planguage: "Please enter the publication language",
                psubject: "Please enter the publication subject",
                psource: "Please enter the publication source",
                pdate: "Please enter the publication date and time",
                pformat: "Please enter the publication format",
                pdescription: {
                    required:  "Please enter a publication description",
                    minlength: "Description must be at least 20 characters long"
                }
            }
        });
    });
}();

var addVideoValidation = function () {
    $().ready(function() {
        $("#addVideoForm").validate({
            rules: {
                vtitle:     "required",
                vsubject: "required",
                vvideo_link: "required",
                vdescription: {
                    required:  true,
                    minlength: 20 
                }
            },
            messages: {
                vtitle:      "Please enter the video title",
                vsubject:    "Please enter the video subject",
                vvideo_link: "Please enter the video link",
                vdescription: {
                    required:  "Please enter a video description",
                    minlength: "Description must be at least 20 characters long"
                }
            }
        });
    });
}();

var addWorkshopValidation= function () {
  $().ready(function() {
    $("#addWorkshopForm").validate({
      rules: {
      place:      "required",
      wtitle:     "required",
      wdate_time: "required"
      },
      messages: {
      place:      "Please enter the workshop place",
      wtitle:     "Please enter the workshop title",
      wdate_time: "Please enter the workshop date and time"
      }
    });
  });
}();

