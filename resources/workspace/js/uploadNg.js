(function(){
    var app = angular.module('upload', ['angularUUID2', 'ngTagsInput', 'ui.mask']);

    app.controller("PanelController", ['$scope', '$http', 'uuid2', function($scope, $http, uuid2){
            
            //Separate field models
            var guid = uuid2.newuuid();
            
            $scope.singleFields = {
                cid : guid,
                title : '',
                subject : '',
                description : '',
                rights : '',
                date : ''
            };
            
            this.isRequiredEmpty = function (){
                if ($scope.singleFields.title == '')
                    return true;
                if ($scope.singleFields.subject == '')
                    return true;
                if ($scope.singleFields.description == '')
                    return true;
                if ($scope.singleFields.rights == '')
                    return true;
                if ($scope.contributors[0].contributor == "")
                    return true;
                if ($scope.sources[0].source == "")
                    return true;
                if ($scope.singleFields.date == "")
                    return true;
                
                return false;
            };
            
            $scope.fileList;
            
            $scope.jsonCollection = [];
            
            $scope.contributors = [
                {contributor : ""}
            ];
            
            $scope.sources = [
                {source : ""}
            ];
            
            //Optional parameters
            $scope.coverages = [];
            $scope.creators = [];
            $scope.formats = [];
            $scope.identifiers = [];
            $scope.languages = [];
            $scope.relations = [];
            $scope.types = [];
            $scope.urls = [];
            
            
            //Additional field functions
            this.addCoverage = function () {
                $scope.coverages.push({
                    coverage: ""
                });              
            };
            
            this.addCreator = function () {
                $scope.creators.push({
                    creator: ""
                });              
            };
            
            this.addFormat = function () {
                $scope.formats.push({
                    format: ""
                });              
            };
            
            this.addIdentifier = function () {
                $scope.identifiers.push({
                    identifier: ""
                });              
            };
            
            this.addLanguage = function () {
                $scope.languages.push({
                    language: ""
                });              
            };
            
            this.addRelation = function () {
                $scope.relations.push({
                    relation: ""
                });              
            };
            
            this.addType = function () {
                $scope.types.push({
                    type: ""
                });              
            };
            
            this.addURL = function () {
                $scope.urls.push({
                    url: ""
                });              
            };
            
            this.addSource = function () {
                $scope.sources.push({
                    source: ""
                });              
            };
            
            this.addContributor = function () {
                $scope.contributors.push({
                    contributor: ""
                });
            };
            
            //Delete field functions
            this.deleteContributor = function () {
                var lastIndex = $scope.contributors.length - 1;
                if (lastIndex !== 0) {
                    $scope.contributors.splice(lastIndex, 1);
                }
            };
            
            this.deleteSource = function () {
                var lastIndex = $scope.sources.length - 1;
                if (lastIndex !== 0) {
                    $scope.sources.splice(lastIndex, 1);
                }
            };
            
            this.deleteCoverage = function () {
                var lastIndex = $scope.coverages.length - 1;
                if (lastIndex !== -1) {
                    $scope.coverages.splice(lastIndex, 1);
                }
            };
            
            this.deleteCreator = function () {
                var lastIndex = $scope.creators.length - 1;
                if (lastIndex !== -1) {
                    $scope.creators.splice(lastIndex, 1);
                }
            };
            
            this.deleteFormat = function () {
                var lastIndex = $scope.formats.length - 1;
                if (lastIndex !== -1) {
                    $scope.formats.splice(lastIndex, 1);
                }
            };
            
            this.deleteIdentifier = function () {
                var lastIndex = $scope.identifiers.length - 1;
                if (lastIndex !== -1) {
                    $scope.identifiers.splice(lastIndex, 1);
                }
            };
            
            this.deleteLanguage = function () {
                var lastIndex = $scope.languages.length - 1;
                if (lastIndex !== -1) {
                    $scope.languages.splice(lastIndex, 1);
                }
            };
            
            this.deleteRelation = function () {
                var lastIndex = $scope.relations.length - 1;
                if (lastIndex !== -1) {
                    $scope.relations.splice(lastIndex, 1);
                }
            };
            
            this.deleteType = function () {
                var lastIndex = $scope.types.length - 1;
                if (lastIndex !== -1) {
                    $scope.types.splice(lastIndex, 1);
                }
            };
            
            this.deleteURL = function () {
                var lastIndex = $scope.urls.length - 1;
                if (lastIndex !== -1) {
                    $scope.urls.splice(lastIndex, 1);
                }
            };
            
            //Tag handling
            $scope.tags = []; 
            
            $scope.addTags = function (items) {
                $scope.tags.push(items);
            };
            
            $scope.loadTags = function () {
                return tagAutoComplete; // used to autopopulate pre-existing tags from the database
            };
            
            //Dynamic tab handling 
            this.tab = 1;
            this.selectTab = function(setTab) {
                this.tab = setTab;
            };
            this.isSelected = function(checkTab){
                return this.tab === checkTab;
            };
            
            
            //Assemble Full JSON
            
            this.assembleJSON = function () {
                $scope.fileList = files;
                $scope.jsonCollection = [];
                var singleFields_copy = angular.copy($scope.singleFields);
                $scope.jsonCollection.push(singleFields_copy);
                
                for (file in $scope.fileList)
                    $scope.jsonCollection.push({'file' : $scope.fileList[file]});
                
                for (tag in $scope.tags)
                    $scope.jsonCollection.push($scope.tags[tag]);
            
                for (contributor in $scope.contributors)
                    $scope.jsonCollection.push($scope.contributors[contributor]);
                
                for (source in $scope.sources)
                    $scope.jsonCollection.push($scope.sources[source]);
            
                for (coverage in $scope.coverages)
                    $scope.jsonCollection.push($scope.coverages[coverage]);
            
                for (creator in $scope.creators)
                    $scope.jsonCollection.push($scope.creators[creator]);
            
                for (format in $scope.formats)
                    $scope.jsonCollection.push($scope.formats[format]);
            
                for (identifier in $scope.identifiers)
                    $scope.jsonCollection.push($scope.identifiers[identifier]);
            
                for (language in $scope.languages)
                    $scope.jsonCollection.push($scope.languages[language]);
            
                for (relation in $scope.relations)
                    $scope.jsonCollection.push($scope.relations[relation]);
                
                for (type in $scope.types)
                    $scope.jsonCollection.push($scope.types[type]);
                
                for (url in $scope.urls)
                    $scope.jsonCollection.push($scope.urls[url]);
            };
            
            
            this.submitCollection = function (target_url, redirect_url){
                //var collection_json = $scope.jsonCollection;
                this.assembleJSON();
                $scope.redirect_url = redirect_url;
                console.log(redirect_url);
                $http({method: 'POST', url: target_url, data: $scope.jsonCollection}).
                        success(function(data) {
                            if(data){
                                data = data.trim();
                                if (data === 'success'){
                                    topNoty('success', 'Collection Uploaded Successfully');
                                    url = $scope.redirect_url;
                                    setTimeout('window.location = url', 3000);
                                    //console.log(data);
                                }
                                else{
                                    topNoty('error', 'Oops! Server Error Occurred');
                                }
                            }
                }).error(function(data) {
                    $.noty.closeAll();
                    topNoty('error', 'Oops! Server Error Occurred');
                });
            };            
            
        }]);
    
})();