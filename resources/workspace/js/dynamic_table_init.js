function fnFormatDetails ( oTable, nTr, rowIndex )
{
    var aData = oTable.fnGetData( nTr );
    var sOut = '<h3>Resource Metadata</h3>';
    sOut += '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
    $.each(resources[rowIndex], function (k, v){    //need better way to get the index
        sOut += '<tr><td>'+k+': '+v+'</td></tr>';
    });
    
    sOut += '</table>';
    return sOut;
}


$(document).ready(function() {

    //alert(JSON.stringify(resources));
    /*
     * Insert a 'details' column to the table
     */
    var nCloneTh = document.createElement( 'th' );
    var nCloneTd = document.createElement( 'td' );
    //nCloneTd.innerHTML = '<img src="http://localhost/elseweb-water/resources/workspace/img/details_open.png">';
    nCloneTd.innerHTML = '<img src="'+ baseurl +'resources/workspace/img/details_open.png">';
    nCloneTd.className = "center";

    $('#hidden-table-info thead tr').each( function () {
        this.insertBefore( nCloneTh, this.childNodes[0] );
    } );

    $('#hidden-table-info tbody tr').each( function () {
        this.insertBefore(  nCloneTd.cloneNode( true ), this.childNodes[0] );
    } );


    /*
     * Initialse DataTables, with no sorting on the 'details' column
     */
    var oTable = $('#hidden-table-info').dataTable( {
        "aoColumnDefs": [
            { "bSortable": false, "aTargets": [ 0 ] }
        ],
        "aaSorting": [[4, 'desc']]
    });

    /* Add event listener for opening and closing details
     * Note that the indicator for showing which row is open is not controlled by DataTables,
     * rather it is done here
     */
    
    $(document).on('click','#hidden-table-info tbody td img',function () {
        var nTr = $(this).parents('tr')[0];
        var rowIndex = oTable.fnGetPosition( $(this).closest('tr')[0] );
        if ( oTable.fnIsOpen(nTr) )
        {
            /* This row is already open - close it */
            //this.src = "http://localhost/elseweb-water/resources/workspace/img/details_open.png";
            this.src = baseurl + "resources/workspace/img/details_open.png";
            oTable.fnClose( nTr );
        }
        else
        {
            /* Open this row */
            this.src = baseurl + "resources/workspace/img/details_close.png";
            oTable.fnOpen( nTr, fnFormatDetails(oTable, nTr, rowIndex), 'details' );
        }
    } );
} );