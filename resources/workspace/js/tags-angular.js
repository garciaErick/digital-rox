/*
 * @author Oliver Sindayigaya & Jesus Medrano
 * Framework: Angular JS
 * Module Description: New Scenario GUI metadata field validation.
 * View: newscenario.php (see file head for js dependencies)
 */

/* global angular, segment, scenario, tagAutoComplete */

// Uses the ngTagsInput.js file
var app = angular.module('ngTags', ['ngTagsInput']);
app.controller('tagControl', function ($scope) {
    
    $scope.tags = []; 
    
    $scope.addTags = function (items) {
        $scope.tags.push(items);
    };
    
    $scope.loadTags = function () {
        return tagAutoComplete; // used to autopopulate pre-existing tags from the database
    };
    
});   