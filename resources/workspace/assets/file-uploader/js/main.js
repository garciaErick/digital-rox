/*
 * jQuery File Upload Plugin JS Example 8.9.1
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* global $, window */

$(function () {
    'use strict';
    
    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
        // Uncomment the following to send cross-domain cookies:
        url: baseurl + 'resources/workspace/assets/file-uploader/server/php/',
        formData: {
            param1: $('#uploadID').val()
        }
    });
    
    $('#fileupload').bind('fileuploaddone', function (e, data) {
        $.each(data.result.files, function (index, file) {
            //console.log(file.name);
            files.push(file.name);
        });
    });
    
    $('#fileupload').bind('fileuploaddestroy', function (e, data) {
        var filename = data.url.substring(data.url.indexOf("=") + 1);
        //console.log(data);
        //console.log(filename);
        files.pop(filename);
    });

    if (window.location.hostname === 'blueimp.github.io') {

        // Demo settings:
        $('#fileupload').fileupload('option', {
            url: baseurl + 'resources/workspace/assets/file-uploader/server/php/',
            // Enable image resizing, except for Android and Opera,
            // which actually support image resizing, but fail to
            // send Blob objects via XHR requests:
            disableImageResize: /Android(?!.*Chrome)|Opera/
                .test(window.navigator.userAgent),
            maxFileSize: 5000000,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
        });
        // Upload server status check for browsers with CORS support:
        if ($.support.cors) {
            $.ajax({
                url: baseurl + 'resources/workspace/assets/file-uploader/server/php/',
                type: 'HEAD'
            }).fail(function () {
                $('<div class="alert alert-danger"/>')
                    .text('Upload server currently unavailable - ' +
                            new Date())
                    .appendTo('#fileupload');
            });
        }
    } else {

        // Load existing files:
        $('#fileupload').addClass('fileupload-processing');
        var location = $('#fileupload').fileupload('option', 'url');
        //var location = $('#fileupload').fileupload('option', 'url')+$('#uploadID').val();
        //alert(location);
        $.ajax({
            // Uncomment the following to send cross-domain cookies:
            xhrFields: {withCredentials: true},
            url: location,
            dataType: 'json',
            context: $('#fileupload')[0]
        }).always(function () {
            $(this).removeClass('fileupload-processing');
        }).done(function (result) {
            $(this).fileupload('option', 'done')
                .call(this, $.Event('done'), {result: result});
        });
    }

});
