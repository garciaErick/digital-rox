/* File: main.js
 * Author: Luis Garnica
 * View Dependant: All
 * Description: Javascript functions available for all views.
 *			  -> User Login Handling
 *			  -> Noty plugin handler functions (general notifications).
 *  
 *  */


//Global variables

var files = Array();


/* User Login functions */

function userLogin(base_url) {
  var email = $('#InputEmail').val();
  var password = $('#InputPassword').val();
  $.ajax({
    'url': base_url + 'user/login',
    'type': 'POST', //the way you want to send data to your URL
    'data': 'email=' + email + '&password=' + password,
    'success': function (result) { //probably this request will return anything, it'll be put in var "result"
      if (result) {
        if (result === 'success') {
          topNoty('success', 'Login Successful!');
          url = base_url + 'workspace/dashboard';
          //setTimeout('location.reload()', 2900);
          setTimeout('window.location = url', 3000);
        }
        else
    topNoty('warning', result);
      }
      else
    topNoty('error', 'An error has ocurred.');
    }
  });
}


/*Functions for the workspace/userAdmin Controller*/

function createUser(base_url) {
  var firstName = $('#firstName').val();
  var lastName  = $('#lastName').val();
  var email     = $('#email').val();
  var password  = $('#password').val();
  var passConf  = $('#confirmPassword').val();
  var startDate = $('#startDate').val();
  var isUserAdmin;
  var isCollaborator;
  var isContentManager;
  var isStudent;
  var isUSDAOfficial;

  //Optional values
  var picture     = $('#picture').val();
  var institution = $('#institution').val();
  var position    = $('#position').val();
  var department  = $('#department').val();
  var linkedin    = $('#linkedin').val();
  var website     = $('#website').val();
  var team        = $('#team').val();
  var area        = $('#area').val();
  var education   = $('#education').val();
  var activities  = $('#activities').val();

  //Checkbox inputs
  if ($('#isUserAdmin').is(':checked'))
    isUserAdmin = 1;
  else
    isUserAdmin = 0;

  if ($('#isCollaborator').is(':checked'))
    isCollaborator = 1;
  else
    isCollaborator = 0;

  if ($('#isContentManager').is(':checked'))
    isContentManager = 1;
  else
    isContentManager = 0;

  if ($('#isStudent').is(':checked'))
    isStudent = 1;
  else
    isStudent = 0;

  if ($('#isUSDAOfficial').is(':checked'))
    isUSDAOfficial = 1;
  else
    isUSDAOfficial = 0;


  $.ajax({
    'url': base_url + 'workspace/userAdmin/createUserFunction', //controller function
    'type': 'POST', //method
    'data': 'firstName=' + firstName + '&lastName=' + lastName + '&email=' + email + '&password=' + password +
    '&passConf=' + passConf + '&isUserAdmin=' + isUserAdmin + '&picture=' + picture + '&institution=' + institution
    + '&position=' + position + '&department=' + department + '&linkedin=' + linkedin + '&website=' + website
    + '&team=' + team + '&area=' + area + '&isCollaborator=' + isCollaborator + '&isContentManager=' + isContentManager
    + '&isStudent=' + isStudent + '&isUSDAOfficial=' + isUSDAOfficial + '&education=' + education + '&activities=' + activities
    + '&startDate=' + startDate,
    'success': function (result) { //probably this request will return anything, it'll be put in var "result"
      if (result) {
        if (result === 'success') {
          topNoty('success', 'User Creation Successful!');
          url = base_url + 'workspace/userAdmin/createUser';
          setTimeout('location.reload()', 2900);
          //					setTimeout('window.location = url', 3000);
        }
        else
    topNoty('warning', result);
      }
      else
    topNoty('error', 'An error has ocurred.');
    }
  });
}

function updateInformation(base_url) {
  var uid         = $('#uid').val();
  var firstName   = $('#firstName').val();
  var lastName    = $('#lastName').val();
  var email       = $('#email').val();
  var picture     = $('#picture').val();
  var institution = $('#institution').val();
  var position    = $('#position').val();
  var department  = $('#department').val();
  var linkedin    = $('#linkedin').val();
  var website     = $('#website').val();
  var team        = $('#team').val();
  var area        = $('#area').val();
  var education   = $('#education').val();   
  var activities  = $('#activities').val(); 
  var startDate   = $('#startDate').val(); 
  var endDate     = $('#endDate').val(); 

  $.ajax({
    'url': base_url + 'workspace/userAdmin/updateInformationFunction',
    'type': 'POST', //the way you want to send data to your URL
    'data': 'firstName=' + firstName + '&lastName=' + lastName + '&email=' + email + '&picture=' + picture
    + '&institution=' + institution + '&position=' + position + '&department=' + department 
    + '&linkedin=' + linkedin + '&website=' + website + '&team=' + team + '&area=' + area
    + '&education=' + education + '&activities=' + activities + '&startDate=' + startDate
    + '&endDate=' + endDate +'&uid=' + uid,
    'success': function (result) { //probably this request will return anything, it'll be put in var "result"
      if (result) {
        if (result === 'success') {
          topNoty('success', 'Updated User Information');
          url = base_url + 'workspace/dashboard';
          //setTimeout('location.reload()', 2900);
          setTimeout('window.location = url', 3000);
        }
        else
    topNoty('warning', result);
      }
      else
    topNoty('error', 'An error has ocurred.');
    }
  });
}

function changePassword(base_url) {
  var oldPassword = $('#oldPassword').val();
  var password    = $('#password').val();
  var passConf    = $('#confirmPassword').val();

  $.ajax({
    'url': base_url + 'workspace/userAdmin/changePasswordFunction',
    'type': 'POST', //the way you want to send data to your URL
    'data': 'oldPassword=' + oldPassword + '&password=' + password + '&passConf=' + passConf,
    'success': function (result) { //probably this request will return anything, it'll be put in var "result"
      if (result) {
        if (result === 'success') {
          topNoty('success', 'Password Changed Successfully');
          url = base_url + 'workspace/dashboard';
          //setTimeout('location.reload()', 2900);
          setTimeout('window.location = url', 3000);
        }
        else
    topNoty('warning', result);
      }
      else
    topNoty('error', 'An error has ocurred.');
    }
  });
}

function finishUpload(base_url) {
  $.ajax({
    'url': base_url + 'workspace/collections/finishUpload',
  'type': 'POST',
  'data': 'files=' + files,//the way you want to send data to your URL
  'success': function (result) { //probably this request will return anything, it'll be put in var "result"
    if (result) {
      result = result.trim();
      if (result === 'success') {
        topNoty('success', 'Successfully Uploaded and Inserted Metadata');
        url = base_url + 'workspace/dashboard';
        setTimeout('window.location = url', 3000);
      }
      else
    topNoty('warning', result);
    }
    else
    topNoty('error', 'An error has ocurred.');
  }
  });
}

function uploadMetadata(base_url) {
  $(function () {
    $('#default').stepy({
      backLabel: 'Previous',
    block: true,
    nextLabel: 'Next',
    titleClick: true,
    titleTarget: '.stepy-tab'
    });
  });
  var title        = $('#title').val();
  var contributors = $('#contributors').val();
  var subject      = $('#subject').val();
  var source       = $('#source').val();
  var date         = $('#date').val();
  var description  = $('#description').val();
  var rights       = $('#rights').val();
  var tags         = $('#tags').val();
  var coverage     = $('#coverage').val();
  var creator      = $('#creator').val();
  var format       = $('#format').val();
  var identifier   = $('#identifier').val();
  var language     = $('#langauge').val();
  var relation     = $('#relation').val();
  var type         = $('#type').val();

  $.ajax({
    'url': base_url + 'workspace/collections/uploadFunction',
    'type': 'POST', //the way you want to send data to your URL 'data': 'title=' + title + '&contributors=' + contributors +'&subject=' + subject + '&source=' + source + '&date=' + date + '&description=' + description + '&rights=' + rights + '&tags=' + tags + '&coverage=' + coverage + '&creator=' + creator + '&format=' + format + '&identifier=' + identifier + '&language=' + language + '&relation=' + relation + '&type=' + type,
    'success': function (result) { //probably this request will return anything, it'll be put in var "result"
      if (result) {
        if (result === 'success') {
          topNoty('success', 'You can now proceed to uploading your files');
          url = base_url + 'workspace/collections/upload';
          //setTimeout('location.reload()', 2900);
          setTimeout('window.location = url', 3000);
        }
        else
    topNoty('warning', result);
      }
      else
    topNoty('error', 'An error has ocurred.');
    }
  });
}

function deleteUser(base_url) {
  var multiple = new Array();
  $.each($("input[name='multiple[]']:checked"), function () {
    multiple.push($(this).val());
  })
  $.ajax({
    'url': base_url + 'workspace/userAdmin/deleteUserFunction',
    'type': 'POST', //the way you want to send data to your URL
    'data': 'multiple=' + multiple,
    'success': function (result) { //probably this request will return anything, it'll be put in var "result"
      if (result) {
        if (result === 'success') {
          topNoty('success', 'Successfully Deleted User');
          url = base_url + 'workspace/userAdmin/deleteUser';
          //					setTimeout('location.reload()', 2900);
          setTimeout('window.location = url', 3000);
        }
        else
    topNoty('warning', result);
      }
      else
    topNoty('error', 'An error has ocurred.');
    }
  });
}

//Function to set bookmark
function setBookmark(base_url, cid){
  $.ajax({
    'url': base_url + 'workspace/bookmark/setBookmark',
  'type': 'GET', 
  'data': 'cid=' + cid,
  'success': function (result) { 
    if (result) {
      if (result === 'success') {
        topNoty('success', 'Collection added to bookmarks');
      }
      else
    topNoty('warning', result);
    }
    else
    topNoty('error', 'An error has ocurred.');
  }
  });
}

//Function to unset bookmark
function unsetBookmark(base_url, cid){
  $.ajax({
    'url': base_url + 'workspace/bookmark/unsetBookmark',
  'type': 'GET', 
  'data': 'cid=' + cid,
  'success': function (result) { 
    if (result) {
      if (result === 'success') {
        url = base_url + 'workspace/bookmark';
        setTimeout('window.location = url', 300);
      }
      else
    topNoty('warning', result);
    }
    else
    topNoty('error', 'An error has ocurred.');
  }
  }); 
}

function addMeeting2(base_url) {
    var pname         = $('#place').val();
    var metitle       = $('#metitle').val();
    var medescription = $('#medescription').val();
    var medate_time   = $('#medate_time').val();
    var meetingAttachment = "n/a";

    $.ajax({
        'url':  base_url + 'workspace/meetings/addMeetingFunction',
        'type': 'POST', //the way you want to send data to your URL
        'data': 'metitle=' + metitle + '&medescription=' + medescription + '&medate_time=' +
                medate_time + '&meetingAttachment=' + meetingAttachment + '&pname=' + pname,
        'success': function (result) { //probably this request will return anything, it'll be put in var "result"
            if (result) {
                if (result === 'success') {
                    topNoty('success', 'Meeting added successfully!');
                    url = base_url + 'workspace/meetings/viewMeeting';
                    //setTimeout('location.reload()', 2900);
                    setTimeout('window.location = url', 3000);
                }
                else
                    topNoty('warning', result);
            }
            else
                topNoty('error', 'An error has ocurred.');
        }
    });
}

function addQuery(base_url) {
  var qstatement = $('#qstatement').val();
  var qcomments  = $('#qcomments').val();
  
  $.ajax({
    'url':     base_url + 'workspace/query/addQueryFunction',
    'type':    'POST', //the way you want to send data to your URL
    'data':    'qstatement=' + qstatement + '&qcomments=' + qcomments,
    'success': function (result) { //probably this request will return anything, it'll be put in var "result"
      if (result) {
        if (result === 'success') {
          topNoty('success', 'Query added successfully!');
          url = base_url + 'workspace/query/addQuery';
          //setTimeout('location.reload()', 2900);
          setTimeout('window.location = url', 3000);
        }
        else
    topNoty('warning', result);
      }
      else
    topNoty('error', 'An error has ocurred.');
    }
  });
}

function editNewsArticle(base_url, nid) {
  var ntitle   = $('#ntitle').val();
  var ncontent = CKEDITOR.instances.ncontent.getData();
  
  $.ajax({
    'url':     base_url + 'workspace/news/editNewsArticleFunction',
    'type':    'POST', //the way you want to send data to your URL
    'data':    'ntitle=' + ntitle + '&ncontent=' + ncontent + '&nid=' + nid,
    'success': function (result) { //probably this request will return anything, it'll be put in var "result"
      if (result) {
        if (result === 'success') {
          topNoty('success', 'News article updated successfully!');
          url = base_url + 'workspace/news/editNewsArticle/' + nid;
          setTimeout('window.location = url', 3000);
        }
        else
    topNoty('warning', result);
      }
      else
    topNoty('error', 'An error has ocurred.');
    }
  });
}

function addPlace(base_url) {
  var type     = $('#type').val();
  var pname    = $('#pname').val();
  var pcity    = $('#pcity').val();
  var padress  = $('#padress').val();
  var pcountry = $('#pcountry').val();
  var pwebsite = $('#pwebsite').val();
  var pcomments = $('#pcomments').val();
  $.ajax({
    'url': base_url + 'workspace/places/addPlaceFunction',
    'type': 'POST', //the way you want to send data to your URL
    'data': 'pcity=' + pcity + '&padress=' + padress + '&pcountry=' +
    pcountry + '&pwebsite=' + pwebsite + '&pname=' + pname + '&pcomments' + pcomments,
    'success': function (result) { //probably this request will return anything, it'll be put in var "result"
      if (result) {
        if (result === 'success') {
          topNoty('success', 'Place added successfully!');
          if(type == 'workshop'){ url = base_url + 'workspace/workshops/addWorkshop'; }
          if(type == 'meeting' ){ url = base_url + 'workspace/meetings/addMeeting';   }
  setTimeout('window.location = url', 3000);
        }
        else
    topNoty('warning', result);
      }
      else
    topNoty('error', 'An error has ocurred.');
    }
  });
}

function addWorkshop(base_url) {
  var pname             = $('#place').val();
  var wtitle            = $('#wtitle').val();
  var wdescription      = $('#wdescription').val();
  var wdate_time        = $('#wdate_time').val();
  var meetingAttachment = $('#meetingAttachment').val();
  var wpresenter        = $('#wpresenter').val();
  var winstitution      = $('#winstitution').val();
  var wtype             = $('#wtype').val();
  
  $.ajax({
    'url': base_url + 'workspace/workshops/addWorkshopFunction',
    'type': 'POST', //the way you want to send data to your URL
    'data': 'wtitle=' + wtitle + '&wdescription=' + wdescription + '&wdate_time=' +
    wdate_time + '&meetingAttachment=' + meetingAttachment + '&pname=' + pname
    + '&wpresenter=' + wpresenter + '&winstitution=' + winstitution + '&wtype=' + wtype,
    'success': function (result) { //probably this request will return anything, it'll be put in var "result"
      if (result) {
        if (result === 'success') {
          topNoty('success', 'Event added successfully!');
          url = base_url + 'workspace/workshops/addWorkshop';
          setTimeout('window.location = url', 3000);
        }
        else
    topNoty('warning', result);
      }
      else
    topNoty('error', 'An error has ocurred.');
    }
  });
}

function editWorkshopjs(base_url, wid) {
    var pname             = $('#place').val();
    var wtitle            = $('#wtitle').val();
    var wdescription      = $('#wdescription').val();
    var wdate_time        = $('#wdate_time').val();
    var meetingAttachment = $('#meetingAttachment').val();
    var wpresenter        = $('#wpresenter').val();
    var winstitution      = $('#winstitution').val();
    var wtype             = $('#wtype').val();
  
    $.ajax({
        'url' :  base_url + 'workspace/workshops/editWorkshopFunction',
        'type': 'POST', //the way you want to send data to your URL
        'data': 'wtitle=' + wtitle + '&wdescription=' + wdescription + '&wdate_time=' +
                wdate_time + '&meetingAttachment=' + meetingAttachment + '&pname=' + pname + '&wid=' + wid
                + '&wpresenter=' + wpresenter + '&winstitution=' + winstitution + '&wtype=' + wtype,
        'success': function (result) { //probably this request will return anything, it'll be put in var "result"
            if (result) {
                if (result === 'success') {
                    topNoty('success', 'Event updated successfully!');
                    url = base_url + 'workspace/workshops/editWorkshops';
                    setTimeout('window.location = url', 3000);
                }
                else
                    topNoty('warning', result);
            }
            else
                topNoty('error', 'An error has ocurred.');
        }
    });
}




function addPublication(base_url) {
  var ptitle        = $('#ptitle').val();
  var pdescription  = $('#pdescription').val();
  var pcontent_link = $('#pcontent_link').val();
  var planguage     = $('#planguage').val();
  var psubject      = $('#psubject').val();
  var psource       = $('#psource').val();
  var pdate         = $('#pdate').val();
  var pformat       = $('#pformat').val();
  $.ajax({
    'url': base_url + 'workspace/publications/addPublicationFunction',
    'type': 'POST', //the way you want to send data to your URL
    'data': 'ptitle=' + ptitle + '&pdescription=' + pdescription + '&pcontent_link=' +
    pcontent_link + '&planguage=' + planguage + '&psubject=' + psubject
    + '&psource=' + psource + '&pdate=' + pdate + '&pformat=' + pformat,
    'success': function (result) { //probably this request will return anything, it'll be put in var "result"
      if (result) {
        if (result === 'success') {
          topNoty('success', 'Publication added successfully!');
          url = base_url + 'workspace/publications/addPublication';
          //setTimeout('location.reload()', 2900);
          setTimeout('window.location = url', 3000);
        }
        else
    topNoty('warning', result);
      }
      else
    topNoty('error', 'An error has ocurred.');
    }
  });
}

function editPublicationjs(base_url, pid) {
  var ptitle        = $('#ptitle').val();
  var pdescription  = $('#pdescription').val();
  var pcontent_link = $('#pcontent_link').val();
  var planguage     = $('#planguage').val();
  var psubject      = $('#psubject').val();
  var psource       = $('#psource').val();
  var pdate         = $('#pdate').val();
  var pformat       = $('#pformat').val();
  $.ajax({
    'url':     base_url + 'workspace/publications/editPublicationFunction',
    'type':    'POST', //the way you want to send data to your URL
    'data': 'ptitle=' + ptitle + '&pdescription=' + pdescription + '&pcontent_link=' +
    pcontent_link + '&planguage=' + planguage + '&psubject=' + psubject
    + '&psource=' + psource + '&pdate=' + pdate + '&pformat=' + pformat + '&pid=' + pid,
    'success': function (result) { //probably this request will return anything, it'll be put in var "result"
      if (result) {
        if (result === 'success') {
          topNoty('success', 'Publication updated successfully!');
          url = base_url + 'workspace/publications/editPublicationView?pid=' + pid;
          // workspace/videos/editVideoInfo?vid=$row->vid
          setTimeout('window.location = url', 3000);
        }
        else
    topNoty('warning', result);
      }
      else
    topNoty('error', 'An error has ocurred.');
    }
  });
}

function addVideo(base_url) {
  var vtitle          = $('#vtitle').val();
  var vdescription    = $('#vdescription').val();
  var vsubject        = $('#vsubject').val();
  var vvideo_link     = $('#vvideo_link').val();
  var vadditional_url = $('#vadditional_url').val();
  $.ajax({
    'url': base_url + 'workspace/videos/addVideoFunction',
    'type': 'POST', //the way you want to send data to your URL
    'data': 'vtitle=' + vtitle + '&vdescription=' + vdescription + '&vsubject=' +
    vsubject + '&vvideo_link=' + vvideo_link + '&vadditional_url=' + vadditional_url,
    'success': function (result) { //probably this request will return anything, it'll be put in var "result"
      if (result) {
        if (result === 'success') {
          topNoty('success', 'Video added successfully!');
          url = base_url + 'workspace/videos/addVideo';
          //setTimeout('location.reload()', 2900);
          setTimeout('window.location = url', 3000);
        }
        else
    topNoty('warning', result);
      }
      else
    topNoty('error', 'An error has ocurred.');
    }
  });
}

function editVideojs(base_url, vid) {
  var vtitle          = $('#vtitle').val();
  var vdescription    = $('#vdescription').val();
  var vsubject        = $('#vsubject').val();
  var vvideo_link     = $('#vvideo_link').val();
  var vadditional_url = $('#vadditional_url').val();
  $.ajax({
    'url':     base_url + 'workspace/videos/editVideoFunction',
    'type':    'POST', //the way you want to send data to your URL
    'data': 'vtitle=' + vtitle + '&vdescription=' + vdescription + '&vsubject=' +
    vsubject + '&vvideo_link=' + vvideo_link + '&vadditional_url=' + vadditional_url + '&vid=' + vid,
    'success': function (result) { //probably this request will return anything, it'll be put in var "result"
      if (result) {
        if (result === 'success') {
          topNoty('success', 'Video updated successfully!');
          url = base_url + 'workspace/videos/editVideoInfo?vid=' + vid;
          // workspace/videos/editVideoInfo?vid=$row->vid
          setTimeout('window.location = url', 3000);
        }
        else
    topNoty('warning', result);
      }
      else
    topNoty('error', 'An error has ocurred.');
    }
  });
}

/* Functions: topNoty, middleNoty
 * Description: Notification Handler receives type (warning, error or success) and text to display 
 * notification will popup on the middle top of the current loaded webpage.
 * */

function topNoty(type, text) {
    noty({
        layout: 'top',
        type: type,
        text: text,
        dismissQueue: true,
        animation: {
            open: {height: 'toggle'},
            close: {height: 'toggle'},
            easing: 'swing',
            speed: 650
        },
        timeout: 1900
    });
}

function middleNoty(type, text) {
    noty({
        layout: 'center',
        type: type,
        text: text,
        dismissQueue: false,
        animation: {
            open: {height: 'toggle'},
            close: {height: 'toggle'},
            easing: 'swing',
            speed: 650
        }
    });
}

function reset()
{
    $('#myFormular').children('input:text, textarea').each(function ()
    {
        $(this).fadeOut('slow', function ()
        {
            $(this).val('');
            $(this).fadeIn('slow');
        });
    });
}


$('#resetButton').click(reset);
