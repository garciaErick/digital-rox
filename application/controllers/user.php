<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* File: user.php (controller)
 * Author: Luis Garnica
 * View Dependant: dashboard
 * Description: Validates and handles user login, validation and management.
 *  */

class User extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('user_model');
		$this->load->library(array('session', 'form_validation'));
		$this->load->helper(array('url', 'form'));
		$this->load->database('default');
	}

	/* User session Signin and Signout functions */
	/*
	 * Function: login
	 * Description: Workspace login function. Via email and password.
	 * */

	public function login() {
		$this->form_validation->set_rules('email', 'Email', 'required|trim|min_length[2]|max_length[150]|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[5]|max_length[150]|xss_clean');

		//throw error messages if we have any
		if ($this->form_validation->run() == FALSE) {
			echo validation_errors();
		} else {
			$email     = $this->input->post('email');
			$password  = sha1($this->input->post('password'));
      /* $password  = $this->input->post('password'); */
			$user_info = $this->user_model->login($email, $password);
			if ($user_info == TRUE) {
				$data = array(
					'is_logged_in'      => TRUE,
					'uid'               => $user_info->uid,
					'is_admin'          => $user_info->is_admin,
					'email'             => $user_info->uemail
				);
				$this->session->set_userdata($data);
				echo "success";
			}
		}
	}

	/*
	 * Function: logout
	 * Description: Destroys all session variables and resets to home page.
	 * */

	public function logout() {
		$this->session->sess_destroy();
		$url = site_url();
		header('Location: ' . $url);
	}

}

?>
