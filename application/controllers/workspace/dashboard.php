<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller {
    
    
    public function __construct() {
        parent::__construct();
        $this->load->library(array('session', 'form_validation'));
    }
    
    public function index($renderData=""){  
        $this->load->model('resource_model'); 
        if($this->session->userdata('is_logged_in')){
            $this->title = "Water | Workspace";
            $folder = 'workspace';
            $this->_render('workspace/dashboard',$renderData, $folder);   
        }
        else{
            $this->title = "Water | Home";
            $folder = 'template';
            $this->_render('pages/home',$renderData, $folder);
        }

    }
    
}
