<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ResourceDetails extends MY_Controller {
    
    
    public function __construct() {
        parent::__construct();
        $this->load->model('resource_model');
        $this->load->library(array('session', 'form_validation'));
        $this->load->helper(array('url', 'form'));
        $this->load->database('default');
    }
    
    public function index($renderData=""){  
        if ($this->session->userdata('is_logged_in')) {
            //Get method for rid
            $cid = $this->input->get('rid', TRUE);
            //Database queries
            $resource_raw = $this->resource_model->getResourceById($cid); 
            $resource_files = $this->resource_model->getResourceFiles($cid);
            //Process raw resource data into columns
            $oldcid = -1;
            $i=0;
            $index = -1;
            $table = null;
            foreach ($resource_raw->result() as $row){
                $cid2 = $row->cid;
               
                if ($oldcid != $cid2){
                    $oldcid = $cid2;
                    $index ++;
                    $table[$index]['User_ID'] = $row->uid;
                    $table[$index]['Uploader'] = $row->name;
                    $table[$index]['Upload_Date'] = $row->cupload_date;
                    $table[$index]['Resource_ID'] = $row->cid;
                    $table[$index][$i.$row->mlabel] = $row->cm_data; 
                }
                else{
                    $table[$index][$i.$row->mlabel] = $row->cm_data; 
                }
                $i++;
            }
            
            //Set data variables for the view
            $this->data['resource'] = $table;
            $this->data['files'] = $resource_files;
            $this->load->helper('nav_helper');
            
            $this->title = "Water | Workspace";
            $folder = 'workspace';
            $this->_render('workspace/resourcedetails', $renderData, $folder);
            
           /* for debugging
            print_r($resource_raw->result());
            echo '<br><br>';
            print_r($resource_files->result());
            echo '<br><br>';
            print_r($table);  */
           
        } else {
            $this->title = "Water | Home";
            $folder = 'template';
            $this->_render('pages/home', $renderData, $folder);
        }
    }
    
}