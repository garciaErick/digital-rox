<?php

if (!defined('BASEPATH'))
  exit('No direct script access allowed');

class Workshops extends MY_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model('workshops_model');
    $this->load->model('places_model');
    $this->load->library(array('session', 'form_validation'));
    $this->load->helper(array('url', 'form'));
    $this->load->database('default');
  }


  public function addWorkshop($renderData = "") {
    if (($this->session->userdata('is_logged_in')) && ($this->session->userdata('is_contentmanager') == 1)) {
      $this->title = "Water | Workspace";
      $folder = 'workspace';
      $data['places'] = $this->places_model->getAll();
      $this->load->view('workspace/addWorkshop', $data,       TRUE);
      $this->_render(   'workspace/addWorkshop', $renderData, $folder);
    } else {
      $this->load->view('workspace/denied');
    }
  }

  public function addWorkshopFunction(){
    //User validation check
    if (($this->session->userdata('is_logged_in')) && ($this->session->userdata('is_contentmanager') == 1)){
      //Do nothing
    } 
    else {
      $this->load->view('workspace/denied');
    }      
    $this->form_validation->set_rules('pname',        'Place Name',           'required|min_length[2]|max_length[150]|xss_clean');
    $this->form_validation->set_rules('wtitle',       'Event Title',         'required|min_length[2]|max_length[150]|xss_clean');
    $this->form_validation->set_rules('wdescription', 'Event Description',   'required|min_length[2]|xss_clean');
    $this->form_validation->set_rules('wdate_time',   'Event Date and Time', 'required|min_length[2]|max_length[150]|xss_clean');
    $this->form_validation->set_rules('wpresenter',   'Event Presenter', 'min_length[2]|max_length[100]|xss_clean');
    $this->form_validation->set_rules('winstitution',   'Presenter Institution', 'min_length[2]|max_length[60]|xss_clean');
    $this->form_validation->set_rules('wtype',   'Event Type', 'required|min_length[2]|max_length[50]|xss_clean');
    
    if ($this->form_validation->run() == FALSE) {	
      echo validation_errors();
    }
    else {
      $pname        = $this->input->post('pname');
      $pid_FK       = $this->places_model->getPid($pname);
      $uid_FK       = $this->session->userdata('uid');
      $wtitle       = $this->input->post('wtitle');
      $wdescription = $this->input->post('wdescription');
      $wdate_time   = $this->input->post('wdate_time');
      $wpresenter   = $this->input->post('wpresenter');
      $winstitution = $this->input->post('winstitution');
      $wtype        = $this->input->post('wtype');
      
      
      $newWorkshop  = array(
        'pid_FK'       => $pid_FK,
        'uid_FK'       => $uid_FK,
        'wtitle'       => $wtitle,
        'wdescription' => $wdescription,
        'wdate_time'   => $wdate_time,
        'wpresenter'   => $wpresenter,
        'winstitution' => $winstitution,
        'wtype'        => $wtype,
      );
      $this->workshops_model->addWorkshop($newWorkshop);
      echo 'success';
    }
  }

  public function editWorkshops($renderData = "") {

    if (($this->session->userdata('is_logged_in')) && ($this->session->userdata('is_contentmanager') == 1)) {
      $this->title = "Water | Workspace";
      $folder = 'workspace';
      $this->data['workshops']  =  $this->workshops_model->getAll();
      $this->data['places'] = $this->places_model->getAll();
      $this->_render('workspace/editWorkshops', $renderData, $folder);
    } else {
      $this->load->view('workspace/editWorkshops');
    }
  }


  public function editWorkshopInfo($renderData = "") {
    if (($this->session->userdata('is_logged_in')) && ($this->session->userdata('is_contentmanager') == 1)) {
      $wid    = $this->input->get("wid"); 
      $this->title = "Water | Workspace";
      $folder = 'workspace';
      $this->data['workshops']      = $this->workshops_model->getWorkshopDetails($wid);
      $this->data['places'] = $this->places_model->getAll();
      $this->_render('workspace/workshopDetails', $renderData, $folder);
    } else {
      $this->title = "Access Denied";
      $this->load->view('workspace/denied');
    }  
  }

  public function editWorkshopFunction(){
    //User validation check
    if (($this->session->userdata('is_logged_in')) && ($this->session->userdata('is_collaborator') == 1)){
      //Do nothing
    } 
    else {
      $this->title = "Access Denied";
      $this->load->view('workspace/denied');
    }
    $this->form_validation->set_rules('wid',          'Workshop ID',            'required|max_length[150]|xss_clean');
    $this->form_validation->set_rules('pname',        'Place Name',             'required|min_length[2]|max_length[150]|xss_clean');
    $this->form_validation->set_rules('wtitle',       'Workshop Title',         'required|min_length[2]|max_length[150]|xss_clean');
    $this->form_validation->set_rules('wdescription', 'Workshop Description',   'required|min_length[2]|xss_clean');
    $this->form_validation->set_rules('wdate_time',   'Workshop Date and Time', 'required|min_length[2]|max_length[150]|xss_clean');
    $this->form_validation->set_rules('wpresenter',   'Event Presenter', 'min_length[2]|max_length[100]|xss_clean');
    $this->form_validation->set_rules('winstitution',   'Presenter Institution', 'min_length[2]|max_length[60]|xss_clean');
    $this->form_validation->set_rules('wtype',   'Event Type', 'required|min_length[2]|max_length[50]|xss_clean');
    
    if ($this->form_validation->run() == FALSE) { 
      echo validation_errors();
    }
    else {
      $wid          = $this->input->post('wid');
      $pname        = $this->input->post('pname');
      $pid_FK       = $this->places_model->getPid($pname);
      $uid_FK       = $this->session->userdata('uid');
      $wtitle       = $this->input->post('wtitle');
      $wdescription = $this->input->post('wdescription');
      $wdate_time   = $this->input->post('wdate_time');      
      $wpresenter   = $this->input->post('wpresenter');
      $winstitution = $this->input->post('winstitution');
      $wtype        = $this->input->post('wtype');
      $udpatedWorkshop  = array(
        'pid_FK'       => $pid_FK,
        'uid_FK'       => $uid_FK,
        'wtitle'       => $wtitle,
        'wdescription' => $wdescription,
        'wdate_time'   => $wdate_time,
        'wpresenter'   => $wpresenter,
        'winstitution' => $winstitution,
        'wtype'        => $wtype,
        );
      $this->workshops_model->updateWorkshop($udpatedWorkshop, $wid); 
      echo 'success';
    }
  }

  public function deleteWorkshopFunction($renderData = ""){
    if (($this->session->userdata('is_logged_in')) && ($this->session->userdata('is_contentmanager') == 1)) {
      //delete that publication here
      $wid = $this->input->get("wid");  
      $this->workshops_model->deleteWorkshop($wid);  
      $this->title = "Water | Workspace";
      $folder = 'workspace';
      $this->data['workshops']  =  $this->workshops_model->getAll();
      $this->_render('workspace/editWorkshops', $renderData, $folder);
    } else {
      $this->title = "Access Denied";
      $this->load->view('workspace/denied');
    }
      
  }

}
