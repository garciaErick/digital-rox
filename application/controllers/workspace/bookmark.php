<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bookmark extends MY_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('bookmark_model');
        $this->load->library(array('session', 'form_validation'));
        $this->load->helper(array('url', 'form'));
        $this->load->database('default');
    }
    
    //Render of My Bookmarks view
    public function index($renderData=""){  
        if($this->session->userdata('is_logged_in')){ 
            $this->title = "Water | Workspace";
            $folder = 'workspace';
            
            $uid = $this->session->userdata('uid');
            $resource_raw = $this->bookmark_model->getUserBookmarks($uid); 
            $oldcid = -1;
            $index = -1;
            $table = null;
            foreach ($resource_raw->result() as $row){
                $cid = $row->cid;
                if ($oldcid != $cid){
                    $oldcid = $cid;
                    $index ++;
                    $table[$index]['User_ID'] = $row->uid;
                    $table[$index]['Uploader'] = $row->name;
                    $table[$index]['Upload_Date'] = $row->cupload_date;
                    $table[$index]['Resource_ID'] = $row->cid;
                    $table[$index][$row->mlabel] = $row->cm_data; 
                }
                else{
                    $table[$index][$row->mlabel] = $row->cm_data; 
                }
            }
            
            $this->data['resources'] = $table;   
            
            $this->_render('workspace/bookmark',$renderData, $folder);   
        }
        
        else{
            $this->title = "Access Denied";
            $this->load->view('workspace/denied');
        }
        
    }
     

    public function setBookmark(){
        if ($this->session->userdata('is_logged_in')) {
            $cid = $this->input->get('cid', TRUE);
            $uid = $this->session->userdata('uid');
            $this->bookmark_model->addBookmark($uid, $cid);  
            echo "success";
        }
        else{
            $this->title = "Access Denied";
            $this->load->view('workspace/denied');
        }
            
    }
    
    public function unsetBookmark(){
        if ($this->session->userdata('is_logged_in')) {
            $cid = $this->input->get('cid', TRUE);
            $uid = $this->session->userdata('uid');
            $this->bookmark_model->deleteBookmark($uid, $cid);
            echo "success";
        }
        else{
            $this->title = "Access Denied";
            $this->load->view('workspace/denied');
        }
    }
            
}
