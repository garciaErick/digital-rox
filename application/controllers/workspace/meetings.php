<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Meetings extends MY_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model('meetings_model');
    $this->load->model('places_model');
    $this->load->library(array('session', 'form_validation'));
    $this->load->helper(array('url', 'form'));
    $this->load->database('default');
  }

  public function viewMeetings($renderData = "") {
    if (($this->session->userdata('is_logged_in'))) {
      $this->title = "Water | Workspace";
      $folder = 'workspace';
      $this->data['meetings']  =  $this->meetings_model->getMeetingView();
      $this->_render('workspace/viewMeetings', $renderData, $folder);
    } else {
      $this->title = "Access Denied";
      $this->load->view('workspace/denied');
    }
  }

  public function addMeeting($renderData = "") {
    if (($this->session->userdata('is_logged_in')) && ($this->session->userdata('is_contentmanager') == 1)) {
      $this->title = "Water | Workspace";
      $folder= 'workspace';
      $data['places'] = $this->places_model->getAll();
      $this->load->view('workspace/addMeeting', $data,       TRUE);
      $this->_render('workspace/addMeeting',    $renderData, $folder);
    } else {
      $this->title = "Access Denied";
      $this->load->view('workspace/denied');
    }
  }

  public function addMeetingFunction(){
    //User validation check
    if (($this->session->userdata('is_logged_in')) && ($this->session->userdata('is_contentmanager') == 1)){
      //Do nothing
    } 
    else {
      $this->load->view('workspace/denied');
    }

    $this->form_validation->set_rules('pname',         'Place Name',          'required|min_length[2]|max_length[150]|xss_clean');
    $this->form_validation->set_rules('metitle',       'Meeting Title',         'required|min_length[2]|max_length[150]|xss_clean');
    $this->form_validation->set_rules('medate_time',   'Meeting Date and Time', 'required|min_length[2]|max_length[150]|xss_clean');
    $this->form_validation->set_rules('medescription', 'Meeting Description',   'min_length[2]|xss_clean');
    if ($this->form_validation->run() == FALSE) {	
      echo validation_errors();
    }
    else {
      $pname         = $this->input->post('pname');
      $pid_fk        = $this->places_model->getPid($pname);
      $uid           = $this->session->userdata('uid');
      $metitle       = $this->input->post('metitle');
      $medate_time   = $this->input->post('medate_time');
      $medescription = $this->input->post('medescription');
      $newMeeting    = array(
        'pid_fk'        => $pid_fk,
        'metitle'       => $metitle,
        'medate_time'   => $medate_time,
        'medescription' => $medescription,
      );
      $this->meetings_model->addMeeting($newMeeting);
      echo 'success';
    }
  }

  public function editMeetings($renderData = "") {
    if (($this->session->userdata('is_logged_in')) && ($this->session->userdata('is_contentmanager') == 1)) {
      $this->title = "Water | Workspace";
      $folder = 'workspace';
      $this->data['meetings']  =  $this->meetings_model->getAll();
      $this->_render('workspace/editMeetings', $renderData, $folder);
    } else {
      $this->title = "Access Denied";
      $this->load->view('workspace/denied');
    }
  }

  public function editMeetingView($renderData = ""){
    if (($this->session->userdata('is_logged_in')) && ($this->session->userdata('is_contentmanager') == 1)) {
      //delete that meeting here
      $meid = $this->input->get("meid");   
      $this->title = "Water | Workspace";
      $folder = 'workspace';
      //get the meeting details here
      $this->data['places'] = $this->places_model->getAll();
      $this->data['meeting']  =  $this->meetings_model->getMeetingDetails($meid);
      $this->_render('workspace/meetingDetails', $renderData, $folder);
    } else {
      $this->title = "Access Denied";
      $this->load->view('workspace/denied');
    }      
      
  }
  
  public function editMeetingFunction($renderData = ""){
    if (($this->session->userdata('is_logged_in')) && ($this->session->userdata('is_contentmanager') == 1)) {
        
      $this->form_validation->set_rules('pname',         'Place Name',          'required|min_length[2]|max_length[150]|xss_clean');
      $this->form_validation->set_rules('metitle',       'Meeting Title',         'required|min_length[2]|max_length[150]|xss_clean');
      $this->form_validation->set_rules('medate_time',   'Meeting Date and Time', 'required|min_length[2]|max_length[150]|xss_clean');
      $this->form_validation->set_rules('medescription', 'Meeting Description',   'min_length[2]|xss_clean');  
      if ($this->form_validation->run() == FALSE) {	
        echo validation_errors();
      }
      else {
        $meid          = $this->input->post("meid");   
        $pname         = $this->input->post('pname');
        $pid_fk        = $this->places_model->getPid($pname);
        $metitle       = $this->input->post('metitle');
        $medate_time   = $this->input->post('medate_time');
        $medescription = $this->input->post('medescription');
        $updatedMeeting    = array(
          'pid_fk'        => $pid_fk,
          'metitle'       => $metitle,
          'medate_time'   => $medate_time,
          'medescription' => $medescription,
        );
        $this->meetings_model->updateMeeting($updatedMeeting, $meid);

        $this->title = "Water | Workspace";
        $folder = 'workspace';
        $this->data['meetings']  =  $this->meetings_model->getAll();
        $this->_render('workspace/editMeetings', $renderData, $folder);
    }
             
    } else {
      $this->title = "Access Denied";
      $this->load->view('workspace/denied');
    } 
  }
  
  public function deleteMeetingFunction($renderData = ""){
    if (($this->session->userdata('is_logged_in')) && ($this->session->userdata('is_contentmanager') == 1)) {
      //delete that meeting here
      $meid = $this->input->get("meid");  
      $this->meetings_model->deleteMeeting($meid);  
      $this->title = "Water | Workspace";
      $folder = 'workspace';
      $this->data['meetings']  =  $this->meetings_model->getAll();
      $this->_render('workspace/editMeetings', $renderData, $folder);
    } else {
      $this->title = "Access Denied";
      $this->load->view('workspace/denied');
    }
      
  }

}
