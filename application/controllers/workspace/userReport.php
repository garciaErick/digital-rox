<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class userReport extends MY_Controller {
    
    public function index($renderData=""){  
        if($this->session->userdata('is_logged_in')){
            $this->load->model('user_model');
            $this->title = "Water | Directory";
            $folder = 'workspace';
            $this->data['members'] = $this->user_model->getAll(); 
            $this->_render('workspace/userReport',$renderData, $folder);   
        }
        else{
            $this->load->view('workspace/denied');
        } 
    }
     
}