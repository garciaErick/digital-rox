<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class events extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('workshops_model');
		$this->load->library(array('session', 'form_validation'));
		$this->load->helper(array('url', 'form'));
		$this->load->database('default');
	}

	public function index($renderData=""){  
		if (($this->session->userdata('is_logged_in')) && ($this->session->userdata('is_contentmanager') == 1)) {
			$this->title = "Water | Workspace";
			$folder = 'template';
			$this->data['event'] = $this->workshops_model->getAll();
			$this->_render('pages/display_events', $renderData, $folder);
		} else {
			$this->load->view('workspace/denied');
		} 
	}

}    
