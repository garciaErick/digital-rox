<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Videos extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('videos_model');
		$this->load->library(array('session', 'form_validation'));
		$this->load->helper(array('url', 'form'));
                $this->load->database('default');
	}
        
	public function addVideo($renderData = "") {
	  if (($this->session->userdata('is_logged_in')) && ($this->session->userdata('is_contentmanager') == 1)) {
		$this->title = "Water | Workspace";
		$folder = 'workspace';
    $data['videos'] = $this->videos_model->getAll();
    $this->load->view('workspace/addVideo', $data,       TRUE);
		$this->_render('workspace/addVideo',    $renderData, $folder);
	} else {
    $this->title = "Acess Denied";
    $this->load->view('workspace/denied');
    }
	}
	
	public function addVideoFunction(){

            //echo 'success';
            //User validation check
            if (($this->session->userdata('is_logged_in')) && ($this->session->userdata('is_contentmanager') == 1)){
                //Do nothing
            } 
            else {
                $this->load->view('workspace/denied');
            } 
            
            $this->form_validation->set_rules('vtitle',          'Video Title',           'required|min_length[2]|max_length[150]|xss_clean');
            $this->form_validation->set_rules('vdescription',    'Video Description',     'required|min_length[2]|max_length[150]|xss_clean');
            $this->form_validation->set_rules('vsubject',        'Video Subject',         'required|min_length[2]|xss_clean');
            $this->form_validation->set_rules('vvideo_link',     'Video Link',            'required|min_length[2]|max_length[150]|xss_clean');
            $this->form_validation->set_rules('vadditional_url', 'Video Additional Url ', 'min_length[2]|max_length[150]|xss_clean');
            if ($this->form_validation->run() == FALSE) {	
              echo validation_errors();
            }
            else {
              $uid_fk          = $this->session->userdata('uid');
              $vtitle          = $this->input->post('vtitle');
              $vdescription    = $this->input->post('vdescription');
              $vsubject        = $this->input->post('vsubject');
              $vvideo_link     = $this->input->post('vvideo_link');
              $vadditional_url = $this->input->post('vadditional_url');
              $newVideo        = array(
                'uid_fk'          => $uid_fk,
                'vtitle'          => $vtitle,
                'vdescription'    => $vdescription,
                'vsubject'        => $vsubject,
                'vvideo_link'     => $vvideo_link,
                'vadditional_url' => $vadditional_url,
              );
              $this->videos_model->addVideo($newVideo);
              echo 'success';
            }
	}
	
  public function editVideos($renderData = "") {
   if (($this->session->userdata('is_logged_in')) && ($this->session->userdata('is_contentmanager') == 1)) {
            $this->title = "Water | Workspace";
            $folder = 'workspace';
            $this->data['videos']  =  $this->videos_model->getAll();
            $this->_render('workspace/editVideos', $renderData, $folder);
        } else {
            $this->title = "Access Denied";
            $this->load->view('workspace/denied');
        }
  }

	
	public function editVideoInfo($renderData = "") {
    if (($this->session->userdata('is_logged_in')) && ($this->session->userdata('is_contentmanager') == 1)) {
      //delete that publication here
      $vid = $this->input->get("vid");   
      $this->title = "Water | Workspace";
      $folder = 'workspace';
      //get the publication details here
      //$this->data['publications']   = $this->publications_model->getAll();
      $this->data['videos']      = $this->videos_model->getVideoDetails($vid);
      $this->_render('workspace/videoDetails', $renderData, $folder);
    } else {
      $this->title = "Access Denied";
      $this->load->view('workspace/denied');
    }   

  }
	
	
  public function editVideoFunction(){
		//User validation check
    if (($this->session->userdata('is_logged_in')) && ($this->session->userdata('is_collaborator') == 1)){
      //Do nothing
    } 
    else {
      $this->title = "Access Denied";
      $this->load->view('workspace/denied');
    }
            $this->form_validation->set_rules('vid',             'Video ID',              'required|max_length[150]|xss_clean');
            $this->form_validation->set_rules('vtitle',          'Video Title',           'required|min_length[2]|max_length[150]|xss_clean');
            $this->form_validation->set_rules('vdescription',    'Video Description',     'required|min_length[2]|max_length[150]|xss_clean');
            $this->form_validation->set_rules('vsubject',        'Video Subject',         'required|min_length[2]|xss_clean');
            $this->form_validation->set_rules('vvideo_link',     'Video Link',            'required|min_length[2]|max_length[150]|xss_clean');
            $this->form_validation->set_rules('vadditional_url', 'Video Additional Url ', 'min_length[2]|max_length[150]|xss_clean');
            if ($this->form_validation->run() == FALSE) { 
              echo validation_errors();
            }
            else {
              $vid             = $this->input->post('vid');
              $vtitle          = $this->input->post('vtitle');
              $vdescription    = $this->input->post('vdescription');
              $vsubject        = $this->input->post('vsubject');
              $vvideo_link     = $this->input->post('vvideo_link');
              $vadditional_url = $this->input->post('vadditional_url');
              $updatedVideo        = array(
                'vtitle'          => $vtitle,
                'vdescription'    => $vdescription,
                'vsubject'        => $vsubject,
                'vvideo_link'     => $vvideo_link,
                'vadditional_url' => $vadditional_url
              );
                $this->videos_model->updateVideo2($updatedVideo, $vid); 
                echo 'success';
    }
	}

  public function deleteVideoFunction($renderData = ""){
    if (($this->session->userdata('is_logged_in')) && ($this->session->userdata('is_contentmanager') == 1)) {
      //delete that publication here
      $vid = $this->input->get("vid");  
      $this->videos_model->deleteVideo($vid);  
      $this->title = "Water | Workspace";
      $folder = 'workspace';
      $this->data['videos']  =  $this->videos_model ->getAll();
      $this->_render('workspace/editVideos', $renderData, $folder);
    } else {
      $this->title = "Access Denied";
      $this->load->view('workspace/denied');
    }
      
  }

}
