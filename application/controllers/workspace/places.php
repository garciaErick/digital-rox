<?php

if (!defined('BASEPATH'))
  exit('No direct script access allowed');

class Places extends MY_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model('places_model');
    $this->load->library(array('session', 'form_validation'));
    $this->load->helper(array('url', 'form'));
    $this->load->database('default');
  }


  public function addPlaceFunction(){
    //User validation check
    if (($this->session->userdata('is_logged_in')) && ($this->session->userdata('is_contentmanager') == 1)){
      //Do nothing
    } 
    else {
      $this->load->view('workspace/denied');
    }      
    $this->form_validation->set_rules('pname',     'Place Name',     'required|min_length[2]|max_length[150]|xss_clean');
    $this->form_validation->set_rules('pcity',     'Place City',     'required|min_length[2]|max_length[150]|xss_clean');
    $this->form_validation->set_rules('padress',   'Place Adress',   'required|min_length[2]|xss_clean');
    $this->form_validation->set_rules('pcountry',  'Place Country',  'required|min_length[2]|max_length[150]|xss_clean');
    $this->form_validation->set_rules('pwebsite',  'Place Website',  'min_length[2]|max_length[500]|xss_clean');
    $this->form_validation->set_rules('pcomments', 'Place Comments', 'min_length[2]|max_length[500]|xss_clean');
    if ($this->form_validation->run() == FALSE) {	
      echo validation_errors();
    }
    else {
      $pname       = $this->input->post('pname');
      $pcity       = $this->input->post('pcity');
      $padress     = $this->input->post('padress');
      $pcountry    = $this->input->post('pcountry');
      $pwebsite    = $this->input->post('pwebsite');
      $pcomments   = $this->input->post('pcomments');
      $newPlace = array(
        'pname'     => $pname,
        'pcity'     => $pcity,
        'padress'   => $padress,
        'pcountry'  => $pcountry,
        'pwebsite'  => $pwebsite,
        'pcomments' => $pcomments,
      );
      $this->places_model->addPlace($newPlace);
      echo 'success';
    }
  }
}
