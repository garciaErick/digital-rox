<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Publications extends MY_Controller {
    
        public function __construct() {
        parent::__construct();
        $this->load->model('publications_model');
        $this->load->library(array('session', 'form_validation'));
        $this->load->helper(array('url', 'form'));
        $this->load->database('default');
    }
    
        
    public function addPublication($renderData = "") {
        if (($this->session->userdata('is_logged_in')) && ($this->session->userdata('is_contentmanager') == 1)) {
            $this->title = "Water | Workspace";
            $folder = 'workspace';
            $this->_render('workspace/addPublication', $renderData, $folder);
        } else {
            $this->load->view('workspace/denied');
        }
    }
        
    public function addPublicationFunction(){
        //User validation check
        if (($this->session->userdata('is_logged_in')) && ($this->session->userdata('is_contentmanager') == 1)){
            //Do nothing
        } 
        else {
            $this->load->view('workspace/denied');
        } 
        
      $this->form_validation->set_rules('ptitle',        'Publication Title',        'required|min_length[2]|max_length[150]|xss_clean');
      $this->form_validation->set_rules('pdescription',  'Publication Description',  'required|min_length[2]|max_length[150]|xss_clean');
      $this->form_validation->set_rules('pcontent_link', 'Publication Content Link', 'required|min_length[2]|xss_clean');
      $this->form_validation->set_rules('planguage',     'Publication Language',     'required|min_length[2]|max_length[150]|xss_clean');
      $this->form_validation->set_rules('psubject',      'Publication Subject',      'required|min_length[2]|max_length[150]|xss_clean');
      $this->form_validation->set_rules('psource',       'Publication Source',       'required|min_length[2]|max_length[150]|xss_clean');
      $this->form_validation->set_rules('pdate',         'Publication Date',         'required|min_length[2]|max_length[150]|xss_clean');
      $this->form_validation->set_rules('pformat',       'Publication Format',       'required|min_length[2]|max_length[150]|xss_clean');
      if ($this->form_validation->run() == FALSE) {	
        echo validation_errors();
      }
      else {
        $uid_fk         = $this->session->userdata('uid');
        $ptitle         = $this->input->post('ptitle');
        $pdescription   = $this->input->post('pdescription');
        $pcontent_link  = $this->input->post('pcontent_link');
        $planguage      = $this->input->post('planguage');
        $psubject       = $this->input->post('psubject');
        $psource        = $this->input->post('psource');
        $pdate          = $this->input->post('pdate');
        $pformat        = $this->input->post('pformat');
        $newPublication = array(
          'uid_fk'        => $uid_fk,
          'ptitle'        => $ptitle,
          'pdescription'  => $pdescription,
          'pcontent_link' => $pcontent_link,
          'planguage'     => $planguage,
          'psubject'      => $psubject,
          'psource'       => $psource,
          'pdate'         => $pdate,
          'pformat'       => $pformat,
        );
        $this->publications_model->addPublication($newPublication);
        echo 'success';
      }
    }
    
    public function editPublications($renderData = "") {

        if (($this->session->userdata('is_logged_in')) && ($this->session->userdata('is_contentmanager') == 1)) {
            $this->title = "Water | Workspace";
            $folder = 'workspace';
            $this->data['publications']  =  $this->publications_model->getAll();
            $this->_render('workspace/editPublications', $renderData, $folder);
        } else {
            $this->title = "Access Denied";
            $this->load->view('workspace/denied');
        }
    }

    public function editPublicationView($renderData = ""){

    if (($this->session->userdata('is_logged_in')) && ($this->session->userdata('is_contentmanager') == 1)) {
      //delete that publication here
      $pid = $this->input->get("pid");   
      $this->title = "Water | Workspace";
      $folder = 'workspace';
      //get the publication details here
      //$this->data['publications']   = $this->publications_model->getAll();
      $this->data['publications']      = $this->publications_model->getPublicationDetails($pid);
      $this->_render('workspace/publicationDetails', $renderData, $folder);
    } else {
      $this->title = "Access Denied";
      $this->load->view('workspace/denied');
    }      
      
  }

  public function editPublicationFunction($renderData = ""){
    //User validation check
    if (($this->session->userdata('is_logged_in')) && ($this->session->userdata('is_collaborator') == 1)){
      //Do nothing
    } 
    else {
      $this->title = "Access Denied";
      $this->load->view('workspace/denied');
    }
            $this->form_validation->set_rules('pid',           'Publication ID',           'required|max_length[150]|xss_clean');
            $this->form_validation->set_rules('ptitle',        'Publication Title',        'required|min_length[2]|max_length[150]|xss_clean');
            $this->form_validation->set_rules('pdescription',  'Publication Description',  'required|min_length[2]|max_length[150]|xss_clean');
            $this->form_validation->set_rules('pcontent_link', 'Publication Content Link', 'required|min_length[2]|xss_clean');
            $this->form_validation->set_rules('planguage',     'Publication Language',     'required|min_length[2]|max_length[150]|xss_clean');
            $this->form_validation->set_rules('psubject',      'Publication Subject',      'required|min_length[2]|max_length[150]|xss_clean');
            $this->form_validation->set_rules('psource',       'Publication Source',       'required|min_length[2]|max_length[150]|xss_clean');
            $this->form_validation->set_rules('pdate',         'Publication Date',         'required|min_length[2]|max_length[150]|xss_clean');
            $this->form_validation->set_rules('pformat',       'Publication Format',       'required|min_length[2]|max_length[150]|xss_clean');
            if ($this->form_validation->run() == FALSE) { 
              echo validation_errors();
            }
            else {
              $pid            = $this->input->post('pid');
              $uid_fk         = $this->session->userdata('uid');
              $ptitle         = $this->input->post('ptitle');
              $pdescription   = $this->input->post('pdescription');
              $pcontent_link  = $this->input->post('pcontent_link');
              $planguage      = $this->input->post('planguage');
              $psubject       = $this->input->post('psubject');
              $psource        = $this->input->post('psource');
              $pdate          = $this->input->post('pdate');
              $pformat        = $this->input->post('pformat');
              $updatedVideo = array(
                'uid_fk'        => $uid_fk,
                'ptitle'        => $ptitle,
                'pdescription'  => $pdescription,
                'pcontent_link' => $pcontent_link,
                'planguage'     => $planguage,
                'psubject'      => $psubject,
                'psource'       => $psource,
                'pdate'         => $pdate,
                'pformat'       => $pformat,
        );
                $this->publications_model->updatePublication($updatedVideo, $pid); 
                echo 'success';
    }
  }


  public function deletePublicationFunction($renderData = ""){
    if (($this->session->userdata('is_logged_in')) && ($this->session->userdata('is_contentmanager') == 1)) {
      //delete that publication here
      $pid = $this->input->get("pid");  
      $this->publications_model->deletePublication($pid);  
      $this->title = "Water | Workspace";
      $folder = 'workspace';
      $this->data['publications']  =  $this->publications_model ->getAll();
      $this->_render('workspace/editPublications', $renderData, $folder);
    } else {
      $this->title = "Access Denied";
      $this->load->view('workspace/denied');
    }
      
  }

}
