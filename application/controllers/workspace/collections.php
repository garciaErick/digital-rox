<?php

if (!defined('BASEPATH'))
  exit('No direct script access allowed');

class Collections extends MY_Controller {

  public function upload($renderData = "") {

    if ($this->session->userdata('is_logged_in') && ($this->session->userdata('is_collaborator') == 1)) {
      $this->load->model('tags_model');
      $tag_list = $this->tags_model->getTagName();
      $this->title = "Water | Workspace";
      $folder = 'workspace';
      $this->data['tagList'] = $tag_list;
      $this->_render('workspace/uploadWizard', $renderData, $folder);
    } else { 
      $this->title = "Access Denied"; 
      $this->load->view('workspace/denied');
    }
    
  }

  public function addCollectionFunction(){
    //User validation check
    if (($this->session->userdata('is_logged_in')) && ($this->session->userdata('is_collaborator') == 1)){
       //Do nothing
    } 
    else {
        $this->load->view('workspace/denied');
    }

    $input_collection = json_decode(trim(file_get_contents('php://input')), true);
    //print_r($input_collection);
    $this->db->trans_start();
    //Parse and Insert collection (working ok, needs error handling)
    $this->insertCollection($input_collection);

    //Parse and Insert Required Metadata (Needs Refractoring)
    $this->insertRequiredMetadata($input_collection);

    //Parse and Insert Additional Metadata
    $this->insertAdditionalMetadata($input_collection);

    //Parse and Insert File locations 
    $this->insertFile_Locations($input_collection);

    //Parse and Insert tags (working ok, needs error handling)
    $this->insertTags($input_collection);
    $this->db->trans_complete();


    if ($this->db->trans_status() === FALSE){
      echo 'Database error occurred';
    }
    else{ 
      $this->db->trans_commit();
      echo "success";
    }	

  }

  public function insertCollection($input_collection){
    $this->load->model('metadata_model');
    $cid = $input_collection[0]['cid'];
    $uid = $this->session->userdata('uid');
    /* Sanitize Inputs */
    $cid = $this->security->xss_clean($cid);
    $uid = $this->security->xss_clean($uid);
    $this->metadata_model->addResource($cid, $uid);
  }

  public function insertRequiredMetadata($input_collection){
    $this->load->model('metadata_model');
    $cid = $input_collection[0]['cid'];
    /* Keys */
    $titleID       = $this->metadata_model->getMetaID('Title');
    $subjectID     = $this->metadata_model->getMetaID('Subject');
    $descriptionID = $this->metadata_model->getMetaID('Description');
    $rightsID      = $this->metadata_model->getMetaID('Rights');
    $dateID        = $this->metadata_model->getMetaID('Date');
    $keys          = array($titleID, $subjectID, $descriptionID, $rightsID, $dateID);
    /* Values */
    $title         = $input_collection[0]['title'];
    $subject       = $input_collection[0]['subject'];
    $description   = $input_collection[0]['description'];
    $rights        = $input_collection[0]['rights'];
    $date          = $input_collection[0]['date'];
    $uid           = $this->session->userdata('uid');
    /* Sanitize Inputs */ 
    $cid           = $this->security->xss_clean($cid);
    $title         = $this->security->xss_clean($title);
    $subject       = $this->security->xss_clean($subject);
    $description   = $this->security->xss_clean($description);
    $rights        = $this->security->xss_clean($rights);
    $date          = $this->security->xss_clean($date);
    $uid           = $this->security->xss_clean($uid);
    $values        = array($title, $subject, $description, $rights, $date);
    for ($i        = 0; $i < count($keys); $i++) {
      $newMetadata = array(
        'cid_fk'  => $cid,
        'mid_fk'  => $keys[$i],
        'cm_data' => $values[$i],
      );
      $this->metadata_model->addMetadata($newMetadata);
    }
  }

  public function insertAdditionalMetadata($input_collection){
    $this->load->model('metadata_model');
    $cid = $input_collection[0]['cid'];
    $cid = $this->security->xss_clean($cid);

    //Get the ids
    $contributorID = $this->metadata_model->getMetaID('Contributor');
    $sourceID = $this->metadata_model->getMetaID('Source');
    $coverageID = $this->metadata_model->getMetaID('Coverage');
    $creatorID = $this->metadata_model->getMetaID('Creator');
    $formatID = $this->metadata_model->getMetaID('Format');
    $identifierID = $this->metadata_model->getMetaID('Identifier');
    $languageID = $this->metadata_model->getMetaID('Language');
    $relationID = $this->metadata_model->getMetaID('Relation');
    $typeID = $this->metadata_model->getMetaID('Type');
    $URLID = $this->metadata_model->getMetaID('URL');

    foreach($input_collection as $array){ //php warnings for undefined indexes
      foreach ($array as $element){ 
        if (isset($array['contributor']) && $array['contributor'] != null){
          $array['contributor'] = $this->security->xss_clean($array['contributor']);
          $newMetadata = array(
            'cid_fk'  => $cid,
            'mid_fk'  => $contributorID,
            'cm_data' => $array['contributor'],
          );
          $this->metadata_model->addMetadata($newMetadata);
        }
        if (isset($array['source']) && $array['source'] != null){
          $array['source'] = $this->security->xss_clean($array['source']);
          $newMetadata = array(
            'cid_fk'  => $cid,
            'mid_fk'  => $sourceID,
            'cm_data' => $array['source'],
          );
          $this->metadata_model->addMetadata($newMetadata);
        }
        if (isset($array['coverage']) && $array['coverage'] != null){
          $array['coverage'] = $this->security->xss_clean($array['coverage']);
          $newMetadata = array(
            'cid_fk'  => $cid,
            'mid_fk'  => $coverageID,
            'cm_data' => $array['coverage'],
          );
          $this->metadata_model->addMetadata($newMetadata);
        }
        if (isset($array['creator']) && $array['creator'] != null){
          $array['creator'] = $this->security->xss_clean($array['creator']);
          $newMetadata = array(
            'cid_fk'  => $cid,
            'mid_fk'  => $creatorID,
            'cm_data' => $array['creator'],
          );
          $this->metadata_model->addMetadata($newMetadata);
        }
        if (isset($array['format']) && $array['format'] != null){
          $array['format'] = $this->security->xss_clean($array['format']);
          $newMetadata = array(
            'cid_fk'  => $cid,
            'mid_fk'  => $formatID,
            'cm_data' => $array['format'],
          );
          $this->metadata_model->addMetadata($newMetadata);
        }
        if (isset($array['identifier']) && $array['identifier'] != null){
          $array['identifier'] = $this->security->xss_clean($array['identifier']);
          $newMetadata = array(
            'cid_fk'  => $cid,
            'mid_fk'  => $identifierID,
            'cm_data' => $array['identifier'],
          );
          $this->metadata_model->addMetadata($newMetadata);
        }
        if (isset($array['language']) && $array['language'] != null){
          $array['language'] = $this->security->xss_clean($array['language']);
          $newMetadata = array(
            'cid_fk'  => $cid,
            'mid_fk'  => $languageID,
            'cm_data' => $array['language'],
          );
          $this->metadata_model->addMetadata($newMetadata);
        }
        if (isset($array['relation']) && $array['relation'] != null){
          $array['relation'] = $this->security->xss_clean($array['relation']);
          $newMetadata = array(
            'cid_fk'  => $cid,
            'mid_fk'  => $relationID,
            'cm_data' => $array['relation'],
          );
          $this->metadata_model->addMetadata($newMetadata);
        }
        if (isset($array['type']) && $array['type'] != null){
          $array['type'] = $this->security->xss_clean($array['type']);
          $newMetadata = array(
            'cid_fk'  => $cid,
            'mid_fk'  => $typeID,
            'cm_data' => $array['type'],
          );
          $this->metadata_model->addMetadata($newMetadata);
        }
        if (isset($array['url']) && $array['url'] != null){
          $array['url'] = $this->security->xss_clean($array['url']);
          $newMetadata = array(
            'cid_fk'  => $cid,
            'mid_fk'  => $URLID,
            'cm_data' => $array['url'],
          );
          $this->metadata_model->addMetadata($newMetadata);
        } 
      }
    }	

  }

  public function insertFile_Locations($input_collection){
    $this->load->model('metadata_model');
    $cid = $input_collection[0]['cid'];
    $cid = $this->security->xss_clean($cid);
    foreach($input_collection as $array)
    {
      foreach ($array as $element){ 
        if (isset($array['file']) && $array['file'] != null){
          $file = $array['file'];
          $file = $this->security->xss_clean($file);
          $this->metadata_model->addResource_Files($cid, $file);
        }
      }
    }
  }

  public function insertTags($input_collection){
    $this->load->model('tags_model');
    $cid      = $input_collection[0]['cid'];
    $cid      = $this->security->xss_clean($cid);
    $tag_list = $this->tags_model->getTagName();
    $tag_array = array();
    foreach ($tag_list as $row){
      $tname = $this->security->xss_clean($row->tname);
      array_push($tag_array, $tname);
    }

    foreach($input_collection as $array){
      foreach ($array as $element){ 
        if (isset($array['text']) && $array['text'] != null){
          $array['text']  = $this->security->xss_clean($array['text']);
          if (in_array($array['text'], $tag_array)){ 
            //get the primary key and link with the collection
            $row = $this->tags_model->getTagId($array['text']);
            $tid = $row[0]->tid;
            //echo $tid;
            $this->tags_model->insertCollectionTag($cid, $tid);
          }
          else{
            $last_id = $this->tags_model->insertTagName($array['text']); 
            $this->tags_model->insertCollectionTag($cid, $last_id);
          }
        }
      }
    }
  }

}
