<?php

if (!defined('BASEPATH'))
  exit('No direct script access allowed');

class UserAdmin extends MY_Controller {

  public function __construct($renderData = ""){
    parent::__construct();
    $this->load->model('user_model');
    $this->load->library(array('session', 'form_validation'));
    $this->load->helper(array('url', 'form'));
    $this->load->database('default');
  }

  public function index($renderData = "") {

    if ($this->session->userdata('is_logged_in')) {
      $this->title = "Water | Workspace";
      $folder = 'workspace';
      $this->_render('workspace/dashboard', $renderData, $folder);
    } else {
      $this->load->view('workspace/denied');
    }
  }

  //need to check is the user is admin to access this view
  public function createUser($renderData = "") {
    if (($this->session->userdata('is_logged_in')) && ($this->session->userdata('is_admin') == 1)) {
      $this->title = "Water | Workspace";
      $folder = 'workspace';
      $this->_render('workspace/createUser', $renderData, $folder);
    } else {
      $this->load->view('workspace/denied');
    }
  }

  //need to check is the user is admin to access this function
  public function createUserFunction($renderData = "") {

    if (($this->session->userdata('is_logged_in')) && ($this->session->userdata('is_admin') == 1)){
      $this->form_validation->set_rules('firstName', 'First Name', 'required|trim|min_length[2]|max_length[150]|xss_clean');
      $this->form_validation->set_rules('lastName', 'Last Name', 'required|trim|min_length[2]|max_length[150]|xss_clean');
      $this->form_validation->set_rules('email', 'Email', 'required|trim|min_length[2]|max_length[150]|xss_clean');
      $this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[5]|max_length[150]|xss_clean');
      $this->form_validation->set_rules('passConf', 'Password Confirmation', 'required|trim|min_length[5]|max_length[150]|xss_clean|matches[password]');
      //Optional Values
      $this->form_validation->set_rules('picture', 'Picture', 'trim|xss_clean');

      //throw error messages if we have any
      if ($this->form_validation->run() == FALSE) {
        echo validation_errors();
      } else {
        $firstName = $this->input->post('firstName');
        $lastName = $this->input->post('lastName');
        $email = $this->input->post('email');
        $password = sha1($this->input->post('password'));
        $isUserAdmin = (int) $this->input->post('isUserAdmin');

        //Consult the file name
        /* $picture = NULL; */
        /* if ($_FILES['picture']['tmp_name'] != NULL) */
        /*     $picture = file_get_contents($_FILES['picture']['tmp_name'],TRUE); */

        $newUser = array(
          'is_admin' => $isUserAdmin,
          'ufirst_name' => $firstName,
          'ulast_name' => $lastName,
          'uemail' => $email,
          'upassword' => $password,
          /* 'upicture_link' => NULL, */
        );

        /* if($picture != NULL && $picture != ""){ */
        /*   $newUser['upicture_link'] = $picture; */
        /* } */
        //insert to database
        $this->user_model->createUser($newUser);  

        //Render edit users view
        $this->title = "Water | Workspace";
        $folder = 'workspace';
        $this->data['users']  =  $this->user_model->getAll();
        $this->_render('workspace/editUsers', $renderData, $folder);
      }

    } 
    else {
      $this->title = "Access Denied";
      $this->load->view('workspace/denied');
    }
  }

  public function editUsers($renderData = "") {
    if (($this->session->userdata('is_logged_in')) && ($this->session->userdata('is_admin') == 1)) {
      $this->title = "Water | Workspace";
      $folder = 'workspace';
      $this->data['users']  =  $this->user_model->getAll();
      $this->_render('workspace/editUsers', $renderData, $folder);
    } else {
      $this->title = "Access Denied";
      $this->load->view('workspace/denied');
    }
  }

  public function updateInformation($renderData = "") {
    if ($this->session->userdata('is_logged_in')) {
      $this->title = "Water | Workspace";
      $folder = 'workspace';
      $uid = $this->session->userdata('uid');
      $data['results'] = $this->user_model->getInfo($uid);
      $this->load->view('workspace/updateInformation', $data, TRUE);
      $this->_render('workspace/updateInformation', $renderData, $folder);
    } else {
      $this->title = "Access Denied";
      $this->load->view('workspace/denied');
    }
  }

  public function editUserView($renderData = "") {
    if ($this->session->userdata('is_logged_in') && ($this->session->userdata('is_admin') == 1)) {
      $this->title = "Water | Workspace";
      $folder = 'workspace';
      $uid = $this->input->get('uid');
      $data['results'] = $this->user_model->getInfo($uid);
      $this->load->view('workspace/updateInformation', $data, TRUE);
      $this->_render('workspace/updateInformation', $renderData, $folder);
    } else {
      $this->title = "Access Denied";
      $this->load->view('workspace/denied');
    }
  }

  public function updateInformationFunction($renderData = "") {

    //Login check
    if ($this->session->userdata('is_logged_in')){
      $this->form_validation->set_rules('firstName', 'First Name', 'required|trim|min_length[2]|max_length[150]|xss_clean');
      $this->form_validation->set_rules('lastName', 'Last Name', 'required|trim|min_length[2]|max_length[150]|xss_clean');
      $this->form_validation->set_rules('email', 'Email', 'required|trim|min_length[2]|max_length[150]|xss_clean');
      $this->form_validation->set_rules('picture', 'Picture URL', 'trim|xss_clean');
      $this->form_validation->set_rules('institution', 'Institution', 'trim|xss_clean');
      $this->form_validation->set_rules('position', 'Position', 'trim|xss_clean');
      $this->form_validation->set_rules('department', 'Department', 'trim|xss_clean');
      $this->form_validation->set_rules('linkedin', 'Linked-IN', 'trim|xss_clean');
      $this->form_validation->set_rules('website', 'Website', 'trim|xss_clean');
      $this->form_validation->set_rules('team', 'Team', 'trim|xss_clean');
      $this->form_validation->set_rules('area', 'Area', 'trim|xss_clean');
      $this->form_validation->set_rules('education', 'Education', 'trim|xss_clean');
      $this->form_validation->set_rules('activities', 'Activities', 'trim|xss_clean');
      $this->form_validation->set_rules('startDate', 'Start Date', 'trim|xss_clean');
      $this->form_validation->set_rules('endDate', 'End Date', 'trim|xss_clean');

      //throw error messages if we have any
      if ($this->form_validation->run() == FALSE) {
        echo validation_errors();
      } else {
        $firstName = $this->input->post('firstName');
        $lastName = $this->input->post('lastName');
        $email = $this->input->post('email');
        $uid = $this->input->post('uid');
        $picture_link = $this->input->post('picture');
        $institution = $this->input->post('institution');
        $position = $this->input->post('position');
        $department = $this->input->post('department');
        $linkedin = $this->input->post('linkedin');
        $website = $this->input->post('website');
        $team = $this->input->post('team');
        $area = $this->input->post('area');
        $education = $this->input->post('education');
        $activities = $this->input->post('activities');
        $startDate = $this->input->post('startDate');
        $endDate = $this->input->post('endDate');

        //Consult the file name
        $picture = NULL;
        if ($_FILES['picture']['tmp_name'] != NULL)
          $picture = file_get_contents($_FILES['picture']['tmp_name'],TRUE);

        $updatedInfo = array(
          'ufirst_name' => $firstName,
          'ulast_name' => $lastName,
          'uemail' => $email,
          'uinstitution' => $institution,
          'uposition' => $position,
          'udepartment' => $department,
          'ulinkedin' => $linkedin,
          'uwebsite' => $website,
          'uteam' => $team,
          'uarea' => $area,
          'ueducation_level' => $education,
          'uactivities' => $activities,
          'ustart_date' => $startDate,
          'uend_date' => $endDate
        );

        if($picture != NULL && $picture != ""){
          $updatedInfo['upicture_link'] = $picture;
        }

        //Update the database
        //print_r($updatedInfo);
        $this->user_model->updateInfo($updatedInfo, $uid);

        //Render to newxt view
        $this->title = "Water | Directory";
        $folder = 'workspace';
        $this->data['member'] = $this->user_model->getAll(); 
        $this->_render('workspace/people',$renderData, $folder);  
      }
    } 
    else {
      $this->title = "Access Denied";
      $this->load->view('workspace/denied');
    }       
  }

  public function changePasswordFunction(){
    //Login check
    if ($this->session->userdata('is_logged_in')){
      //Do nothing
    } 
    else {
      $this->load->view('workspace/denied');
    }             

    $this->form_validation->set_rules('oldPassword', 'Old Password', 'required|trim|max_length[150]|xss_clean');
    $this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[5]|max_length[150]|xss_clean');
    $this->form_validation->set_rules('passConf', 'Password Confirmation', 'required|trim|max_length[150]|xss_clean|matches[password]');   

    //throw error messages if we have any
    if ($this->form_validation->run() == FALSE) {
      echo validation_errors();
    } else {
      $oldPassword = sha1($this->input->post('oldPassword'));
      $password = sha1($this->input->post('password'));
      $uid = $this->session->userdata('uid');

      $updatedInfo = array(
        'upassword' => $password
      );
      if ($this->user_model->checkPassword($uid, $oldPassword)) {
        $this->user_model->updateInfo($updatedInfo, $uid);
        echo "success";
      } else {
        echo "Your old password input didnt match with our records, please try again";
      }
    }

  }

  //need to check is the user is admin to access this view
  public function updatePassword($renderData = "") {
    if ($this->session->userdata('is_logged_in')) {
      $this->title = "Water | Workspace";
      $folder = 'workspace';
      $this->_render('workspace/updatePassword', $renderData, $folder);
    } else {
      $this->load->view('workspace/denied');
    }  
  }

  //need to check is the user is admin to access this view
  public function deleteUser($renderData = "") {
    if (($this->session->userdata('is_logged_in')) && ($this->session->userdata('is_admin') == 1)) {
      $this->title = "Water | Workspace";
      $folder = 'workspace';
      $data['results'] = $this->user_model->getAll();
      $this->load->view('workspace/deleteUser', $data, TRUE);
      $this->_render('workspace/deleteUser', $renderData, $folder);
    } else {
      $this->title = "Access Denied";
      $this->load->view('workspace/denied');
    }

  }

  public function deleteUserFunction($renderData = "") {
    //User validation check
    if (($this->session->userdata('is_logged_in')) && ($this->session->userdata('is_admin') == 1)){
      $user = $this->input->get('uid');
      $this->user_model->deleteUser($user);
      $this->title = "Water | Workspace";
      $folder = 'workspace';
      $this->data['users']  =  $this->user_model->getAll();
      $this->_render('workspace/editUsers', $renderData, $folder);
    } 
    else {
      $this->title = "Access Denied";
      $this->load->view('workspace/denied');
    }
  }

}
