<?php

if (!defined('BASEPATH'))
  exit('No direct script access allowed');

class Query extends MY_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model('query_model');
    $this->load->library(array('session', 'form_validation'));
    $this->load->helper(array('url', 'form'));
    $this->load->database('default');
  }

  public function addQuery($renderData = "") {

    if ($this->session->userdata('is_logged_in')) {
      $this->title = "Water | Workspace";
      $folder = 'workspace';
      $this->_render('workspace/addQuery', $renderData, $folder);
    } else {
      $this->title = "Access Denied";
      $this->load->view('workspace/denied');
    }
  }

  public function addQueryFunction(){
    //User validation check
    if ($this->session->userdata('is_logged_in')){
        $this->form_validation->set_rules('qstatement', 'Query Statement', 'required|min_length[2]|xss_clean');	
        $this->form_validation->set_rules('qcomments', 'Query Comments', 'xss_clean');	
        if ($this->form_validation->run() == FALSE) {	
          echo "hello";
          echo validation_errors();
        }
        else {
          /* $uid		  = $this->session->userdata('uid'); */
          $qstatement= $this->input->post('qstatement');
          $qcomments  = $this->input->post('qcomments');
          $newQuery	  = array(
            /* 'uid_FK'	   => $uid, */
            'qstatement'	   => $qstatement,
            'qcomments'	   => $qcomments
          );
          $this->query_model->addQuery($newQuery); 
          echo 'success';
        }
    } 
    else {
      $this->title = "Access Denied";
      $this->load->view('workspace/denied');
    }
  }

  public function editQuery($renderData = "") {
    if ($this->session->userdata('is_logged_in')){
      $this->title  = "Water | Workspace";
      $folder       = 'workspace';
      $data['queries'] = $this->query_model->getQuery();
      $this->load->view('workspace/editQuery', $data,       TRUE);
      $this->_render('workspace/editQuery',    $renderData, $folder);
    } else {
      $this->load->view('workspace/denied');
    }
  }

  public function editNewsArticle($nid) {
    if (($this->session->userdata('is_logged_in')) && ($this->session->userdata('is_contentmanager') == 1)){
      $this->title         = "Water | Workspace";
      $folder              = 'workspace';
      $uid                 = $this->session->userdata('uid');
      $data['newsArticle'] = $this->query_model->getNewsArticle($uid, $nid);
      $this->load->view('workspace/editNewsArticle',       $data, TRUE);
      $this->_render('workspace/editNewsArticle', "", $folder);
    } else {
      $this->load->view('workspace/denied');
    }
  }

  public function editNewsArticleFunction(){
    //User validation check
    if (($this->session->userdata('is_logged_in')) && ($this->session->userdata('is_collaborator') == 1)){
      //Do nothing
    } 
    else {
      $this->title = "Access Denied";
      $this->load->view('workspace/denied');
    }
    $this->form_validation->set_rules('nid',      'News ID',      'required|max_length[150]|xss_clean');
    $this->form_validation->set_rules('ntitle',   'News Title',   'required|min_length[2]|max_length[150]|xss_clean');
    $this->form_validation->set_rules('ncontent', 'News Content', 'required|min_length[2]|xss_clean');
    if ($this->form_validation->run() == FALSE) {	
      echo validation_errors();
    }
    else {
      $uid           = $this->session->userdata('uid');
      $nid           = $this->input->post('nid');
      $ntitle        = $this->input->post('ntitle');
      $ncontent      = $this->input->post('ncontent');
      $nupload_date  = date("Y-m-d h:i:s");
      $npicture_link = "";
      $updatedNews = array(
        'ntitle'       => $ntitle,
        'ncontent'     => $ncontent,
        'nupload_date' => $nupload_date, 
        'npicture_link' => $npicture_link,
      );
      $this->query_model->updateNews($updatedNews, $uid, $nid); 
      echo 'success';
    }
  }

  public function deleteNewsFunction($renderData = ""){
    if (($this->session->userdata('is_logged_in')) && ($this->session->userdata('is_contentmanager') == 1)) {
      //delete that publication here
      $uid = $this->session->userdata('uid');
      $nid = $this->input->get("nid"); 
      $this->query_model->deleteNews2($nid);  
      $this->title = "Water | Workspace";
      $folder = 'workspace';
      $data['news'] = $this->query_model->getNews($uid);
      $this->load->view('workspace/editQuery', $data,       TRUE);
      $this->_render('workspace/editNews',    $renderData, $folder);
    } else {
      $this->title = "Access Denied";
      $this->load->view('workspace/denied');
    }
      
  }

}
