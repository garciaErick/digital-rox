<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class People extends MY_Controller {
    
    public function index($renderData=""){  
        if($this->session->userdata('is_logged_in')){
            $this->load->model('user_model');
            $this->title = "Water | Directory";
            $folder = 'workspace';
            $this->data['member'] = $this->user_model->getAll(); 
            $this->_render('workspace/people',$renderData, $folder);   
        }
        else{
            $this->load->view('workspace/denied');
        } 
    }
    
    public function members($renderData=""){
         if($this->session->userdata('is_logged_in')){
            $this->load->model('user_model');
            $this->title = "Water | Directory";
            $folder = 'workspace';
            $this->data['member'] = $this->user_model->getMembers(); 
            $this->_render('workspace/people',$renderData, $folder);   
        }
        else{
            $this->load->view('workspace/denied');
        }           
    }
    
    public function students($renderData=""){
         if($this->session->userdata('is_logged_in')){
            $this->load->model('user_model');
            $this->title = "Water | Directory";
            $folder = 'workspace';
            $this->data['member'] = $this->user_model->getStudents(); 
            $this->_render('workspace/people',$renderData, $folder);   
        }
        else{
            $this->load->view('workspace/denied');
        }           
    }
    
    public function officials($renderData=""){
         if($this->session->userdata('is_logged_in')){
            $this->load->model('user_model');
            $this->title = "Water | Directory";
            $folder = 'workspace';
            $this->data['member'] = $this->user_model->getUSDAOfficials(); 
            $this->_render('workspace/people',$renderData, $folder);   
        }
        else{
            $this->load->view('workspace/denied');
        }           
    }
     
}