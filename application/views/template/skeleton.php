<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title><?php echo $title ?></title>
	<meta name="description" content="<?php echo $description ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="keywords" content="<?php echo $keywords ?>" />
	<meta name="author" content="<?php echo $author ?>" />
	<link rel="shortcut icon" href="<?php echo base_url(IMAGES."favicon.ico");?>">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="<?php echo base_url(CSS."bootstrap.min.css");?>">
    <link rel="stylesheet" href="<?php echo base_url(CSS."style.css");?>">

    <!--external css-->
    <link href="<?php echo base_url(CSS."font-awesome.css");?>" rel="stylesheet" />
    

    <!-- HTML5 Shiv and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  
  <body>
	<?php echo $body ?>
	<!-- js placed at the end of the document so the pages load faster -->
        <script src="<?php echo base_url(JS."jquery-2.1.1.min.js");?>"></script>
        <script src="<?php echo base_url(JS."bootstrap.min.js");?>"></script>
        <script src="<?php echo base_url(JS."jquery.mb.YTPlayer.js");?>"></script>
        <script src="<?php echo base_url(JS."jquery.easing.min.js");?>"></script>
        <script src="<?php echo base_url(JS."custom.js");?>"></script>
        <script src="<?php echo base_url(JS."main.js");?>"></script>
        <script src="<?php echo base_url(ASSETS."noty/packaged/jquery.noty.packaged.min.js");?>"></script>
        <script src="<?php echo base_url(ASSETS."noty/layouts/top.js");?>"></script>
        <script src="<?php echo base_url(ASSETS."noty/themes/default.js");?>"></script>
  </body>
</html>
