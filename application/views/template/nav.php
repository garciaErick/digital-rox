<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
      <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
          </button>
          <div style="margin-top: 10px; margin-left: 20px">
              <a style="color: #ffffff" class="" href="#">
                  <i class="fa fa-2x fa-facebook"></i>
              </a>
              <a href="#" style="margin-left: 30px; color: #ffffff">
                  <i class="fa fa-2x fa-twitter"></i>
              </a>
              <a href="#" style="margin-left: 30px; color: #ffffff">
                  <i class="fa fa-2x fa-youtube"></i>
              </a>
          </div>

      </div>
      <div class="navbar-collapse collapse navbar-right scroll-me">
          <ul class="nav navbar-nav ">
              <?php if (!$this->session->userdata('is_logged_in')) { ?>
                <li id="workspace"><a href="#" data-toggle="modal" data-target="#basicModal">LOG IN</a></li>
              <?php } else{ ?>
                <li id="workspace"><a href="<?php echo site_url("workspace/dashboard");?>">DASHBOARD</a></li>
              <?php } ?>
          </ul>
      </div>
  </div>
</nav>


<div class="modal fade"  id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header" style="background-color: rgb(25, 71, 84)">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" style="color: #ffffff">Workspace Login</h4>
      </div>
      <div class="modal-body">
          <form role="form" method="post" id="loginForm" name="loginForm" onsubmit="userLogin('<?php echo site_url()?>'); return false">
              <div class="form-group">
                  <label for="InputEmail">Email address</label>
                  <input type="email" class="form-control" id="InputEmail" placeholder="Email">
              </div>
              <div class="form-group">
                   <label for="InputPassword">Password</label>
                   <input type="password" class="form-control" id="InputPassword" placeholder="Password">
              </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <input type="submit" value="Login" class="btn btn-primary"/>
      </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
