<section id="section-content">
    <div class="container">
        <h1>Events</h1>
        <hr>
        
        <div class="btn-row">
            <div class="btn-group">
                <a href=""><button class="btn btn-white" type="button">Seminar</button></a>
                <a href=""><button class="btn btn-white" type="button">Symposium</button></a>
                <a href=""><button class="btn btn-white" type="button">Workshop</button></a>
                <a href=""><button class="btn btn-white" type="button">Other</button></a>
            </div>
        </div>
        <br><br>
        
        <?php   
        foreach($event as $row){  ?>
        <div class='row'>
            <div class="col-md-12 grey">
                <h4><?php echo $row->wtitle ?><span class="text-muted small"> - <?php echo $row->wpresenter; ?></span></h4>
                <address>
                 <strong><?php echo $row->wtype;?></strong><br>
                 <?php echo $row->winstitution;?><br>
                 <?php echo $row->wdate_time;?><br>
                </address>
            </div>
        </div>
        <?php   }  ?>
    </div>
</section>
