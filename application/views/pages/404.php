<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <title>404</title>
        
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(WCSS . "bootstrap.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url(WCSS . "bootstrap-reset.css"); ?>" rel="stylesheet">
            
    <!--external css-->
    <link href="<?php echo base_url(WASSETS . "font-awesome/css/font-awesome.css"); ?>" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(WCSS . "style.css"); ?>" rel="stylesheet"> 
    <link href="<?php echo base_url(WCSS . "style-responsive.css"); ?>" rel="stylesheet">
            
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

  <body class="body-404">

    <div class="container">

      <section class="error-wrapper">
          <i class="icon-404"></i>
          <h1>404</h1>
          <h2>page not found</h2>
          <p class="page-404">Something went wrong or that page doesn’t exist yet. </p> <!-- <a href="index.html">Return Home</a></p> -->
      </section>

    </div>
  </body>
</html>
