<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="University of Texas at El Paso">
        <meta name="keyword" content="Dashboard, Sustainable Water Resources, Workspace, Site Manager">
        <link rel="shortcut icon" href="<?php echo base_url(IMAGES."favicon.ico");?>">
        
        <title>Coming Soon </title>
            
        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url(WCSS . "bootstrap.min.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url(WCSS . "bootstrap-reset.css"); ?>" rel="stylesheet">
            
        <!--external css-->
        <link href="<?php echo base_url(WASSETS . "font-awesome/css/font-awesome.css"); ?>" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="<?php echo base_url(WCSS . "style.css"); ?>" rel="stylesheet"> 
        <link href="<?php echo base_url(WCSS . "style-responsive.css"); ?>" rel="stylesheet">
        
        <!-- coming soon styles -->
        <link href="<?php echo base_url(CSS . "soon.css"); ?>" rel="stylesheet">
        
    </head>
    
    <body class="cs-bg">
        <!-- START HEADER -->
        <section id="header">
            <div class="container">
                <header>
                    <!-- HEADLINE -->
                    <h1 > Coming Soon...</h1>
                    <br/>
                    <h2> We are currently working very hard to bring you this awesome section =)</h2>
                </header>
            </div>
        </section>
        <!-- END HEADER -->
        
    </body>
    <!-- END BODY -->
</html>