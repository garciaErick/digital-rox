<!--main content start-->
<?php
if(isset($newsArticle)){ 
  foreach ($newsArticle as $row) {
    $nid           = $row->nid;
    $ntitle        = $row->ntitle;
    $ncontent      = $row->ncontent;
    $npicture_link = $row->npicture_link;
  }
}
?>
<section id="main-content">
    <section class="wrapper site-min-height">
        <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <header class="panel-heading"><font color="black">Edit News Article</font></header>
                    <div class="panel-body">
                        <ul class="nav nav-pills">
                            <li role="presentation" class="active"><a href='<?php echo site_url("workspace/news/editNews") ?>'><i class="fa fa-caret-left"> Back</a></i></li>
                        </ul><br>
                        <form role="form" class="cmxform" id="NewsForm" name="NewsForm" onsubmit="editNewsArticle('<?php echo site_url() ?>', <?php echo $nid ?>); return false">
                            <div class="form-group">
                                <label for="ntitle">News Article Title</label>
                                <input type="text" class="form-control" id="ntitle" placeholder="Enter Title" value="<?php echo $ntitle ?>">
                            </div>
                            <div class="form-group">
                                <label for="ncontent">News Content</label>
                                <textarea name="ncontent" class="form-control ckeditor" rows="15" id="ncontent"><?php echo $ncontent ?></textarea>
                            </div>
                            <button type="submit" class="btn btn-info">Edit Information</button>
                        </form>
                    </div>
                </section>
            </div>
        </div>
    </section>
