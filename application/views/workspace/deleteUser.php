
<section id="main-content">
		<section class="wrapper site-min-height">
				<div class="row">
						<div class="col-md-6">
								<section class="panel">
										<header class="panel-heading"><font color="black">Edit Users</font></header>
										<div class="panel-body">
												<form role="form" method="post" id="deleteUserForm" name="deleteUserForm" onsubmit="deleteUser('<?php echo site_url() ?>'); return false">
														<table class="table table-striped table-hover table-responsive mainCol  " >
																<tr >
																		<td><font size="4" color="black">Uid</b></td>
																		<td><font size="4" color="black">Email</b></td>
																		<td><font class="hidden-xs hidden-sm"  size="4" color="black">First Name</b></td>
																		<td><font class="hidden-xs" size="4" color="black">Last Name</b></td>
																		<td><font class="hidden-xs" size="4" color="black">Delete</b></td>
																		<?php
																		$counter = 0;
																		foreach ($results as $row) {

																				echo "<tr >";
																				echo "<td>";
																				echo $row->uid;
																				echo "</td>";
																				echo "<td>";
																				echo $row->uemail;
																				echo "</font></a></td>";
																				echo "<td><font class='hidden-xs hidden-sm' >";
																				echo $row->ufirst_name;
																				echo "</font></td>";
																				echo "<td> <font class='hidden-xs ' >";
																				echo $row->ulast_name;
																				echo "</td>";
																				echo "<td align='center'>";
																				echo "<input type='checkbox' name='multiple[]' id='multiple[]' value='" . $row->uid . "'>";
																				echo "</td>";
																				echo "</tr>";
																		}
																		?>
														</table>
														<button type="submit" class="btn btn-info" id="resetButton">Submit</button>
												</form>
										</div>
								</section>
						</div>
				</div>
		</section>
</section>