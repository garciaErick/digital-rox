<?php
$title = 'A name given to the resource';
$contributors = 'An entity responsible for making contributions to the resource';
$subject = 'The topic of the resource';
$source = 'A related resource from which the described resource is derived';
$date = 'A point or period of time associated with an event in the lifecycle of the resource';
$description = 'An account of the resource';
$rights = 'Information about rights held in and over the resource';
$tags = 'Certain tags to classify each collection';
$creator = 'An entity primarily responsible for making the resource';
$coverage = 'The spatial or temporal topic of the resource, the spatial applicability of the resource, or the jurisdiction under which the resource is relevant';
$format = 'The file format, physical medium, or dimensions of the resource';
$identifier = 'An unambiguous reference to the resource within a given context';
$language = 'A language of the resource.';
$relation = 'A related resource';
$type = 'The nature or genre of the resource';
?>
    
<script type="text/javascript">
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();	
    });
    var tagAutoComplete = []; //    array used for autocomplete
    <?php foreach ($tagList as $tagName): ?>
        //    Used to autocomplete existing tags from the database
        tagAutoComplete.push("<?php echo $tagName->tname; ?>");
    <?php endforeach; ?>  
</script>
    
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <!-- top panel (heading, title, description, user and upload date -->
        <div class="row">
            <!-- metadata panel -->
            <div class="col-md-12">
                <section class="panel">
                    <header class="panel-heading">
                        Metadata
                    </header>
                    <div class="panel-body">
                        <form role="form" method="post" class="form-horizontal" id="default1" name="uploadFormMetadata" onsubmit="uploadMetadata('<?php echo site_url() ?>');
                            return false" novalidate>
                            <section data-ng-app="upload" data-ng-controller="PanelController as panel">
                                <section class="tab-menu">
                                    <ul class="nav nav-pills">
                                        <li ng-class="{active:panel.isSelected(1)}">
                                            <a href ng-click="panel.selectTab(1)">1. Required Metadata</a>
                                        </li>
                                        <li ng-class="{active:panel.isSelected(2), inactive: panel.isRequiredEmpty()}">
                                            <a href ng-click="panel.selectTab(2)">2. Additional Metadata</a>
                                        </li>
                                        <li ng-class="{active:panel.isSelected(3), inactive: tags.length == 0}">
                                            <a href ng-click="panel.selectTab(3)">3. Upload Resources</a>
                                        </li>
                                        <li ng-class="{active:panel.isSelected(4), inactive: panel.isRequiredEmpty() || tags.length == 0}"> 
                                            <a href ng-click="panel.selectTab(4); panel.assembleJSON()">4. Review</a>
                                        </li>
                                        <li ng-class="{active:panel.isSelected(5), inactive: panel.isRequiredEmpty() || tags.length == 0}">
                                            <a href ng-click="panel.selectTab(5); panel.submitCollection('<?php echo site_url('workspace/collections/addCollectionFunction') ?>','<?php echo site_url('workspace/dashboard') ?>' )">5. Submit</a>
                                        </li>
                                    </ul>
                                </section>
                                <div class="panel" data-ng-show="panel.isSelected(1)" style="margin-left: 25px; margin-right: 25px">
                                    <h2 class="StepTitle"><font color="black">Required Metadata</font></h2>
                                    <div class="alert alert-info">
                                        <button data-dismiss="alert" class="close close-sm" type="button">
                                            <i class="fa fa-times"></i>
                                        </button>
                                        Fill all required fields to move into the next step.
                                    </div>
                                    <div class="form-group">
                                        <label>Collection ID</label> 
                                        <input type="text" class="form-control" ng-disabled="true" id="uploadID" ng-model="singleFields.cid"> 
                                    </div>    
                                    <div class="form-group">
                                        <label data-toggle="tooltip" data-placement="top" title="<?php echo $title ?>">
                                            Title
                                        </label> 
                                        <input type="text" name="title" class="form-control" placeholder="Title" id="title" ng-model="singleFields.title" maxlength="100" required> 
                                    </div>
                                    </form>    
                                    <div class="form-group">
                                        <label data-toggle="tooltip" data-placement="top" title="<?php echo $contributors?>" >
                                            Contributors 
                                        </label>
                                        <a ng-click="panel.addContributor()">
                                            <button type="button" class="btn btn-primary btn-xs">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </button>
                                        </a>
                                        <a ng-click="panel.deleteContributor()">
                                            <button type="button" class="btn btn-primary btn-xs">
                                                <span class="glyphicon glyphicon-minus"></span>
                                            </button>
                                        </a>
                                        <div ng-repeat="item in contributors" style="margin-top: 5px;">
                                            <input type="text" name="contributor" class="form-control" placeholder="Contributor" ng-model="item.contributor" required minlength="3" maxlength="60">
                                        </div>    
                                    </div>
                                    <div class="form-group">
                                        <label data-toggle="tooltip" data-placement="top" title="<?php echo $subject ?>">
                                            Subject
                                        </label>
                                        <input type="text" name="subject" class="form-control" placeholder="Subject" id="subject" ng-model="singleFields.subject" required minlength="3" maxlength="60">
                                    </div>
                                    <div class="form-group">
                                        <label data-toggle="tooltip" data-placement="top" title="<?php echo $source ?>">
                                            Source
                                        </label>
                                        <a ng-click="panel.addSource()">
                                            <button type="button" class="btn btn-primary btn-xs">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </button>
                                        </a>
                                        <a ng-click="panel.deleteSource()">
                                            <button type="button" class="btn btn-primary btn-xs">
                                                <span class="glyphicon glyphicon-minus"></span>
                                            </button>
                                        </a>    
                                        <div ng-repeat="item in sources" style="margin-top: 5px;">
                                            <input type="text" class="form-control" placeholder="Source" ng-model="item.source" minlength="2" maxlength="80" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label data-toggle="tooltip" data-placement="top" title="<?php echo $description ?>">
                                            Description
                                        </label>
                                        <textarea name="description" class="form-control" cols="60" rows="5" id="description" ng-model="singleFields.description" required></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label data-toggle="tooltip" data-placement="top" title="<?php echo $rights ?>">
                                            Rights
                                        </label>
                                        <textarea name="rights" class="form-control" cols="60" rows="5" id="rights" ng-model="singleFields.rights" required></textarea>
                                    </div>
                                        
                                    <div class="form-group">
                                        <label data-toggle="tooltip" data-placement="top" title="<?php echo $date ?>">
                                            Date (yyyy-mm-dd)
                                        </label>
                                        <input type="text" class="form-control" ng-model="singleFields.date" ui-mask="9999-99-99" model-view-value="true"  ui-mask-placeholder ui-mask-placeholder-char="_">
                                    </div>
                                </div>	
                                    
                                <div class="panel" data-ng-show="panel.isSelected(2)" style="margin-left: 25px; margin-right: 25px">
                                    <h2 class="StepTitle"><font color="black">Additional Metadata</font></h2>
                                    <form class="form-inline">    
                                        <div class="form-group">
                                            <label for="tags">Tag</label>
                                            <tags-input type="text"
                                                        placeholder="Add a tag"
                                                        style="width:800px"
                                                        class="bootstrap"
                                                        ng-model="tags"  
                                                        add-from-autocomplete-only="false"
                                                        allowed-tags-pattern="^[a-zA-Z0-9]+$"
                                                        add-on-enter="true"
                                                        min-tags="1"
                                                        replace-spaces-with-dashes="false">                                                    
                                                <auto-complete source="loadTags()"
                                                               min-length="0"
                                                               debounce-delay="0"
                                                               max-results="15"
                                                               highlight-matched-text="true"
                                                               load-on-down-arrow="true"
                                                               select-first-match="false">
                                                </auto-complete>
                                            </tags-input>
                                            <span>At least one tag is required.</span><br><br>
                                            <!-- <pre> tags = {{tags}}</pre> -->
                                        </div>
                                    </form>    
                                    <div class="form-group">
                                        <label data-toggle="tooltip" data-placement="top" title="<?php echo $coverage ?>">
                                            Coverage
                                        </label>
                                        <a ng-click="panel.addCoverage()">
                                            <button type="button" class="btn btn-primary btn-xs">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </button>
                                        </a>
                                        <a ng-click="panel.deleteCoverage()">
                                            <button type="button" class="btn btn-primary btn-xs">
                                                <span class="glyphicon glyphicon-minus"></span>
                                            </button>
                                        </a>
                                        <div ng-repeat="item in coverages" style="margin-top: 5px;">
                                            <input type="text" class="form-control" placeholder="Coverage" ng-model="item.coverage">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label data-toggle="tooltip" data-placement="top" title="<?php echo $creator ?>" >
                                            Creator
                                        </label>
                                        <a ng-click="panel.addCreator()">
                                            <button type="button" class="btn btn-primary btn-xs">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </button>
                                        </a>
                                        <a ng-click="panel.deleteCreator()">
                                            <button type="button" class="btn btn-primary btn-xs">
                                                <span class="glyphicon glyphicon-minus"></span>
                                            </button>
                                        </a>  
                                        <div ng-repeat="item in creators" style="margin-top: 5px;">
                                            <input type="text" class="form-control" placeholder="Creator" ng-model="item.creator">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label data-toggle="tooltip" data-placement="top" title="<?php echo $format ?>" >
                                            Format
                                        </label>
                                        <a ng-click="panel.addFormat()">
                                            <button type="button" class="btn btn-primary btn-xs">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </button>
                                        </a>    
                                        <a ng-click="panel.deleteFormat()">
                                            <button type="button" class="btn btn-primary btn-xs">
                                                <span class="glyphicon glyphicon-minus"></span>
                                            </button>
                                        </a>   
                                        <div ng-repeat="item in formats" style="margin-top: 5px;">
                                            <input type="text" class="form-control" placeholder="Format" ng-model="item.format">
                                        </div>    
                                    </div>
                                    <div class="form-group">
                                        <label data-toggle="tooltip" data-placement="top" title="<?php echo $identifier ?>">
                                            Identifier
                                        </label>
                                        <a ng-click="panel.addIdentifier()">
                                            <button type="button" class="btn btn-primary btn-xs">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </button>
                                        </a>   
                                        <a ng-click="panel.deleteIdentifier()">
                                            <button type="button" class="btn btn-primary btn-xs">
                                                <span class="glyphicon glyphicon-minus"></span>
                                            </button>
                                        </a> 
                                        <div ng-repeat="item in identifiers" style="margin-top: 5px;">
                                            <input type="text" class="form-control" placeholder="Identifier" ng-model="item.identifier">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label data-toggle="tooltip" data-placement="top" title="<?php echo $language ?>">
                                            Language
                                        </label>
                                        <a ng-click="panel.addLanguage()">
                                            <button type="button" class="btn btn-primary btn-xs">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </button>
                                        </a>   
                                        <a ng-click="panel.deleteLanguage()">
                                            <button type="button" class="btn btn-primary btn-xs">
                                                <span class="glyphicon glyphicon-minus"></span>
                                            </button>
                                        </a>
                                        <div ng-repeat="item in languages" style="margin-top: 5px;">
                                            <input type="text" class="form-control" placeholder="Language" ng-model="item.language">
                                        </div>    
                                    </div>
                                    <div class="form-group">
                                        <label data-toggle="tooltip" data-placement="top" title="<?php echo $relation ?>">
                                            Relation
                                        </label>
                                        <a ng-click="panel.addRelation()">
                                            <button type="button" class="btn btn-primary btn-xs">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </button>
                                        </a>
                                        <a ng-click="panel.deleteRelation()">
                                            <button type="button" class="btn btn-primary btn-xs">
                                                <span class="glyphicon glyphicon-minus"></span>
                                            </button>
                                        </a>
                                        <div ng-repeat="item in relations" style="margin-top: 5px;">
                                            <input type="text" class="form-control" placeholder="Relation" ng-model="item.relation">
                                        </div>    
                                    </div>
                                    <div class="form-group">
                                        <label data-toggle="tooltip" data-placement="top" title="<?php echo $type ?>">
                                            Type
                                        </label>
                                        <a ng-click="panel.addType()">
                                            <button type="button" class="btn btn-primary btn-xs">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </button>
                                        </a>  
                                        <a ng-click="panel.deleteType()">
                                            <button type="button" class="btn btn-primary btn-xs">
                                                <span class="glyphicon glyphicon-minus"></span>
                                            </button>
                                        </a>
                                        <div ng-repeat="item in types" style="margin-top: 5px;">
                                            <input type="text" class="form-control" placeholder="Type" ng-model="item.type">
                                        </div>    
                                    </div>
                                </div>
                        </form>
                            
                        <div class="panel" data-ng-show="panel.isSelected(3)">
                            <h2 class="StepTitle"><font color="black">Upload Resources</font></h2>
                            <h3>Web Resources
                                <a ng-click="panel.addURL()">
                                    <button type="button" class="btn btn-primary btn-xs">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                </a>   
                                <a ng-click="panel.deleteURL()">
                                    <button type="button" class="btn btn-primary btn-xs">
                                        <span class="glyphicon glyphicon-minus"></span>
                                    </button>
                                </a>    
                            </h3> 
                            <div ng-repeat="item in urls" style="margin-top: 5px;">     
                                <div class="form-group" style="margin-left: 8px; margin-right: 8px">
                                    <input type="text" class="form-control" placeholder="Web URL" ng-model="item.url">
                                </div>
                            </div>
                                
                            <h3>File Resources</h3>
                            <label>Max File Size:</label><span> 300 MB</span><br>
                            <label>Allowed File Formats:</label><span> gif-jpe?g-png-xls-xlsx-pdf-doc-docx-zip-rar-ppt-pptx-vue-txt</span><br>
                            <label>Files can be dragged and dropped into the panel below.</label>
                            <form id="fileupload" action="//jquery-file-upload.appspot.com/" method="POST" enctype="multipart/form-data">
                                <noscript>
                                <input type="hidden" name="redirect" value="http://blueimp.github.io/jQuery-File-Upload/">
                                </noscript>
                                <div class="row fileupload-buttonbar">
                                    <div class="col-lg-7">
                                        <br>
                                        <span class="btn btn-success fileinput-button">
                                            <i class="glyphicon glyphicon-plus"></i>
                                            <span>Add files...</span>
                                            <input type="file" name="files[]" multiple>
                                        </span>
                                        <button type="submit" class="btn btn-primary start">
                                            <i class="glyphicon glyphicon-upload"></i>
                                            <span>Start upload</span>
                                        </button>
                                        <button type="reset" class="btn btn-warning cancel">
                                            <i class="glyphicon glyphicon-ban-circle"></i>
                                            <span>Cancel upload</span>
                                        </button>
                                        <button type="button" class="btn btn-danger delete">
                                            <i class="glyphicon glyphicon-trash"></i>
                                            <span>Delete</span>
                                        </button>
                                        <input type="checkbox" class="toggle">
                                        <span class="fileupload-process"></span>
                                    </div>
                                    <div class="col-lg-5 fileupload-progress fade">
                                        The global progress bar
                                            
                                        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                            <div class="progress-bar progress-bar-success" style="width:0%;">
                                            </div>
                                        </div>
                                        <div class="progress-extended">
                                            &nbsp;
                                        </div>
                                    </div>
                                </div>
                                <table role="presentation" class="table table-striped">
                                    <tbody class="files">
                                    </tbody>
                                </table>
                            </form>
                            <br>
                            <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
                                <div class="slides">
                                </div>
                                <h3 class="title"></h3>
                                <a class="prev">‹</a>
                                <a class="next">›</a>
                                <a class="close">×</a>
                                <a class="play-pause"></a>
                                <ol class="indicator">
                                </ol>
                            </div>
                        </div>
                        <div class="panel" data-ng-show="panel.isSelected(4)">
                            <h2 class="StepTitle"><font color="black">Review</font></h2>
                            <div class="row">
                                <div class="col-md-6">
                                    <p><label>Collection ID: </label> {{singleFields.cid}} </p>
                                    <p><label>Title: </label> {{singleFields.title}} </p>
                                    <p><label>Subject: </label> {{singleFields.subject}} </p>
                                    <p><label>Description: </label> {{singleFields.description}} </p>
                                    <p><label>Rights: </label> {{singleFields.rights}} </p>
                                    <p><label>Date: </label> {{singleFields.date}} </p>
                                    <div ng-repeat="item in contributors">
                                        <p><label>Contributor: </label> {{item.contributor}}</p>
                                    </div>   
                                    <div ng-repeat="item in sources">
                                        <p><label>Source: </label> {{item.source}}</p>
                                    </div> 
                                </div>
                                <div class="col-md-6">
                                    <div ng-repeat="item in tags">
                                        <p><label>Tag: </label> {{item.text}}</p>
                                    </div> 
                                    <div ng-repeat="item in coverages">
                                        <p><label>Coverage: </label> {{item.coverage}}</p>
                                    </div> 
                                    <div ng-repeat="item in creators">
                                        <p><label>Creator: </label> {{item.creator}}</p>
                                    </div> 
                                    <div ng-repeat="item in formats">
                                        <p><label>Format: </label> {{item.format}}</p>
                                    </div> 
                                    <div ng-repeat="item in identifiers">
                                        <p><label>Identifier: </label> {{item.identifier}}</p>
                                    </div> 
                                    <div ng-repeat="item in languages">
                                        <p><label>Language: </label> {{item.language}}</p>
                                    </div> 
                                    <div ng-repeat="item in relations">
                                        <p><label>Relation: </label> {{item.relation}}</p>
                                    </div> 
                                    <div ng-repeat="item in types">
                                        <p><label>Type: </label> {{item.type}}</p>
                                    </div> 
                                    <div ng-repeat="item in fileList">
                                        <p><label>File Resource: </label> {{item}}</p> 
                                    </div>
                                    <div ng-repeat="item in urls">
                                        <p><label>Web Resource: </label> {{item.url}}</p>
                                    </div>
                                </div>    
                            </div>
                            <!-- <pre>{{jsonCollection | json}}</pre> --> 
                        </div>
                        
                        <div class="panel" data-ng-show="panel.isSelected(5)">
                            <div class="alert alert-info" style="margin-top: 20px;">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                                <strong>Executing Transaction...</strong>
                            </div>
                        </div>
                </section>
                    
            </div>
    </section>
</div>
</div>
<!-- page end -->
</section>
</section>