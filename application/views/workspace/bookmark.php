<script type="text/javascript">
    var resources = <?php echo json_encode($resources); ?>;
</script>

<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">  
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        My Bookmarked Collections
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <div class="adv-table" id="dynamic"> 
                            <table class="display table table-bordered table-striped" id="hidden-table-info">
                                <thead>
                                    <tr>
                                        <th>Collection ID</th>
                                        <th>Title</th>
                                        <th>Contributor</th>
                                        <th>Upload Date</th>
                                        <th>View</th>
                                        <th>Unbookmark</th>
                                            
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(isset($resources)){ ?>
                                        <?php foreach($resources as $row){ ?>
                                    <tr>
                                        <td><?php echo $row['Resource_ID']; ?></td>
                                        <td><?php echo $row['Title']; ?></td>
                                        <td><?php echo $row['Uploader']; ?></td>
                                        <td><?php echo $row['Upload_Date']; ?></td>
                                        <td class="text-center"><a href="<?php echo site_url("workspace/resourceDetails?rid=".$row['Resource_ID'])?>" target="_blank" ><i class="fa fa-eye fa-2x"></i></a></td>
                                        <td class="text-center">
                                            <button type="button" class="btn btn-primary btn-sm" onclick="unsetBookmark('<?php echo site_url(); ?>','<?php echo $row['Resource_ID'];?>')">
                                                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                            </button>
                                        </td>
                                    </tr>
                                         <?php } ?>
                                    <?php } ?>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- end resource panel-->
    </section>
</section>
<!--main content end-->