<section id="main-content">
	<section class="wrapper site-min-height">
		<div class="row">
			<div class="col-md-12">
				<section class="panel">
					<header class="panel-heading"><font color="black">Add a Video</font></header>
				<div class="panel-body">
				<small>*Required Field</small>
            	<hr>
				<form class="cmxform" role="form" method="post" id="addVideoForm" name="addVideoForm" onsubmit="addVideo('<?php echo site_url() ?>'); return false;">
				
							<div class="form-group">
								<label for="title">Title</label>
								<input type="text" class="form-control" id="vtitle" placeholder="Enter publication title">
							</div>
							<div class="form-group">
								<label data-toggle="tooltip" data-placement="top"> Description </label>
								<textarea class="form-control" cols="60" rows="5" id="vdescription"></textarea>
							</div>
							<div class="form-group">
								<label for="subject">Subject</label>
								<input type="text" class="form-control" id="vsubject" placeholder="Enter the subject of the publication">
							</div> 
							<div class="form-group">
								<label for="link">Video Link</label>
								<input type="text" class="form-control" id="vvideo_link" placeholder="Enter link with the publication content">
							</div>
							<div class="form-group">
								<label for="format">Additional URL</label>
								<input type="text" class="form-control" id="vadditional_url" placeholder="Enter an additional url if necessary">
							</div>	
							<button type="submit" class="btn btn-info">Submit</button>
						</form>
					</div>
				</section>
			</div>
		</div>
	</section>
</section>
