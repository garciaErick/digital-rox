<section id="main-content">
    <section class="wrapper site-min-height">
        <div class="row">
            <div class="col-md-6">
                <section class="panel">
                    <header class="panel-heading"><font color="black">Add Event</font></header>
                    <div class="panel-body">
                        <small>*Required Field</small>
                        <hr>
                        <form role="form" method="post" id="addWorkshopForm" name="addWorkshopForm" onsubmit="addWorkshop('<?php echo site_url();?>'); return false">
                            <div class="form-group">
                                <input type="text" class="form-control hidden" id="type" value="workshop">
                            </div> 
                            <div class="form-group">
                                <label for="places">Select Place</label><br>
                                <select class="form-control m-bot15" id='place'>
					  <?php if(isset($places)){ ?>
					  <?php foreach($places as $row){ ?>
                                    <option><?php echo $row->pname;?></option>
					  <?php } }?>
                                </select>
                                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#basicModal"><small>Add new place</small></button>
                            </div>
                            <div class="form-group">
                                <label for="wpresenter">Presenter</label>
                                <input type="text" class="form-control" id="wpresenter" placeholder="Enter Presenter Name">
                            </div> 
                            <div class="form-group">
                                <label for="winstitution">Institution</label>
                                <input type="text" class="form-control" id="winstitution" placeholder="Enter Presenter Institution">
                            </div> 
                            <div class="form-group">
                                <label for="wtitle">*Event Title</label>
                                <input type="text" class="form-control" id="wtitle" placeholder="Enter Workshop Title">
                            </div> 
                            <div class="form-group">
                                <label for="wdescription">*Event Description</label>
                                <textarea class="form-control" id="wdescription" rows="6"></textarea>
                            </div> 
                            <div class="form-group">
                                <label for="wdescription">*Event Type</label>
                                <select class="form-control" id="wtype" name="wtype">
                                    <option>Seminar</option>
                                    <option>Symposium</option>
                                    <option>Workshop</option>
                                    <option>Other</option>
                                </select>
                            </div> 
                            <div class="form-group">
                                <label for="wdate_time">*Event Date and Time Date (yyyy-mm-dd)</label>
                                <input type="text" placeholder data-mask="9999-99-99" class="form-control" name="wdate_time" id="wdate_time">
                            </div>   
                            <button type="submit" class="btn btn-info">Submit</button>
                        </form>
            </div>
    </section>
</div>
</div>
</section>
</section>
<div class="modal fade"  id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: rgb(35, 133, 161)">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" style="color: #ffffff">Create a new Place</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" id="addPlaceForm" name="addPlaceForm" onsubmit="addPlace('<?php echo site_url()?>'); return false">
                    <small>*Required Field</small>
                    <hr>
                    <div class="form-group">
                        <label for="pname">*Place Name</label>
                        <input type="text" class="form-control" id="pname" placeholder="Insert place name">
                    </div>
                    <div class="form-group">
                        <label for="pcity">*Place City</label>
                        <input type="text" class="form-control" id="pcity" placeholder="Insert place city">
                    </div>
                    <div class="form-group">
                        <label for="padress">*Place Adress</label>
                        <input type="text" class="form-control" id="padress" placeholder="Insert place adress">
                    </div>
                    <div class="form-group">
                        <label for="pcountry">*Place Country</label>
                        <input type="text" class="form-control" id="pcountry" placeholder="Insert place country">
                    </div>
                    <div class="form-group">
                        <label for="pwebsite">Place Website</label>
                        <input type="text" class="form-control" id="pwebsite" placeholder="Insert place website">
                    </div>
                    <div class="form-group">
                        <label for="pcomments">Place Comments</label>
                        <input type="text" class="form-control" id="pcomments" placeholder="Insert place comments">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <input type="submit" value="Submit" class="btn btn-primary"/>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
