<!DOCTYPE html>

<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="University of Texas at El Paso">
        <meta name="keyword" content="Dashboard, Sustainable Water Resources, Workspace, Site Manager">
        <link rel="shortcut icon" href="<?php echo base_url(IMAGES."favicon.ico");?>">
        <title><?php echo $title ?></title>  

        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url(WCSS . "bootstrap.min.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url(WCSS . "bootstrap-reset.css"); ?>" rel="stylesheet">

        <!--InputTags CSS-->        
        <link rel="stylesheet" href="<?php echo base_url(WCSS . "ng-tags-input.bootstrap.min.css"); ?>">
        <link rel="stylesheet" href="<?php echo base_url(WCSS . "ng-tags-input.min.css"); ?>">
        <link rel="stylesheet" href="<?php echo base_url(WCSS . "tagstyle.css"); ?>">
        
        <!--external css-->
        <link href="<?php echo base_url(WASSETS . "font-awesome/css/font-awesome.css"); ?>" rel="stylesheet">

        <link href="<?php echo base_url(WASSETS . "jquery-easy-pie-chart/jquery.easy-pie-chart.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url(WCSS . "owl.carousel.css"); ?>" rel="stylesheet">

        <!--right slidebar-->
        <link href="<?php echo base_url(WCSS . "slidebars.css"); ?>" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="<?php echo base_url(WCSS . "style.css"); ?>" rel="stylesheet"> 
        <link href="<?php echo base_url(WCSS . "style-responsive.css"); ?>" rel="stylesheet">
        <!--File Uploader CSS-->
        <link href="<?php echo base_url(WCSS . "bootstrap-fileupload.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url(WASSETS . "file-uploader/css/jquery.fileupload-ui.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url(WASSETS . "file-uploader/css/jquery.fileupload.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url(WCSS . "blueimp-gallery.min.css"); ?>" rel="stylesheet">

        <!-- Dynamic datatables -->
        <link href="<?php echo base_url(WASSETS . "advanced-datatable/media/css/jquery.dataTables.min.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url(WASSETS . "advanced-datatable/media/css/demo_page.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url(WASSETS . "advanced-datatable/media/css/demo_table.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url(WCSS . "buttons.dataTables.min.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url(WASSETS . "data-tables/DT_bootstrap.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url(WASSETS . "bootstrap-datetimepicker/css/datetimepicker.css"); ?>" rel="stylesheet">
        
        <!-- Upload Wizard tab styles-->
        <link href="<?php echo base_url(WCSS . "style-collections.css"); ?>" rel="stylesheet">
        
        <!-- CSS adjustments for browsers with JavaScript disabled 
        <noscript>
             <link rel="stylesheet" href="assets/file-uploader/css/jquery.fileupload-noscript.css">
        </noscript>
        <noscript>
             <link rel="stylesheet" href="assets/file-uploader/css/jquery.fileupload-ui-noscript.css">
        </noscript>
        
        -->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
        <!--[if lt IE 9]>
         <script src="js/html5shiv.js"></script>
         <script src="js/respond.min.js"></script>
        <![endif]-->        

    </head>

    <body> 
        <section id="container" > 
                    
            <!--Loading jquery.js sometimes is required before everything-->
            <script src="<?php echo base_url(JS . "jquery-2.1.1.min.js"); ?>"></script>
            
             <!-- set a global base_url variable to be used on needed javascripts -->
            <script type="text/javascript">
                var baseurl = ('<?php echo site_url() ?>');
            </script>

            <!--Main JS-->
            <script src="<?php echo base_url(JS . "main.js"); ?>"></script>
            <?php echo $body ?>

            <!-- js placed at the end of the document so the pages load faster -->
            <script src="<?php echo base_url(WJS . "bootstrap.min.js"); ?>"></script>
            <script src="<?php echo base_url(JS . "angular.min.js"); ?>"></script>
            <script src="<?php echo base_url(WJS . "ng-tags-input.min.js"); ?>"></script>
            <script src="<?php echo base_url(WJS . "tags-angular.js"); ?>"></script>
            <script src="<?php echo base_url(WJS . "mask.min.js"); ?>"></script> 
            <script src="<?php echo base_url(WJS . "uploadNg.js"); ?>"></script>
            <script src="<?php echo base_url(WJS . "angular-uuid2.min.js"); ?>"></script> 
            <script src="<?php echo base_url(WJS . "jquery.dcjqaccordion.2.7.js"); ?>"></script>
            <script src="<?php echo base_url(WJS . "jquery.scrollTo.min.js"); ?>"></script>
            <script src="<?php echo base_url(WJS . "jquery.nicescroll.js"); ?>"></script>
            <script src="<?php echo base_url(WJS . "jquery.sparkline.js"); ?>"></script>
            <script src="<?php echo base_url(WASSETS . "jquery-easy-pie-chart/jquery.easy-pie-chart.js"); ?>"></script>
            <script src="<?php echo base_url(WJS . "owl.carousel.js"); ?>"></script>
            <script src="<?php echo base_url(WJS . "jquery.customSelect.min.js"); ?>"></script>
            <script src="<?php echo base_url(WJS . "respond.min.js"); ?>"></script>

            <!--right slidebar-->
            <script src="<?php echo base_url(WJS . "slidebars.min.js"); ?>"></script>



            <!--script for workspace dashboard-->
            <script src="<?php echo base_url(WJS . "sparkline-chart.js"); ?>"></script> 
            <script src="<?php echo base_url(WJS . "easy-pie-chart.js"); ?>"></script> 
            <script src="<?php echo base_url(WJS . "count.js"); ?>"></script> 

            <!--File Upload-->
            <script src="<?php echo base_url(WASSETS . "file-uploader/js/vendor/jquery.ui.widget.js"); ?>"></script>
            <script src="<?php echo base_url(WASSETS . "file-uploader/js/vendor/tmpl.min.js"); ?>"></script>
            <script src="<?php echo base_url(WJS . "load-image.all.min.js"); ?>"></script> 
            <script src="<?php echo base_url(WASSETS . "file-uploader/js/vendor/canvas-to-blob.min.js"); ?>"></script>
            <script src="<?php echo base_url(WJS . "jquery.blueimp-gallery.min.js"); ?>"></script>

            <!--Date/Time Picker-->
            <link   href="<?php echo base_url(WASSETS . "bootstrap-datepicker/css/datepicker.css"); ?>" rel="stylesheet">
            <script src= "<?php echo base_url(WASSETS . "bootstrap-datepicker/js/bootstrap-datepicker.js"); ?>"></script>
            <script src= "<?php echo base_url(WASSETS . "bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"); ?>"></script>

            <!--Step Form Wizard-->
            <script src="<?php echo base_url(WJS . "jquery.stepy.js"); ?>"></script> 

            <!--File Upload Assets-->
            <script src="<?php echo base_url(WASSETS . "file-uploader/js/jquery.iframe-transport.js"); ?>"></script>
            <script src="<?php echo base_url(WASSETS . "file-uploader/js/jquery.fileupload.js"); ?>"></script>
            <script src="<?php echo base_url(WASSETS . "file-uploader/js/jquery.fileupload-process.js"); ?>"></script> 
            <script src="<?php echo base_url(WASSETS . "file-uploader/js/jquery.fileupload-image.js"); ?>"></script>
            <script src="<?php echo base_url(WASSETS . "file-uploader/js/jquery.fileupload-audio.js"); ?>"></script>
            <script src="<?php echo base_url(WASSETS . "file-uploader/js/jquery.fileupload-video.js"); ?>"></script>
            <script src="<?php echo base_url(WASSETS . "file-uploader/js/jquery.fileupload-validate.js"); ?>"></script>
            <script src="<?php echo base_url(WASSETS . "file-uploader/js/jquery.fileupload-ui.js"); ?>"></script>
            <script src="<?php echo base_url(WASSETS . "file-uploader/js/main.js"); ?>"></script>

            <!--Jquery Smart Wizard-->

            <script src="<?php echo base_url(WASSETS . "jquery-smart-wizard/jquery.smartWizard.js"); ?>"></script>
            <link hfref="<?php // echo base_url(WASSETS . "jquery-smart-wizard/smart_wizard.css");   ?>" rel="stylesheet">


            <!--Assets-->
            <script src="<?php echo base_url(ASSETS . "noty/packaged/jquery.noty.packaged.min.js"); ?>"></script>
            <script src="<?php echo base_url(ASSETS . "noty/layouts/top.js"); ?>"></script>
            <script src="<?php echo base_url(ASSETS . "noty/themes/default.js"); ?>"></script>
            <script src="<?php echo base_url(WASSETS . "ckeditor/ckeditor.js"); ?>"></script>

            <!-- Advanced Data tables -->
            <!-- <script type="text/javascript" src="<?php echo base_url(WASSETS . "advanced-datatable/media/js/jquery.dataTables.js"); ?>"></script> -->
            <script type="text/javascript" src="<?php echo base_url(WASSETS . "advanced-datatable/media/js/jquery.dataTables10.min.js"); ?>"></script> 
            <script src="<?php echo base_url(WASSETS . "data-tables/DT_bootstrap.js"); ?>"></script>
            <script src="<?php echo base_url(WJS . "dynamic_table_init.js"); ?>"></script>

            <!-- Additional buttons for advanced data tablespoon -->
            
            <script src="<?php echo base_url(WASSETS . "advanced-datatable/media/js/dataTables.buttons.min.js"); ?>"></script>
            <script src="<?php echo base_url(WASSETS . "advanced-datatable/media/js/buttons.flash.min.js"); ?>"></script>
            <script src="<?php echo base_url(WASSETS . "advanced-datatable/media/js/jszip.min.js"); ?>"></script>
            <script src="<?php echo base_url(WASSETS . "advanced-datatable/media/js/pdfmake.min.js"); ?>"></script>
            <script src="<?php echo base_url(WASSETS . "advanced-datatable/media/js/vfs_fonts.js"); ?>"></script>
            <script src="<?php echo base_url(WASSETS . "advanced-datatable/media/js/buttons.html5.min.js"); ?>"></script>
            <script src="<?php echo base_url(WASSETS . "advanced-datatable/media/js/buttons.print.min.js"); ?>"></script>
            <script src="<?php echo base_url(WASSETS . "advanced-datatable/media/js/buttons.colVis.min.js"); ?>"></script>
            
            <!--common script for all pages-->
            <script src="<?php echo base_url(WJS . "common-scripts.js"); ?>"></script> 

            <!--Advanced Form Components-->
            <!-- <script src="<?php echo base_url(WJS . "advanced-form-components.js"); ?>"></script> -->

            <script src="<?php echo base_url(JS . "jquery.validate.min.js"); ?>"></script> 
            <script src="<?php echo base_url(JS . "additional-methods.min.js"); ?>"></script> 
            <script src="<?php echo base_url(WJS . "validate_rules.js"); ?>"></script> 

            <!-- bootstrap input-mask plugin -->
            <script src="<?php echo base_url(WASSETS . "bootstrap-inputmask/bootstrap-inputmask.min.js"); ?>"></script> 
            
            <script type="text/javascript" class="init">

                $(document).ready(function() {
                    $('#user-table').DataTable( {
                        dom: 'Bfrltip',
                        "scrollX": true,
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print', 'colvis'
                        ],
                    } );

                } );

                //owl carousel
                $(document).ready(function () {
                    $("#owl-demo").owlCarousel({
                        navigation: true,
                        slideSpeed: 300,
                        paginationSpeed: 400,
                        singleItem: true,
                        autoPlay: true

                    });
                });

                //custom select box
                $(function () {
                    $('select.styled').customSelect();
                });
                
                $("#medate_time").datetimepicker({format: 'yyyy-mm-dd hh:ii'});

            </script>

            <!-- The template to display files available for upload -->
            <script id="template-upload" type="text/x-tmpl">
                {% for (var i=0, file; file=o.files[i]; i++) { %}
                <tr class="template-upload fade">
                <td>
                <span class="preview"></span>
                </td>
                <td>
                <p class="name">{%=file.name%}</p>
                <strong class="error text-danger"></strong>
                </td>
                <td>
                <p class="size">Processing...</p>
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
                </td>
                <td>
                {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled>
                <i class="glyphicon glyphicon-upload"></i>
                <span>Start</span>
                </button>
                {% } %}
                {% if (!i) { %}
                <button class="btn btn-warning cancel">
                <i class="glyphicon glyphicon-ban-circle"></i>
                <span>Cancel</span>
                </button>
                {% } %}
                </td>
                </tr>
                {% } %}
            </script>

            <!-- The template to display files available for download -->
            <script id="template-download" type="text/x-tmpl">
                {% for (var i=0, file; file=o.files[i]; i++) { %}
                <tr class="template-download fade">
                <td>
                <span class="preview">
                {% if (file.thumbnailUrl) { %}
                <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                {% } %}
                </span>
                </td>
                <td>
                <p class="name">
                {% if (file.url) { %}
                <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                <span>{%=file.name%}</span>
                {% } %}
                </p>
                {% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
                {% } %}
                </td>
                <td>
                <span class="size">{%=o.formatFileSize(file.size)%}</span>
                </td>
                <td>
                {% if (file.deleteUrl) { %}
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                <i class="glyphicon glyphicon-trash"></i>
                <span>Delete</span>
                </button>
                <input type="checkbox" name="delete" value="1" class="toggle">
                {% } else { %}
                <button class="btn btn-warning cancel">
                <i class="glyphicon glyphicon-ban-circle"></i>
                <span>Cancel</span>
                </button>
                {% } %}
                </td>
                </tr>
                {% } %}
            </script>

        </section>

    </body>

</html>
