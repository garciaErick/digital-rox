<section id="main-content">
    <section class="wrapper site-min-height">
        <div class="row">
            <div class="col-md-6">
                <section class="panel">
                    <header class="panel-heading"><font color="black">Add Meeting</font></header>
                    <div class="panel-body">
                        <small>*Required Field</small>
                        <hr>
                        <form action="<?php echo site_url("workspace/meetings/editMeetingFunction") ?>" class="cmxform" role="form" method="post" id="addMeetingForm">
                            <div class="form-group">
                                <label for="places">Select Place</label><br>
                                <select class="form-control m-bot15" name="pname" id='pname'>
				  <?php if(isset($places)){ ?>
				  <?php foreach($places as $row){ ?>
                                    <option><?php echo $row->pname;?></option>
				  <?php } }?>
                                </select>
                                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#basicModal"><small>Add new place</small></button>
                            </div>
                            <div class="form-group">
                                <label for="metitle">*Meeting Title</label>
                                <input type="text" class="form-control" value="<?php echo $meeting[0]->metitle; ?>" name="metitle" id="metitle" placeholder="Enter Meeting Title">
                            </div> 
                            <div class="form-group">
                                <label for="medescription">Meeting Description</label>
                                <textarea rows="6" class="form-control" name="medescription" id="medescription"><?php echo $meeting[0]->medescription; ?></textarea>
                            </div> 
                            <div class="form-group hidden">
                                <label for="meetingAttachment">Meeting Attachment</label>
                                <input name="meetingAttachment" type="file" id="meetingAttachment">
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Date/Time</label>
                                <input name="medate_time" id="medate_time" size="16" type="text" value="<?php echo $meeting[0]->medate_time; ?>" readonly class="addMeeting_datetime form-control">
                            </div>
                            <input type="hidden" id="meid" name="meid"  value="<?php echo $meeting[0]->meid; ?>">
                            <button type="submit" class="btn btn-info">Submit</button>
                        </form>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
<div class="modal fade"  id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: rgb(35, 133, 161)">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" style="color: #ffffff">Create a new Place</h4>
            </div>
            <div class="modal-body">
                <form role="form" method="post" id="addplaceForm" name="addPlaceForm" onsubmit="addPlace('<?php echo site_url()?>'); return false">
                    <small>*Required Field</small>
                    <hr>
                    <div class="form-group">
                        <label for="pname">*Place Name</label>
                        <input type="text" class="form-control" id="pname" placeholder="Insert place name">
                    </div>
                    <div class="form-group">
                        <label for="pcity">*Place City</label>
                        <input type="text" class="form-control" id="pcity" placeholder="Insert place city">
                    </div>
                    <div class="form-group">
                        <label for="padress">*Place Adress</label>
                        <input type="text" class="form-control" id="padress" placeholder="Insert place adress">
                    </div>
                    <div class="form-group">
                        <label for="pcountry">*Place Country</label>
                        <input type="text" class="form-control" id="pcountry" placeholder="Insert place country">
                    </div>
                    <div class="form-group">
                        <label for="pwebsite">Place Website</label>
                        <input type="text" class="form-control" id="pwebsite" placeholder="Insert place website">
                    </div>
                    <div class="form-group">
                        <label for="pcomments">Place Comments</label>
                        <input type="text" class="form-control" id="pcomments" placeholder="Insert place comments">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <input type="submit" value="Submit" class="btn btn-primary"/>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
