<script type="text/javascript">
    function toggle_activity(uid){
        var id = "act" + uid;
        $("#"+id).toggle();
    }
</script>

<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- button group start -->
        <div class="btn-row">
            <div class="btn-group">
                <a href="<?php echo site_url("workspace/people")?>"><button class="btn btn-white" type="button">All</button></a>
                <a href="<?php echo site_url("workspace/people/members")?>"><button class="btn btn-white" type="button">Members</button></a>
                <a href="<?php echo site_url("workspace/people/students")?>"><button class="btn btn-white" type="button">Students</button></a>
                <a href="<?php echo site_url("workspace/people/officials")?>"><button class="btn btn-white" type="button">USDA Officials</button></a>
            </div>
        </div>
            
        <!-- page start-->
        <?php   
            $c = 2;
            foreach($member as $row){
                if ($c % 2 == 0){    
                    echo "<div class='directory-info-row'>";
                    echo "\n";
                    echo "<div class='row'>";
                } ?>
        
        <div class="col-md-6 col-sm-6">
            <div class="panel">
                <div class="panel-body">
                    <div class="media">
                        <div>
                            <div class="pull-left">
                                  <?php if ($row->upicture_link != NULL) {  
                                    echo '<img class="thumb media-object" src="data:image/jpeg;base64,'.base64_encode($row->upicture_link).'"/>';
                                  }  
                                else { ?>
                                <img class="thumb media-object" src="http://1.gravatar.com/avatar/173407badd2fd8beba911e4888bc73e9?s=1024&d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D1024&r=G" alt="Avatar">
                                <?php } ?>
                                <br/>
                                <div style="margin-right: 8px;">
                                    <button type="button" class="btn btn-primary btn-block" onclick="toggle_activity(<?php echo $row->uid ?>)">View Activities</button>
                                </div>
                            </div>
                            <div class="media-body">
                                <h4><?php echo $row->ufirst_name." ". $row->ulast_name; ?><span class="text-muted small"> - <?php echo $row->uarea; ?></span></h4>
                            <?php if($row->ueducation_level != NULL) { ?>
                                <span><?php echo $row->ueducation_level; ?></span><br/><br/>
                            <?php } ?>
                                <ul class="social-links">
                                      <?php if ($row->uwebsite != NULL){ ?>
                                    <li><a data-placement="top" data-toggle="tooltip" class="tooltips" href="<?php echo $row->uwebsite; ?>" target="_blank" data-original-title="Website"><i class="fa fa-globe"></i></a></li>
                                      <?php } ?>
                                      <?php  if ($row->ulinkedin != NULL){ ?>
                                    <li><a data-placement="top" data-toggle="tooltip" class="tooltips" href="<?php echo $row->ulinkedin; ?>" target="_blank" data-original-title="LinkedIn"><i class="fa fa-linkedin"></i></a></li>
                                      <?php } ?>
                                </ul>
                                <address>
                                    <strong><?php echo $row->uinstitution;?></strong><br>
                                      <?php echo $row->udepartment;?><br>
                                      <?php echo $row->uposition;?><br>
                                    <a href="mailto:<?php echo $row->uemail;?>" target="_top"><?php echo $row->uemail;?></a>
                                </address>
                                <hr>
                                <span>Team: <?php echo $row->uteam;?></span>      
                            </div>
                        </div>
                            
                        <div id="act<?php echo $row->uid; ?>" style="display: none; margin-top: 10px">
                            <?php if($row->uactivities != NULL) { ?>
                            <span><strong>Activities:</strong></span><br/>
                            <span><?php echo $row->uactivities; ?></span>
                            <?php } else { ?>
                            <div class="alert alert-warning fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                                <strong>No activities have been logged for this user.</strong>
                            </div>
                            <?php } ?>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <?php   
            if ($c % 3 == 0){ 
                echo "</div>";
                echo "\n";
                echo "</div>";           
            } $c++; 
            if ($c == 4)
                $c = 2;
            } ?>
        <!-- page end-->
    </section>
</section>
<!--main content end-->