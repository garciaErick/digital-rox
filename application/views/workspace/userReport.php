<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">  
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        User Report Panel
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <table id="user-table" class="display nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Start</th>
                                    <th>End</th>
                                    <th>Institution</th>
                                    <th>Education Lvl</th>
                                    <th>Expertise</th>
                                    <th>Activities</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if(isset($members)){ ?>
                                <?php foreach($members as $row){ ?> 
                                 <tr>
                                    <td><?php echo $row->uid ?></td>
                                    <td><?php echo $row->ufirst_name . ' ' . $row->ulast_name?></td>
                                    <td><?php echo $row->uemail ?></td>
                                    <td><?php echo $row->ustart_date ?></td>
                                    <td><?php echo $row->uend_date ?></td>
                                    <td><?php echo $row->uinstitution ?></td>
                                    <td><?php echo $row->ueducation_level ?></td>
                                    <td><?php echo $row->uarea ?></td>
                                    <td><?php echo $row->uactivities ?></td>
                                  </tr>
                                <?php } ?>
                            <?php } ?>   
                            </tbody>
                        </table>                        
                    </div>
                </section>
            </div>
        </div>
        <!-- end resource panel-->
    </section>
</section>
<!--main content end-->