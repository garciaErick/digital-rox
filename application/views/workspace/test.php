
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Collaborate [zip]
                    </header>
                    <div class="panel-body">
                        <div ng-app="store">

                            <div ng-controller="StoreController as store">
                                <div data-ng-hide="store.product.soldOut">
                                    <input type="text" ng-model="name">
                                    <div data-ng-show="name">Hello {{name}}</div>
                                    <h1> {{store.product[1].name}} </h1>
                                    <h2> {{store.product[1].price | currency}} </h2>
                                    <p> {{store.product[1].description}} </p>
                                    <button data-ng-show="store.product.canPurchase">Add to Cart</button>
                                    <ul>
                                        <li data-ng-repeat="product in store.product"> {{product.name}}</li>
                                    </ul>
                                </div>
                                <section data-ng-controller="PanelController as panel">
                                    <section ng-init="tab = 1">
                                        <ul class="nav nav-pills">
                                            <li ng-class="{ active:tab == 1}">
                                                <a href ng-click="panel.selectTab(1)">Description</a>
                                            </li>
                                            <li ng-class="{ active:tab == 2}">
                                                <a href ng-click="panel.selectTab(2)">Specifications</a>
                                            </li>
                                            <li ng-class="{ active:tab == 3}">
                                                <a href ng-click="panel.selectTab(3)">Reviews</a>
                                            </li>
                                        </ul>
                                    </section>
                                    <div class="panel" ng-show="panel.isSelected(1)" >
                                        <h4>Description</h4>
                                        <p>{{store.product[0].description}}</p>
                                    </div>
                                    <div class="panel" ng-show="panel.isSelected(2)" >
                                        <h4>Specifications</h4>
                                        <blockquote>This are some specifications</blockquote>
                                    </div>
                                    <div class="panel" ng-show="panel.isSelected(3)" >
                                        <h4>Reviews</h4>
                                        <blockquote>Write some reviews</blockquote>
                                </section>
                            </div>
                        </div>
                    </div>
            </div>
    </section>
    </div>
    </div>
    <!-- page end-->
</section>
</section>
<!--main content end-->
