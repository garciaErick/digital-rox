<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <h2>Meeting Calendar</h2>
        <?php if(isset($meetings)){ ?>
        <?php foreach($meetings as $row){ 
            $date = date_create($row->medate_time);
        ?>
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        <?php echo $row->metitle;  ?>
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                        </span>
                        
                        <div class="pull-right"><?php echo date_format($date, 'g:ia \o\n l jS F Y'); ?></div>
                    </header>
                    <div class="panel-body">
                        <?php echo $row->medescription; ?> <br><br>
                        <!-- need to put the place here -->
                        <?php echo $row->pname; ?> <br>
                        <?php echo $row->padress; ?> <br>
                        <?php echo $row->pcity.", ". $row->pcountry; ?> <br>
                        <?php if(isset($row->pwebsite) && $row->pwebsite != "")
                            echo $row->pwebsite;
                        ?>
                        <br>
                        <?php if(isset($row->pcomments) && $row->pcomments != "" && $row->pcomments != 0)
                            echo $row->pcomments;
                        ?>

                    </div>  
                </section>   
            </div>    
        </div>
        <?php } ?> 
        <?php } ?> 
    </section>
</section>

