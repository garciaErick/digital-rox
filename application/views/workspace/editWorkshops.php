<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        Edit Events
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <div class="adv-table" id="dynamic"> 
                            <table class="display table table-bordered table-striped" id="hidden-table-info">
                                <thead>
                                    <tr>
                                        <th>Event ID </th>
                                        <th>Title       </th>
                                        <th>Date        </th>
                                        <th>Upload Date </th>
                                        <th>Edit        </th>
                                        <th>Delete      </th>
                                    </tr>
                                </thead>
                                <tbody>
                                      <?php if(isset($workshops)){ ?>
                                        <?php foreach($workshops as $row){ ?>
                                    <tr>
                                        <td><?php echo $row->wid;          ?></td>
                                        <td><?php echo $row->wtitle;       ?></td>
                                        <td><?php echo $row->wdate_time;   ?></td>
                                        <td><?php echo $row->wupload_date; ?></td>
                                        <td class="text-center"><a href="<?php echo site_url("workspace/workshops/editWorkshopInfo?wid=$row->wid")?>" target="_blank" ><i class="fa fa-pencil fa-2x"></i></a></td> <!-- get request for details -->
                                        <td class="text-center">
                                            <a href="<?php echo site_url("workspace/workshops/deleteWorkshopFunction?wid=$row->wid")?>">
                                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                            </button>
                                        </td>
                                    </tr>
                                         <?php } ?>
                                     <?php } ?>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
