<!--footer start-->
<footer class="site-footer">
    <div class="text-center">
        2016 &copy; University of Texas at El Paso
        <a href="#" class="go-top">
            <i class="fa fa-angle-up"></i>
        </a>
    </div>
</footer>
<!--footer end-->
