<section id="main-content">
  <section class="wrapper site-min-height">
  <div class="row">
    <div class="col-md-6">
    <section class="panel">
      <header class="panel-heading"><font color="black">Create Query</font></header>
      <div class="panel-body">
      <small>*Required Field</small>
      <hr>
        <form "cmxform" role="form" method="post" id="addQueryForm" name="addQueryForm" onsubmit="addQuery('<?php echo site_url() ?>'); return false;">
        <div class="form-group">
        <label for="qstatement">*Query Statement</label>
        <input type="text" class="form-control" name="qstatement" id="qstatement" placeholder="Enter query statement">
        </div>
        <div class="form-group">
        <label for="qcomments">Query Comments</label>
        <input type="text" class="form-control" name="qcomments" id="qcomments" placeholder="Enter query comments">
        </div>
        <button type="submit" class="btn btn-info">Submit</button>
      </form>
      </div>
    </section>
    </div>
  </div>
  </section>
</section>
