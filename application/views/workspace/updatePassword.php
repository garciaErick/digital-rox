<section id="main-content">
    <section class="wrapper site-min-height">
        <div class="row">
            <div class="col-md-6">
                <section class="panel">
                    <header class="panel-heading"><font color="black">Update My Password</font></header>
                    <div class="panel-body">
                        <form class="cmxform" role="form" id="updatePasswordForm" name="updatePasswordForm" onsubmit="changePassword('<?php echo site_url() ?>'); return false">
                            <div class="form-group">
                                <label for="oldPassword">Old Password</label>
                                <input type="password" class="form-control" id="oldPassword" placeholder="Please type your previous password" required minlength="5">
                            </div> 
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" id="password" placeholder="Password" required minlength="5">
                            </div> 
                            <div class="form-group">
                                <label for="confirmPassword">Confirm Password</label>
                                <input type="password" class="form-control" id="confirmPassword" placeholder="Re-type your Password" required minlength="5">
                            </div>                 
                            <button type="submit" class="btn btn-info">Change Password</button>
                        </form>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>