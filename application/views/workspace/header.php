<!--header start-->
      <header class="header white-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="" class="logo">Digital - <span>ROX</span></a>
            <!--logo end-->
            <div class="nav notify-row" id="top_menu">
                <!--  notification start -->
                <ul class="nav top-menu">
                    <li><a href="<?php echo site_url();?>" role="button" class="btn btn-navbar"><i class="fa fa-home"></i> Home</a></li>
                </ul>
                <!--  notification end -->
            </div>
            <div class="top-nav ">
                <!--search & user info start-->
                <ul class="nav pull-right top-menu">
                    <!-- user login dropdown start-->
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <!-- <img alt="X" src="<?php echo base_url(WIMAGES."");?>"> -->
                            <span class="username"><?php echo $this->session->userdata('email') ?></span>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu extended logout">
                            <div class="log-arrow-up"></div>
                            <!-- <li><a href="#"><i class=" fa fa-suitcase"></i>Profile</a></li> -->
                            <!-- <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li> -->
                            <!-- <li><a href="#"><i class="fa fa-bell-o"></i> Notification</a></li> -->
                            <li><a href="<?php echo base_url('user/logout') ?>"><i class="fa fa-key"></i> Log Out</a></li>
                        </ul>
                    </li>
                    <!-- user login dropdown end -->
                </ul>
                <!--search & user info end-->
            </div>
        </header>
      <!--header end-->
