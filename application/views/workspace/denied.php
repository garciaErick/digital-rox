<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- Content Header (Page header) -->
        <section class="content-header text-center">
            <h1>
                Access Denied
            </h1>
        </section>
        <!-- Main content -->
        <section class="content text-center">
            <div class="error-page">
                <div class="error-content">
                    <h3><i class="fa fa-warning text-danger"></i></h3>
                    <p>
                       You do not have permission to access this section.  
                    </p>
                </div>
            </div><!-- /.error-page -->
        </section>
    </section>
    <!-- /.content -->
</div><!-- /.content-wrapper -->
