<?php
foreach ($results as $row) {
        $uid = $row->uid;
	$firstName = $row->ufirst_name;
	$lastName  = $row->ulast_name;
	$email	 = $row->uemail;
	$picture_link = $row->upicture_link;
	$institution = $row->uinstitution;
	$position = $row->uposition;
	$department = $row->udepartment;
	$linkedin = $row->ulinkedin;
	$website = $row->uwebsite;
	$team = $row->uteam;
	$area = $row->uarea;
        $education = $row->ueducation_level;
        $activities = $row->uactivities;
        $startDate = $row->ustart_date;
        $endDate = $row->uend_date;
}

?>
<section id="main-content">
	<section class="wrapper site-min-height">
		<div class="row">
			<div class="col-md-6">
				<section class="panel">
					<header class="panel-heading"><font color="black">Update Information</font></header>
					<div class="panel-body">
						<form class="cmxform" role="form" id="updateInformationForm" name="updateInformationForm" onsubmit="updateInformation('<?php echo site_url() ?>'); return false">
                                                    <input type="hidden" value="<?php echo $uid ?>" id="uid" name="uid">
                                                        <div class="form-group">
								<label for="firstName">First Name</label>
								<input type="text" class="form-control" id="firstName" placeholder="Enter first name" value="<?php echo $firstName ?>" minlength="2" required>
							</div>
							<div class="form-group">
								<label for="lastName">Last Name</label>
								<input type="text" class="form-control" id="lastName" placeholder="Enter last name" value="<?php echo $lastName ?>" minlength="2" required>
							</div>
							<div class="form-group">
								<label for="email">Email address</label>
								<input type="email" class="form-control" id="email" placeholder="Enter email" value="<?php echo $email ?>" required>
							</div>
							<div class="form-group">
								<label for="picture">Picture Link</label>
								<input type="text" class="form-control" id="picture" placeholder="Enter avatar url" value="<?php echo $picture_link ?>">
							</div> 
							<div class="form-group">
								<label for="institution">Institution</label>
								<input type="text" class="form-control" id="institution" placeholder="Enter institution name" value="<?php echo $institution ?>">
							</div>  
							<div class="form-group">
								<label for="position">Position</label>
								<input type="text" class="form-control" id="position" placeholder="Enter position name" value="<?php echo $position ?>">
							</div>	 
							<div class="form-group">
								<label for="department">Department</label>
								<input type="text" class="form-control" id="department" placeholder="Enter department name" value="<?php echo $department ?>">
							</div> 
							<div class="form-group">
								<label for="linkedin">Linked-In</label>
								<input type="text" class="form-control" id="linkedin" placeholder="Enter Linked-In site url" value="<?php echo $linkedin ?>">
							</div>  
							<div class="form-group">
								<label for="website">Personal Website</label>
								<input type="text" class="form-control" id="website" placeholder="Enter personal site url" value="<?php echo $website ?>">
							</div>  
							<div class="form-group">
								<label for="team">Team</label>
								<input type="text" class="form-control" id="team" placeholder="Enter belonging team" value="<?php echo $team ?>">
							</div>  
							<div class="form-group">
                                                            <label for="area">Expertise Area</label>
                                                            <input type="text" class="form-control" id="area" placeholder="Enter area of expertise" value="<?php echo $area ?>">
                                                        </div>	
                                                        <div class="form-group">
                                                            <label for="education">Education Level & Degree</label>
                                                            <input type="text" class="form-control" id="education" value="<?php echo $education ?>">
                                                        </div>
                                                    <div class="form-group">
                                                        <label for="activities">Activities</label>
                                                        <textarea class="form-control"  id="activities"  rows="6"><?php echo $activities; ?></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="startDate">Start Date (yyyy-mm-dd)</label>
                                                        <input type="text" placeholder data-mask="9999-99-99" class="form-control" id="startDate" value="<?php echo $startDate; ?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="endDate">End Date (yyyy-mm-dd)</label>
                                                        <input type="text" placeholder data-mask="9999-99-99" class="form-control" id="endDate" value="<?php echo $endDate; ?>">
                                                    </div>
							<button type="submit" class="btn btn-info">Update Information</button>
						</form>
					</div>
				</section>
			</div>
		</div>
	</section>
</section>
