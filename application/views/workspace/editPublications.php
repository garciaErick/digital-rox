<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        Edit Publications
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <div class="adv-table" id="dynamic"> 
                            <table class="display table table-bordered table-striped" id="hidden-table-info">
                                <thead>
                                    <tr>
                                        <th>Publication ID </th>
                                        <th>Title       </th>
                                        <th>Date        </th>
                                        <th>Edit        </th>
                                        <th>Delete      </th>
                                    </tr>
                                </thead>
                                <tbody>
                                      <?php if(isset($publications)){ ?>
                                        <?php foreach($publications as $row){ ?>
                                    <tr>
                                        <td><?php echo $row->pid;          ?></td>
                                        <td><?php echo $row->ptitle;       ?></td>
                                        <td><?php echo $row->pdate; ?></td>
                                        <td class="text-center">
                                            <a href="<?php echo site_url("workspace/publications/editPublicationView?pid=$row->pid")?>">
                                                <i class="fa fa-pencil fa-2x"></i></a></td>
                                        <td class="text-center">
                                           <a href="<?php echo site_url("workspace/publications/deletePublicationFunction?pid=$row->pid")?>">
                                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                            </button>
                                        </td>
                                    </tr>
                                         <?php } ?>
                                     <?php } ?>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
