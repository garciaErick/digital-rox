<!--main content start-->
<section id="main-content">
  <section class="wrapper site-min-height">
    <div class="row">
      <div class="col-sm-12">
        <section class="panel">
          <header class="panel-heading">
            Edit Query
            <span class="tools pull-right">
              <a href="javascript:;" class="fa fa-chevron-down"> </a>
              <a href="javascript:;" class="fa fa-times">        </a>
            </span>
          </header>
          <div class="panel-body">
            <div class="adv-table" id="dynamic"> 
              <table class="display table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>query id  </th>
                    <th>statement </th>
                    <th>Comments  </th>
                  </tr>
                </thead>
                <tbody>
				<?php if(isset($queries)){ ?>
				  <?php foreach($queries as $row){ ?>
                  <tr>
                    <td><?php echo $row->qid;        ?></td>
                    <td><?php echo $row->qstatement; ?></td>
                    <td><?php echo $row->qcomments;  ?></td>
                  </tr>
				  <?php } ?>
				<?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </section>
      </div>
    </div>
  </section>
</section>
