<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <!-- top panel (heading, title, description, user and upload date -->
        
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        Collection Details
                    </header>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-9">
                                <h3><?php echo $resource[0]['0Title'];?></h3>
                            </div>
                            <div class="col-md-3">
                                Contributor: <?php echo $resource[0]['Uploader'];?> <br>
                                Upload Date: <?php echo $resource[0]['Upload_Date'];?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <?php echo $resource[0]['2Description']; ?>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- end of top panel -->
        
        <!-- metadata and file panel -->
        <div class="row">
            <!-- metadata panel -->
            <div class="col-md-6">
                <section class="panel">
                    <header class="panel-heading">
                        Metadata
                    </header>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12" style="min-height: 400px">
                                   <?php while (list($key, $val) = each($resource[0])) {
                                       $array = array("Title", "Description", "Uploader", "Upload_Date", "User_ID", "URL");
                                        if (!wildcard_in_array($key, $array)){
                                             $key2 = preg_replace('/[0-9]+/', '', $key);
                                             echo "<b> $key2 : </b>$val<hr>";
                                        } 
                                   } ?>
                            </div>
                        </div>
                    </div>
                </section>                
            </div>
            
            <!-- file panel -->
            <div class="col-md-6">
                <section class="panel">
                    <header class="panel-heading">
                        File Resources
                    </header>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12" style="min-height: 200px">
                                <table class="display table table-bordered table-responsive">
                                    <tbody>
                                        <?php foreach($files as $file){ ?>
                                        <tr>
                                            <td><?php echo basename($file->rfpath);?> </td>
                                            <td class="text-center" style="width: 10%"><a target="_blank" href="<?php echo base_url('resources/workspace/assets/file-uploader/server/php/files/'.$file->cid_fk.'/'.$file->rfpath); ?>" ><i class="fa fa-download fa-2x"></i></a></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>     
                                </table>
                            </div>
                        </div>
                    </div>
                    <header class="panel-heading">
                        Web Resources
                    </header>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12" style="min-height: 200px">
                                  <?php  $resource_beta = $resource[0]?>  
                                  <?php while (list($key, $val) = each($resource_beta)) {
                                       $array = array("URL");
                                        if (wildcard_in_array($key, $array)){
                                             $key3 = preg_replace('/[0-9]+/', '', $key);
                                             echo "<b> $key3 : </b>$val<hr>";
                                        } 
                                   } ?>
                            </div>    
                        </div>
                    </div>
                    
                </section>                   
            </div>
        </div>
        <!-- page end -->
        
    </section>
</section>
<!--main content end-->

