<!--sidebar start-->
<aside>
  <div id="sidebar"  class="nav-collapse ">
  <!-- sidebar menu start-->
  <ul class="sidebar-menu" id="nav-accordion">
    <li>
    <a href="<?php echo site_url("workspace/dashboard")?>">
      <i class="fa fa-dashboard"></i>
      <span>Dashboard</span>
    </a>
    </li>
    <!-- Not yet available
    <li>
    <a href="<?php echo site_url("workspace/people")?>">
                  <i class="fa fa-group"></i><span>People</span>
    </a>
    </li>
    -->

    <li class="sub-menu">
    <a  href="javascript:;">
      <i class="fa fa-user"></i>
      <span>User</span>
    </a>
    <ul class="sub">
      <?php if($this->session->userdata('is_admin') == 1) { ?>
        <li><a  href="<?php echo site_url("workspace/userAdmin/createUser");?>">Create User</a></li>
        <li><a  href="<?php echo site_url("workspace/userAdmin/editUsers");?>">Edit Users</a></li>
      <?php } ?>
      <li><a  href="<?php echo site_url("workspace/userAdmin/updateInformation");?>">Update My Information</a></li>
      <li><a  href="<?php echo site_url("workspace/userAdmin/updatePassword");?>">Update My Password</a></li>
    </ul>
    </li>
    <li class="sub-menu">
    <a href="javascript:;" >
      <i class="fa fa-search"></i>
      <span>Queries</span>
    </a>
    <ul class="sub">
      <li><a  href="<?php echo site_url("workspace/query/addQuery")?>"><i class="fa"></i>Create</a></li>
      <li><a  href="<?php echo site_url("workspace/query/editQuery")?>"><i class="fa"></i>Edit/View</a></li>
    </ul>
    </li>

  </ul>
  <!-- sidebar menu end-->
  </div>
</aside>
<!--sidebar end-->

