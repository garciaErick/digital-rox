<section id="main-content">
	<section class="wrapper site-min-height">
		<div class="row">
			<div class="col-md-6">
				<section class="panel">
					<header class="panel-heading"><font color="black">Edit Publication</font></header>
					<div class="panel-body">
						<small>*Required Field</small>
						<hr>
						<form role="form"   id="editPublicationForm"    name="editPublicationForm"  onsubmit="editPublicationjs('<?php echo site_url() ?>', <?php echo $publications[0]->pid ?>); return false">
							<div class="form-group">

								<?php if(isset($publications)){ ?>
								<?php foreach($publications as $row){ ?>
								<?php } }?>

								<label for="title">*Title</label>
								<input type="text" class="form-control" value="<?php echo $publications[0]->ptitle; ?>" name="ptitle" id="ptitle" placeholder="Enter Publication Title">
							</div>
							<div class="form-group">
								<label data-toggle="tooltip" data-placement="top"> *Description </label>
								<textarea rows="6" class="form-control" name="pdescription" id="pdescription"><?php echo $publications[0]->pdescription; ?></textarea>
							</div>
							<div class="form-group">
								<label for="link">*Publication Link</label>
								<input type="text" class="form-control" value="<?php echo $publications[0]->pcontent_link; ?>" name="pcontent_link" id="pcontent_link" placeholder="Enter Publication Link">
							</div>
							<div class="form-group">
								<label for="language">*Publication Language</label>
								<input type="text" class="form-control" value="<?php echo $publications[0]->planguage; ?>" name="planguage" id="planguage" placeholder="Enter Publication Language">
							</div> 
							<div class="form-group">
								<label for="subject">*Publication Subject</label>
								<input type="text" class="form-control" value="<?php echo $publications[0]->psubject; ?>" name="psubject" id="psubject" placeholder="Enter Publication Subject">

							</div> 
							<div class="form-group">
								<label for="source">*Publication Source</label>
								<input type="text" class="form-control" value="<?php echo $publications[0]->psource; ?>" name="psource" id="psource" placeholder="Enter the source of the publication">

							</div> 
							<div class="form-group">
								<label data-toggle="tooltip" data-placement="top" title=""> Date </label> <br>
								<div class="col-md-12">
									<div data-date-viewmode="years" data-date-format="dd-mm-yyyy" data-date="" class="input-append date dpYears">
										<input type="text" placeholder data-mask="9999-99-99" class="form-control" name="wdate_time" id="wdate_time">
									</input>
								</div>
							</div>
						</div><br><br>
						<div class="form-group">
							<label for="format">Format</label>
							<input type="text" class="form-control" value="<?php echo $publications[0]->pformat; ?>" name="pformat" id="pformat" placeholder="Enter format of the publication">
						</div>	
						<button type="submit" class="btn btn-info">Submit</button>
					</form>
				</div>
			</section>
		</div>
	</div>
</section>
</section>
