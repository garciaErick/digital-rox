<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        Edit Meetings
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <div class="adv-table" id="dynamic"> 
                            <table class="display table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Meeting ID</th>
                                        <th>Title</th>
                                        <th>Date</th>
                                        <th>Upload Date</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(isset($meetings)){ ?>
					<?php foreach($meetings as $row){ ?>
                                    <tr>
                                        <td><?php echo $row->meid; ?></td>
                                        <td><?php echo $row->metitle; ?></td>
                                        <td><?php echo $row->medate_time; ?></td>
                                        <td><?php echo $row->meupload_date; ?></td>
                                        <td class="text-center">
                                            <a href="<?php echo site_url("workspace/meetings/editMeetingView?meid=$row->meid")?>">
                                                <i class="fa fa-pencil fa-2x"></i></a></td>
                                        <td class="text-center">
                                           <a href="<?php echo site_url("workspace/meetings/deleteMeetingFunction?meid=$row->meid")?>">
                                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                           </a>
                                        </td>
                                    </tr>
					 <?php } ?>
                                    <?php } ?>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
