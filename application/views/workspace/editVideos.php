<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        Edit Videos	
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <div class="adv-table" id="dynamic"> 
                            <table class="display table table-bordered table-striped" id="hidden-table-info">
                                <thead>
                                    <tr>
                                        <th>Video ID </th>
                                        <th>Title       </th>
                                        <th>Upload Date </th>
                                        <th>Edit        </th>
                                        <th>Delete      </th>
                                    </tr>
                                </thead>
                                <tbody>
                                      <?php if(isset($videos)){ ?>
                                        <?php foreach($videos as $row){ ?>
                                    <tr>
                                        <td><?php echo $row->vid;          ?></td>
                                        <td><?php echo $row->vtitle;       ?></td>
                                        <td><?php echo $row->vupload_date; ?></td>
                                        <td class="text-center"><a href="<?php echo site_url("workspace/videos/editVideoInfo?vid=$row->vid")?>" target="_blank" ><i class="fa fa-pencil fa-2x"></i></a></td> <!-- get request for details -->
                                        <!-- get request for details -->
                                        <td class="text-center">
                                         <a href="<?php echo site_url("workspace/videos/deleteVideoFunction?vid=$row->vid")?>">
                                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                            </button>
                                            </button>
                                        </td>
                                    </tr>
                                         <?php } ?>
                                     <?php } ?>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
