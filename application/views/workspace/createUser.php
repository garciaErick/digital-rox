<section id="main-content">
  <section class="wrapper site-min-height">
    <div class="row">
      <div class="col-md-6">
        <section class="panel">
          <header class="panel-heading"><font color="black">Create User</font></header>
          <div class="panel-body">
            <small>*Required Field</small>
            <hr>
            <form class="cmxform" role="form" method="post" id="createUserForm" name="createUserForm" enctype="multipart/form-data" action="<?php echo site_url("workspace/userAdmin/createUserFunction")?>">
              <div class="form-group">
                <label for="firstName">*First Name</label>
                <input type="text" class="form-control" name="firstName" id="firstName" placeholder="Enter first name" minlength="2" required>
              </div>
              <div class="form-group">
                <label for="lastName">*Last Name</label>
                <input type="text" class="form-control" name="lastName" id="lastName" placeholder="Enter last name" minlength="2" required>
              </div>
              <div class="form-group">
                <label for="email">*Email address</label>
                <input type="email" class="form-control" name="email" id="email" placeholder="Enter email" required>
              </div>
              <div class="form-group">
                <label for="password">*Password</label>
                <input type="password" class="form-control" name="password" id="password" placeholder="Password" minlength="5" required>
              </div> 
              <div class="form-group">
                <label for="confirmPassword">*Confirm Password</label>
                <input type="password" class="form-control" name="passConf" id="confirmPassword" placeholder="Re-type your Password" minlength="5" required>
              </div> 
              <div class="form-group">
                <label for="picture">Profile Picture (only jpeg allowed)</label>
                <input type="file" id="picture" name="picture"/>
              </div>
              <div class="form-group">
                <input type="checkbox" value="1" name="isUserAdmin" id="isUserAdmin"><label>&nbsp;Workspace Administrator</label>   
              </div>
              <button type="submit" class="btn btn-info">Submit</button>
            </form>
          </div>
        </section>
      </div>
    </div>
  </section>
</section>
