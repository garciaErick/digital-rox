<section id="main-content">
	<section class="wrapper site-min-height">
		<div class="row">
			<div class="col-md-6">
				<section class="panel">
					<header class="panel-heading"><font color="black">Add a Publication</font></header>
          <div class="panel-body">
            <small>*Required Field</small>
            <hr>
            <form role="form" method="post" id="addPublicationForm" name="addPublicationForm" onsubmit="addPublication('<?php echo site_url() ?>'); return false">
							<div class="form-group">
								 <label for="title">*Title</label>
								 <input type="text" class="form-control" id="ptitle" placeholder="Enter publication title">
							</div>
							<div class="form-group">
								 <label data-toggle="tooltip" data-placement="top"> *Description </label>
								 <textarea class="form-control" cols="60" rows="5" id="pdescription"></textarea>
							</div>
							<div class="form-group">
								 <label for="link">*Publication Link</label>
								 <input type="text" class="form-control" id="pcontent_link" placeholder="Enter link with the publication content">
							</div>
							<div class="form-group">
								 <label for="language">*Publication Language</label>
								 <input type="text" class="form-control" id="planguage" placeholder="Enter language of the publication">
							</div> 
							<div class="form-group">
								 <label for="subject">*Publication Subject</label>
								 <input type="text" class="form-control" id="psubject" placeholder="Enter the subject of the publication">
							</div> 
							<div class="form-group">
								 <label for="source">*Publication Source</label>
								 <input type="text" class="form-control" id="psource" placeholder="Enter the source of the publication">
							</div> 
              <div class="form-group">
                <label data-toggle="tooltip" data-placement="top" title=""> Date </label> <br>
                <div class="col-md-12">
                  <div data-date-viewmode="years" data-date-format="dd-mm-yyyy" data-date="" class="input-append date dpYears">
                    <input type="text" placeholder data-mask="9999-99-99" class="form-control" name="wdate_time" id="wdate_time">
                    </span>
                    </input>
                  </div>
                </div>
              </div><br><br>
							<div class="form-group">
								 <label for="format">Format</label>
								 <input type="text" class="form-control" id="pformat" placeholder="Enter format of the publication">
							</div>	
              <button type="submit" class="btn btn-info">Submit</button>
						</form>
					</div>
				</section>
			</div>
		</div>
	</section>
</section>
