<section id="main-content">
	<section class="wrapper site-min-height">
		<div class="row">
			<div class="col-md-12">
				<section class="panel">
					<header class="panel-heading"><font color="black">Add a Video</font></header>
				<div class="panel-body">
				<small>*Required Field</small>
            	<hr>
				<form class="form"  id="editVideoForm"          name="editVideoForm"        onsubmit="editVideojs('<?php echo site_url() ?>', <?php echo $videos[0]->vid; ?>); return false;">

							<div class="form-group">

							<?php if(isset($videos)){ ?>
							<?php foreach($videos as $row){ ?>
							<?php } }?>

								<label for="title">Title</label>
								<input type="text" class="form-control" id="vtitle" placeholder="Enter publication title" value="<?php echo $videos[0]->vtitle; ?>">
							</div>
							<div class="form-group">
								<label data-toggle="tooltip" data-placement="top"> Description </label>
								<textarea class="form-control" cols="60" rows="5" id="vdescription"><?php echo $videos[0]->vdescription; ?></textarea>
							</div>
							<div class="form-group">
								<label for="subject">Subject</label>
								<input type="text" class="form-control" id="vsubject" placeholder="Enter the subject of the publication" value="<?php echo $videos[0]->vsubject; ?>">
							</div> 
							<div class="form-group">
								<label for="link">Video Link</label>
								<input type="text" class="form-control" id="vvideo_link" placeholder="Enter link with the publication content" value="<?php echo $videos[0]->vvideo_link; ?>">
							</div>
							<div class="form-group">
								<label for="format">Additional URL</label>
								<input type="text" class="form-control" id="vadditional_url" placeholder="Enter an additional url if necessary" value="<?php echo $videos[0]->vadditional_url; ?>">
							</div>	
							<button type="submit" class="btn btn-info">Edit</button>
						</form>
					</div>
				</section>
			</div>
		</div>
	</section>
</section>
