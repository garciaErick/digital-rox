<section id="main-content">
    <section class="wrapper site-min-height">
        <div class="row">
            <div class="col-md-6">
                <section class="panel">
                    <header class="panel-heading"><font color="black">Create User</font></header>
                    <div class="panel-body">
                        <small>*Required Field</small>
                        <hr>
                        <form class="cmxform" role="form" method="post" id="createUserForm" name="createUserForm" onsubmit="createUser('<?php echo site_url() ?>'); return false">
                            <div class="form-group">
                                <label for="firstName">*First Name</label>
                                <input type="text" class="form-control" id="firstName" placeholder="Enter first name" minlength="2" required>
                            </div>
                            <div class="form-group">
                                <label for="lastName">*Last Name</label>
                                <input type="text" class="form-control" id="lastName" placeholder="Enter last name" minlength="2" required>
                            </div>
                            <div class="form-group">
                                <label for="email">*Email address</label>
                                <input type="email" class="form-control" id="email" placeholder="Enter email" required>
                            </div>
                            <div class="form-group">
                                <label for="password">*Password</label>
                                <input type="password" class="form-control" id="password" placeholder="Password" minlength="5" required>
                            </div> 
                            <div class="form-group">
                                <label for="confirmPassword">*Confirm Password</label>
                                <input type="password" class="form-control" id="confirmPassword" placeholder="Re-type your Password" minlength="5" required>
                            </div> 
                            <div class="form-group">
                                <label for="institution">Institution</label>
                                <input type="text" class="form-control" id="institution" placeholder="Enter institution name">
                            </div>  
                            <div class="form-group">
                                <label for="position">Position</label>
                                <input type="text" class="form-control" id="position" placeholder="Enter position name">
                            </div>     
                            <div class="form-group">
                                <label for="department">Department</label>
                                <input type="text" class="form-control" id="department" placeholder="Enter department name">
                            </div> 
                            <div class="form-group">
                                <label for="linkedin">Linked-In</label>
                                <input type="text" class="form-control" id="linkedin" placeholder="Enter Linked-In site url">
                            </div>  
                            <div class="form-group">
                                <label for="website">Personal Website</label>
                                <input type="text" class="form-control" id="website" placeholder="Enter personal site url">
                            </div>  
                            <div class="form-group">
                                <label for="team">Team</label>
                                <input type="text" class="form-control" id="team" placeholder="Enter belonging team">
                            </div>  
                            <div class="form-group">
                                <label for="area">Expertise Area</label>
                                <input type="text" class="form-control" id="area" placeholder="Enter area of expertise">
                            </div>
                            <div class="form-group">
                                <label for="education">Education Level & Degree</label>
                                <input type="text" class="form-control" id="education" placeholder="EG: M.S. in Civil Engineering">
                            </div>
                            <div class="form-group">
                                <label for="activities">Activities</label>
                                <textarea class="form-control"  id="activities"  rows="6"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="startDate">Start Date (yyyy-mm-dd)</label>
                                <input type="text" placeholder data-mask="9999-99-99" class="form-control" id="startDate">
                            </div>
                            <p>Select user roles (can be more than one role).</p>
                            <div class="form-group">
                                <input type="checkbox" id="isUserAdmin"><label>&nbsp;Workspace Administrator</label>   
                            </div>
                            <div class="form-group">
                                <input type="checkbox" id="isCollaborator"><label>&nbsp;Workspace Collaborator</label>   
                            </div>
                            <div class="form-group">
                                <input type="checkbox" id="isContentManager"><label>&nbsp;Site Content Manager</label>   
                            </div>
                            <div class="form-group">
                                <input type="checkbox" id="isStudent"><label>&nbsp;Student Participant</label>   
                            </div>
                            <div class="form-group">
                                <input type="checkbox" id="isUSDAOfficial"><label>&nbsp;USDA Official</label>   
                            </div>
                            <button type="submit" class="btn btn-info">Submit</button>
                        </form>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>