<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        Edit Users
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                        </span>
                    </header>
                    <div class="panel-body">
                        <div class="adv-table" id="dynamic"> 
                            <table class="display table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>User ID</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Register Date</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(isset($users)){ ?>
					<?php foreach($users as $row){ ?>
                                    <tr>
                                        <td><?php echo $row->uid; ?></td>
                                        <td><?php echo $row->ufirst_name.' '.$row->ulast_name; ?> </td>
                                        <td><?php echo $row->uemail; ?></td>
                                        <td><?php echo $row->ureg_date; ?></td>
                                        <td class="text-center">
                                            <a href="<?php echo site_url("workspace/userAdmin/editUserView?uid=$row->uid")?>">
                                                <i class="fa fa-pencil fa-2x"></i></a></td>
                                        <td class="text-center">
                                           <a href="<?php echo site_url("workspace/userAdmin/deleteUserFunction?uid=$row->uid")?>">
                                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                           </a>
                                        </td>
                                    </tr>
					 <?php } ?>
                                    <?php } ?>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>

