<script type="text/javascript">
var resources = <?php echo json_encode($resources); ?>;
var total_resources = <?php echo $total_resources; ?>;
var total_users = <?php echo $total_users; ?>;
var total_bookmarks = <?php echo $total_bookmarks; ?>;
</script>

<!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <!--state overview start-->
        <div class="row state-overview">
          <div class="col-lg-4 col-sm-6">
            <section class="panel">
              <div class="symbol red">
                <i class="fa fa-linux"></i>
              </div>
              <div class="value">
                <h1 class=" count2">
                  0
                </h1>
                <p>Queries</p>
              </div>
            </section>
          </div>
          <div class="col-lg-4 col-sm-6">
            <section class="panel">
              <div class="symbol terques">
                <i class="fa fa-github"></i>
              </div>
              <div class="value">
                <h1 class="count">
                  5
                </h1>
                <p>Users</p>
              </div>
            </section>
          </div>
          <div class="col-lg-4 col-sm-6">
            <section class="panel">
              <div class="symbol yellow">
                <i class="fa fa-paperclip"></i>
              </div>
              <div class="value">
                <h1 class=" count3">
                  Thousands
                </h1>
                <p>Ontologies</p>
              </div>
            </section>
          </div>
        </div>
        <!--state overview end-->
        <!-- begin collection panel -->  
        <div class="row">
          <div class="col-sm-12">
            <section class="panel">
              <header class="panel-heading">
                Uploaded Collections
                <span class="tools pull-right">
                  <a href="javascript:;" class="fa fa-chevron-down"></a>
                  <a href="javascript:;" class="fa fa-times"></a>
                </span>
              </header>
              <div class="panel-body">
                <div class="adv-table" id="dynamic"> 
                  <table class="display table table-bordered table-striped" id="hidden-table-info">
                    <thead>
                      <tr>
                        <th>Collection ID</th>
                        <th>Title</th>
                        <th>Uploader</th>
                        <th>Upload Date</th>
                        <th>View</th>
                        <th>Bookmark</th>

                      </tr>
                    </thead>
                    <tbody>
                    <?php if(isset($resources)){ ?>
                    <?php foreach($resources as $row){ ?>
                      <tr>
                        <td><?php echo $row['Resource_ID']; ?></td>
                        <td><?php echo $row['Title']; ?></td>
                        <td><?php echo $row['Uploader']; ?></td>
                        <td><?php echo $row['Upload_Date']; ?></td>
                        <td class="text-center"><a href="<?php echo site_url("workspace/resourceDetails?rid=".$row['Resource_ID'])?>" target="_blank" ><i class="fa fa-eye fa-2x"></i></a></td>
                        <td class="text-center">
                          <button type="button" class="btn btn-primary btn-sm" onclick="setBookmark('<?php echo site_url(); ?>','<?php echo $row['Resource_ID'];?>')">
                           <span class="glyphicon glyphicon-bookmark" aria-hidden="true"></span>
                          </button>
                        </td>
                      </tr>
                     <?php } ?>
                   <?php } ?>
                  </table>
                </div>
              </div>
            </section>
          </div>
        </div>
        <!-- end collection panel-->
      </section>
    </section>
    <!--main content end-->
