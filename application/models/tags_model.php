<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class TAGs_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database('default');
    }
        
    public function getTAGName(){
        $query = $this->db->query("SELECT `tname` FROM `TAG`");
        return $query->result();
    }
    
    public function getTAGTable(){
        $query = $this->db->query("SELECT * FROM `TAG`");
        return $query->result();
    }
    
    public function insertTAGName($tname){
        $data = array(
            'tname' => $tname
        );
        
        $this->db->insert('TAG', $data);
        $last_id = $this->db->insert_id();
        return $last_id;
    }
    
    public function insertCollectionTAG($cid, $tid){
         $data = array(
            'cid_fk' => $cid,
            'tid_fk' => $tid
        );       
        
         $this->db->insert('COLLECTION_TAG', $data);
    }
    
    public function getTAGId($tname){
        $this->db->limit(1);
        $this->db->where('tname', $tname); 
        $query = $this->db->get('TAG');
        return $query->result();
    }
}
