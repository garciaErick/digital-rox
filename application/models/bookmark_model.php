<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Bookmark_model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }    
    
    public function addBookmark($uid, $cid){
        $data = array(
         'cid_fk' => $cid ,
         'uid_fk' => $uid
        );
    
        $this->db->insert('BOOKMARK', $data); 
    }
    
    public function deleteBookmark($uid, $cid){
        $this->db->where('cid_fk', $cid);
        $this->db->where('uid_fk', $uid);
        $this->db->delete('BOOKMARK'); 
    }
    
    public function getUserBookmarks($uid){
        $query = $this->db->query('select * from BOOKMARK, collection_data where cid = cid_fk and uid_fk = '. $uid .' order by cupload_date');
        return $query; 
    }
    
}