<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* File: LoginModel.php
 * Author: Luis Garnica
 * View Dependant: login, register
 * Description: This class user login to the elseweb website and user registration. 
 *  
 *  */

class Workshops_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function getAll() {
		$query = $this->db->get('WORKSHOP');
		return $query->result();
	}
        
        //select * from WORKSHOP, PLACE where pid = pid_fk 

	public function getWorkshopDetails($wid){
        $this->db->where('wid', $wid);
        $query = $this->db->get('WORKSHOP');
        return $query->result();
	}


	public function addWorkshop($newWorkshop) {
		$this->db->insert('WORKSHOP', $newWorkshop);
	}

	public function updateWorkshop($udpatedWorkshop, $wid) {
		$this->db->where('wid', $wid);
		$this->db->update('WORKSHOP', $udpatedWorkshop);
	}

	public function deleteWorkshop($wid){
		$this->db->where('wid', $wid);
		$this->db->delete('WORKSHOP');
	}
}
