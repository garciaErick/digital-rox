<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* File: LoginModel.php
 * Author: Luis Garnica
 * View Dependant: login, register
 * Description: This class user login to the elseweb website and user registration. 
 *  
 *  */

class Videos_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function getAll() {
		$query = $this->db->get('VIDEO');
		return $query->result();
	}

	 public function getVideoDetails($vid){
        $this->db->where('vid', $vid);
        $query = $this->db->get('VIDEO');
        return $query->result();
	}

	public function addVideo($newVideo) {
		$this->db->insert('VIDEO', $newVideo);
	}
	public function updateVideo2($updatedVideo, $vid) {
		$this->db->where('vid', $vid);
		$this->db->update('VIDEO', $updatedVideo);
	}

	public function updateVideo($updatedVideo, $uid, $vid) {
                $this->db->where('uid_FK',   $uid);
                $this->db->where('vid',      $vid);
		$this->db->update('VIDEO',    $updatedVideo);
	}

	public function deleteVideo($vid){
		$this->db->where('vid', $vid);
		$this->db->delete('VIDEO');
	}
}
