<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* File: LoginModel.php
 * Author: Luis Garnica
 * View Dependant: login, register
 * Description: This class user login to the elseweb website and user registration. 
 *  
 *  */

class Query_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function getAll() {
		$query = $this->db->get('QUERY');
		return $query->result();
	}

	public function addQuery($newQuery) {
		$this->db->insert('QUERY', $newQuery);
	}

    public function getQuery() {
    	$sql   = "SELECT * FROM QUERY";
    	$query = $this->db->query($sql);
    	return $query->result();
  }

    public function getNewsArticle($uid, $nid) {
    	$this->db->where('uid_FK', $uid);
    	$this->db->where('nid',    $nid);
    	$query = $this->db->get('QUERY');
    	return $query->result();
  }

   public function updateNews($updatedNews, $uid, $nid) {
   	    $this->db->where('uid_FK', $uid);
    	$this->db->where('nid',      $nid);
		$this->db->update('QUERY',    $updatedNews);
	}

	public function deleteNews($uid, $nid){
		$this->db->where('uid_FK', $uid);
    	$this->db->where('nid',    $nid);
		$this->db->delete('QUERY');
	}

	public function deleteNews2($nid){
    	$this->db->where('nid',    $nid);
		$this->db->delete('QUERY');
	}

}
