
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Metadata_model extends CI_Model {

  public function __construct() {
    parent::__construct();
  }

  public function addMetadata($newMetadata) {
    $this->db->insert('COLLECTION_METADATA', $newMetadata);
  }

  public function addResource($uploadID, $uid_FK){
    $this->db->where('cid', $uploadID);
    $query = $this->db->get('COLLECTION');
    if ($query->num_rows() == 1) {
    } else {
      $collection = array(
        'cid'    => $uploadID,
        'uid_FK' => $uid_FK
      );
      $this->db->insert('COLLECTION', $collection);
    }
  }

  public function addResource_Files($cid, $file){
    $newFile= array(
      'rfpath' => $file,
      'cid_fk' => $cid,
    );
    $this->db->insert('COLLECTION_FILES', $newFile);
  }

  public function getMetaID($metadata) {
    $sql   = "SELECT mid FROM METADATA WHERE mlabel = ?";
    $query = $this->db->query($sql, $metadata);
    return $query->row()->mid;
  }

}
