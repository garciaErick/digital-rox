<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* File: LoginModel.php
 * Author: Luis Garnica
 * View Dependant: login, register
 * Description: This class user login to the elseweb website and user registration. 
 *  
 *  */

class Places_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function getAll() {
		$query = $this->db->get('PLACE');
		return $query->result();
	}

	public function addPlace($newPlace) {
		$this->db->insert('PLACE', $newPlace);
	}

	public function getPlaceDetails($pid){
        $this->db->where('pid', $pid);
        $query = $this->db->get('PLACE');
        return $query->result();
	}

  	public function getPid($pname) {
    	$this->db->select('pid');
    	$this->db->where('pname', $pname);
    	$query = $this->db->get('PLACE');
    	if ($query->num_rows() == 1) { 
    	  //The entry is already in database
    	} else {
	
    }
    $ret = $query->result();
    return $ret[0]->pid; //Returning pid directly because there is only one result
  }

	public function updateInfo($updatedInfo, $uid) {
		$this->db->where('uid', $uid);
		$this->db->update('USER', $updatedInfo);
	}

	public function deleteUser($uid){
		$this->db->where('uid', $uid);
		$this->db->delete('USER');
	}
}
