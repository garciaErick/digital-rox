<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Meetings_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

        public function getAll() {
		$query = $this->db->get('MEETING');
		return $query->result();
	}
        
        public function getMeetingView(){
            $sql = "SELECT * FROM MEETING, PLACE WHERE pid = pid_fk order by medate_time desc";
            $query = $this->db->query($sql);
            return $query->result();       
        }
        
        public function getMeetingDetails($meid){
            $this->db->where('meid', $meid);
            $query = $this->db->get('MEETING');
            return $query->result();
        }

	public function addMeeting($newMeeting) {
		$this->db->insert('MEETING', $newMeeting);
	}
 
	public function deleteMeeting($meid){
		$this->db->where('meid', $meid);
		$this->db->delete('MEETING');
	}        
        
	public function updateMeeting($updatedMeeting, $meid) {
		$this->db->where('meid', $meid);
		$this->db->update('MEETING', $updatedMeeting);
	}

}
