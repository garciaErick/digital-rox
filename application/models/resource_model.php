<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* File: LoginModel.php
 * Author: Luis Garnica
 * View Dependant: login, register
 * Description: This class user login to the elseweb website and user registration. 
 *  
 *  */

class Resource_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    
    //Get resource_data view, includes all resources, metadata and users
    public function getResourceTable() {
        $this->db->order_by("cupload_date", "desc"); 
        return $this->db->get('collection_data');
    }
    
    //Count the rows on specific table
    public function countRows($table) {
        return $this->db->count_all($table);  
    }
    
    //Count the numer of bookmarks by user
    public function countBookMarks($uid) {
        $query = $this->db->query('select count(*) as value from BOOKMARK where uid_fk = '. $uid);
        return $query->row('value');
    }
    
    public function getResourceById($cid){
        $this->db->where('cid', $cid);
        $query = $this->db->get('collection_data'); 
        return $query;
    }
    
    public function getResourceReferences($cid){
        $this->db->where('cid_src', $cid);
        $query = $this->db->get('COLLECTION'); 
        return $query->result();
    }
    
    public function getResourceFiles($cid){
        $this->db->where('cid_fk', $cid);
        $query = $this->db->get('COLLECTION_FILES'); 
        return $query->result();        
    }
        
}