<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* 
 * Author: Luis Garnica
 * View Dependant: login, user administration
 * Description: This class user login to the elseweb website and user registration. 
 *  
 *  */

class User_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /*
     * Function: login
     * Description: Receives input of username and password, checks combination against the
     *              database and returns a user row if successful.
     * */

    public function login($email, $password) {
        $this->db->where('uemail', $email);
        $this->db->where('upassword', $password);
        $query = $this->db->get('USER'); //Table name USER
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            /* $this->session->set_flashdata('incorrect_user', 'Invalid user/password combination'); //Used for error reporting */
            echo "Invalid user/password combination"; 
        }
    }

    public function getAll() {
        $this->db->order_by("ulast_name", "asc"); 
        $query = $this->db->get('USER');
        return $query->result();
    }
    
    public function checkPassword($uid, $password) {
        $this->db->where('uid', $uid);
        $this->db->where('upassword', $password);
        $query = $this->db->get('USER');
        if ($query->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function createUser($newUser) {
        $this->db->insert('USER', $newUser);
    }

    public function getInfo($uid) {
        $sql = "SELECT * FROM USER WHERE uid = ?";
        $query = $this->db->query($sql, $uid);
        return $query->result();
    }
    
    public function updateInfo($updatedInfo, $uid) {
        $this->db->where('uid', $uid);
        $this->db->update('USER', $updatedInfo);
    }

    public function deleteUser($uid){
        $data = array(
        );
        $this->db->where('uid', $uid);
        $this->db->update('USER', $data);
    }
}
