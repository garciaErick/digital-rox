
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Resource_metadata_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function addMetadata($metadata) {
        $this->db->insert_batch('COLLECTION_METADATA', $metadata);
    }

}
