<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* File: LoginModel.php
 * Author: Luis Garnica
 * View Dependant: login, register
 * Description: This class user login to the elseweb website and user registration. 
 *  
 *  */

class Publications_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function getAll() {
		$query = $this->db->get('PUBLICATION');
		return $query->result();
	}

	 public function getPublicationDetails($pid){
            $this->db->where('pid', $pid);
            $query = $this->db->get('PUBLICATION');
            return $query->result();
        }

	public function addPublication($newPublication) {
		$this->db->insert('PUBLICATION', $newPublication);
	}

	public function updatePublication($updatedPublication, $pid) {
		$this->db->where('pid', $pid);
		$this->db->update('PUBLICATION', $updatedPublication);
	}

	public function deletePublication($pid){
		$this->db->where('pid', $pid);
		$this->db->delete('PUBLICATION');
	}
}
